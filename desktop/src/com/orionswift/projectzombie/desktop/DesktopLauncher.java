package com.orionswift.projectzombie.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.orionswift.projectzombie.ProjectZombie;

public class DesktopLauncher {
	public static void main(String[] arg) {
		try {
			TexturePacker.Settings settings = new TexturePacker.Settings();
			settings.combineSubdirectories = true;
			settings.silent = true;
			settings.paddingX = 2;
			settings.paddingY = 2;
			settings.duplicatePadding = true;
			TexturePacker.process(settings, "sprites/", "_packed_sprites", "sprites");
		} catch (RuntimeException e) {
			if (!(e.getCause() instanceof IllegalArgumentException)) {
				throw e;
			}
		}

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 1280;
		config.height = 720;
		config.title = "Project Zombie";
		new LwjglApplication(new ProjectZombie(), config);
	}
}
