package com.orionswift.projectzombie.tile;

import com.badlogic.ashley.core.Entity;

public class TileEntityHandle {
    private Entity entity;
    private TileType type;

    public TileEntityHandle(Entity entity, TileType type) {
        this.entity = entity;
        this.type = type;
    }

    public Entity getEntity() {
        return entity;
    }

    public TileType getType() {
        return type;
    }
}
