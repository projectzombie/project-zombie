package com.orionswift.projectzombie.tile;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.orionswift.projectzombie.util.Assets;

public enum TileType {
    WOOD(30, false, Assets.sprites.findRegion("tiles/wood"), 5),
    STEEL(100, false, Assets.sprites.findRegion("tiles/steel"), 20),
    TURRET(100, true, Assets.sprites.findRegion("turrets/machinegun/machinegun_still"), 250),
    HORIZONTAL_DOOR(30, true, Assets.sprites.findRegion("tiles/hdoor"), 20),
    VERTICAL_DOOR(30, true, Assets.sprites.findRegion("tiles/vdoor"), 20);

	// TODO: Add description variable.
    private final int maxHealth;
    private final boolean canShootThrough;
    private TextureRegion texture;
    private float price;

    TileType(int maxHealth, boolean canShootThrough, TextureRegion texture, float price) {
        this.maxHealth = maxHealth;
        this.canShootThrough = canShootThrough;
        this.texture = texture;
        this.price = price;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public boolean isCanShootThrough() {
        return canShootThrough;
    }

    public TextureRegion getTexture() {
        return texture;
    }

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
    
}
