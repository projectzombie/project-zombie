package com.orionswift.projectzombie.screen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntityListener;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CCameraFollow;
import com.orionswift.projectzombie.event.Event;
import com.orionswift.projectzombie.shop.Category;
import com.orionswift.projectzombie.shop.PipeBombShopItem;
import com.orionswift.projectzombie.shop.ShopItem;
import com.orionswift.projectzombie.shop.TileTypeShopItem;
import com.orionswift.projectzombie.shop.WeaponShopItem;
import com.orionswift.projectzombie.system.*;
import com.orionswift.projectzombie.tile.TileEntityHandle;
import com.orionswift.projectzombie.tile.TilePosition;
import com.orionswift.projectzombie.tile.TileType;
import com.orionswift.projectzombie.ui.HUDUI;
import com.orionswift.projectzombie.ui.Money;
import com.orionswift.projectzombie.ui.PauseUI;
import com.orionswift.projectzombie.util.Assets;
import com.orionswift.projectzombie.util.CollisionListener;
import com.orionswift.projectzombie.util.EntityCreator;
import com.orionswift.projectzombie.util.WeightedRandom;
import com.orionswift.projectzombie.util.ZombieType;
import com.orionswift.projectzombie.util.hack.SubscriptionFactoryHack;
import com.orionswift.projectzombie.wave.WaveConfiguration;
import com.orionswift.projectzombie.wave.Waves;
import com.orionswift.projectzombie.weapon.SMG;
import com.orionswift.projectzombie.weapon.Shotgun;

import box2dLight.RayHandler;
import net.engio.mbassy.bus.MBassador;
import net.engio.mbassy.bus.config.BusConfiguration;
import net.engio.mbassy.bus.config.Feature;
import net.engio.mbassy.bus.error.IPublicationErrorHandler;
import net.engio.mbassy.bus.error.PublicationError;

public class GS extends ScreenAdapter {
	
    private Engine engine;
    private World world;
    private OrthographicCamera camera;
    private RayHandler rayHandler;
    private SpriteBatch batch;
    private ShapeRenderer shapeRenderer;
    private MBassador<Event> bus;
    private float time = STime.dayStart;
    private Waves waves;
    private int wave;
    private Map<TilePosition, TileEntityHandle> tiles;
    private List<Body> bodiesToRemoveNextFrame = new ArrayList<>();
    private Stage uiStage;
    private List<Category> shopCategories;

    public boolean pause = false;
    public boolean justResumed = false;

    public InputMultiplexer multiplexer = new InputMultiplexer();
    public Money money = new Money(50);

    public GS() {
        waves = new Waves();
        waves
                .addWave(1, new WaveConfiguration(30, 1, new WeightedRandom<ZombieType>()
                		.add(ZombieType.NORMAL, 1)))
                .addWave(2, new WaveConfiguration(35, 1, new WeightedRandom<ZombieType>()
                		.add(ZombieType.NORMAL, 0.8f)
                		.add(ZombieType.CROW, 0.2f)))
                .addWave(3, new WaveConfiguration(70, 0.7f, new WeightedRandom<ZombieType>()
                        .add(ZombieType.NORMAL, 0.8f)
                        .add(ZombieType.CROW, 0.2f)))
                .addWave(4, new WaveConfiguration(50, 1, new WeightedRandom<ZombieType>()
                		.add(ZombieType.NORMAL, 0.8f)
                		.add(ZombieType.KAMIKAZE, 0.1f)
                		.add(ZombieType.CROW, 0.1f)))
                .addWave(5, new WaveConfiguration(50, 1, new WeightedRandom<ZombieType>()
                		.add(ZombieType.NORMAL, 0.7f)
                		.add(ZombieType.KAMIKAZE, 0.1f)
                		.add(ZombieType.CROW, 0.15f)
                		.add(ZombieType.MOOSE, 0.05f)));
        engine = new Engine();
        engine.addSystem(new SAggro());
        engine.addSystem(new SAggroWeaponAngle());
        engine.addSystem(new SAnimation());
        engine.addSystem(new SAnimationOnShoot());
        engine.addSystem(new SCameraFollow());
        engine.addSystem(new SDamageOnTouch());
        engine.addSystem(new SDayNightLighting());
        engine.addSystem(new SDespawnOnDay());
        engine.addSystem(new SDrawGrass());
        engine.addSystem(new SDropCoinsOnDeath());
        engine.addSystem(new SExplodeSoon());
        engine.addSystem(new SExplosion());
        engine.addSystem(new SExplosionSound());
        engine.addSystem(new SFaceAggro());
        engine.addSystem(new SFireBulletEntity());
        engine.addSystem(new SGameMusic());
        engine.addSystem(new SGoryDeath());
        engine.addSystem(new SGunReload());
        engine.addSystem(new SHeadTowardsAggroInput());
        engine.addSystem(new SIgnoreDamage());
        engine.addSystem(new SKillOnHealthZero());
        engine.addSystem(new SKnockback());
        engine.addSystem(new SLightRender());
        engine.addSystem(new SMovement());
        engine.addSystem(new SMultiplier());
        engine.addSystem(new SOpenCloseDoor());
        engine.addSystem(new SParticleEmitters());
        engine.addSystem(new SPickUpSupply());
        engine.addSystem(new SPlaceRemoveTile());
        engine.addSystem(new SPlayAnimationOnInput());
        engine.addSystem(new SPlayerFireWeapon());
        engine.addSystem(new SPlayerInput());
        engine.addSystem(new SPlayerLegTorsoControls());
        engine.addSystem(new SPlayerWeaponAngle());
        engine.addSystem(new SRaycastCollision());
        engine.addSystem(new SRemoveEntityOnDeath());
        engine.addSystem(new SRemoveOnAnimatedSpriteFinish());
        engine.addSystem(new SRemoveOnTouch());
        engine.addSystem(new SRender());
        engine.addSystem(new SRenderHealthBar());
        engine.addSystem(new SRepel());
        engine.addSystem(new SResetMultipliersOnDay());
        engine.addSystem(new SShootOnAggroProximity());
        engine.addSystem(new SSoundEffectOnDamage());
        engine.addSystem(new SSoundEffectOnDeath());
        engine.addSystem(new SSpawnBoss());
        engine.addSystem(new SSpawnSupply());
        engine.addSystem(new SSpawnZombieRandomly());
        engine.addSystem(new SSpawnZombieWave());
        engine.addSystem(new SStepSound());
        engine.addSystem(new SSubtractHealth());
        engine.addSystem(new SSwitchWeapon());
        engine.addSystem(new SSwitchWeaponSound());
        engine.addSystem(new SSwitchWeaponSprite());
        engine.addSystem(new STileCost());
        engine.addSystem(new STilePlacement());
        engine.addSystem(new STime());
        engine.addSystem(new STrailRender());
        engine.addSystem(new SUpdateWeapon());
        engine.addSystem(new SWave());
        engine.addSystem(new SGameOverOnDeath());
        engine.addSystem(new SExplodeOnNear());
        engine.addSystem(new SAbsorbHealthOnDamage());
        engine.addSystem(new SRepeatSoundEffectRandomly());

        engine.addEntityListener(Family.all(CBody.class).get(), new EntityListener() {
            @Override
            public void entityAdded(Entity entity) {

            }

            @Override
            public void entityRemoved(Entity entity) {
                bodiesToRemoveNextFrame.add(entity.getComponent(CBody.class).getBody());
            }
        });

        world = new World(new Vector2(), true);
        world.setContactListener(new CollisionListener());

        tiles = new HashMap<>();

        RayHandler.useDiffuseLight(true);
        rayHandler = new RayHandler(world);
        rayHandler.setBlur(true);
        rayHandler.setAmbientLight(0, 0, 0, 0);

        shapeRenderer = new ShapeRenderer();
        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        bus = new MBassador<>(new BusConfiguration()
                .addFeature(Feature.SyncPubSub.Default().setSubscriptionFactory(new SubscriptionFactoryHack()))
                .addFeature(Feature.AsynchronousHandlerInvocation.Default())
                .addFeature(Feature.AsynchronousMessageDispatch.Default()));
        bus.addErrorHandler(new IPublicationErrorHandler() {
            @Override
            public void handleError(PublicationError error) {
                error.getCause().getCause().printStackTrace();
            }
        });

        for (EntitySystem sys : engine.getSystems()) {
            bus.subscribe(sys);
        }

        Entity player = EntityCreator.createPlayer();
        engine.addEntity(player);

        Gdx.graphics.setCursor(Gdx.graphics.newCursor(Assets.crosshair, Assets.crosshair.getWidth() / 2, Assets.crosshair.getHeight() / 2));

        List<ShopItem> tileItems = new ArrayList<>();
        for (TileType tt : TileType.values()) {
            tileItems.add(new TileTypeShopItem(tt));
        }
        shopCategories = Arrays.asList(
                new Category("Tiles", tileItems),
                new Category("Weapons", Arrays.asList(
                        //new WeaponShopItem("Pistol", Assets.sprites.findRegion("weapons/onehand/pistol"), Pistol.class, 100),
                        new WeaponShopItem("SMG", Assets.sprites.findRegion("weapons/gun"), SMG.class, 150),
                        new WeaponShopItem("Shotgun", Assets.sprites.findRegion("weapons/gun"), Shotgun.class, 200),
                        new PipeBombShopItem("Pipe Bomb", 50, Assets.sprites.findRegion("ballistic/pipebomb"))
                )));

        switchStage(new HUDUI());

        multiplexer.addProcessor(engine.getSystem(SPlayerInput.class));
        multiplexer.addProcessor(engine.getSystem(STilePlacement.class));
        multiplexer.addProcessor(engine.getSystem(SPlayerFireWeapon.class));
        Gdx.input.setInputProcessor(multiplexer);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (!pause) {
            for (Body body : bodiesToRemoveNextFrame) {
                world.destroyBody(body);
            }
            bodiesToRemoveNextFrame.clear();

            shapeRenderer.setProjectionMatrix(camera.combined);
            batch.setProjectionMatrix(camera.combined);

            if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE) && !justResumed) {
                switchStage(new PauseUI());
                pause = true;
                timePause();
            }

            if (justResumed) {
                if (!engine.getSystem(STilePlacement.class).isTiling) {
                    timeUnpause();
                }
            }
            justResumed = false;

            world.step(delta, 5, 15);
            engine.update(delta);
        }

        uiStage.act(delta);
        uiStage.draw();
        //new Box2DDebugRenderer().render(world, camera.combined);
    }

    public void timePause() {
        SGameMusic.pause();
        STime.pause = true;
    }

    public void timeUnpause() {
        SGameMusic.play();
        STime.pause = false;
    }

    @Override
    public void show() {
        multiplexer.addProcessor(engine.getSystem(SPlayerInput.class));
        multiplexer.addProcessor(engine.getSystem(STilePlacement.class));
        multiplexer.addProcessor(engine.getSystem(SPlayerFireWeapon.class));
        Gdx.input.setInputProcessor(multiplexer);
    }

    @Override
    public void resize(int width, int height) {
        camera.setToOrtho(false, width / 64f, height / 64f);

        ImmutableArray<Entity> entities = engine.getEntitiesFor(Family.all(CCameraFollow.class).get());
        if (entities.size() > 0) {
            camera.position.set(entities.first().getComponent(CBody.class).getPosition(), 0);

        }
        camera.update();

        uiStage.getViewport().update(width, height, true);
    }

    public Engine getEngine() {
        return engine;
    }

    public World getWorld() {
        return world;
    }

    public OrthographicCamera getCamera() {
        return camera;
    }

    public SpriteBatch getBatch() {
        return batch;
    }

    public ShapeRenderer getShapeRenderer() {
        return shapeRenderer;
    }

    public static GS i;

    {
        i = this;
    }

    public MBassador<Event> getBus() {
        return bus;
    }

    public RayHandler getRayHandler() {
        return rayHandler;
    }

    public float getTime() {
        return time;
    }

    public void setTime(float time) {
        this.time = time;
    }

    public Map<TilePosition, TileEntityHandle> getTiles() {
        return tiles;
    }

    public void switchStage(Stage stage) {
        if (uiStage != null) {
            multiplexer.removeProcessor(uiStage);
        }
        uiStage = stage;
        multiplexer.addProcessor(0, stage);
    }

    public List<Category> getShopCategories() {
        return shopCategories;
    }

    public Waves getWaves() {
        return waves;
    }

    public int getWave() {
        return wave;
    }

    public void setWave(int wave) {
        this.wave = wave;
    }

    public Stage getUIStage() {
        return uiStage;
    }
}
