package com.orionswift.projectzombie.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.orionswift.projectzombie.ui.TitleUI;
import com.orionswift.projectzombie.ui.UISkin;
import com.orionswift.projectzombie.util.Assets;

public class MenuScreen extends ScreenAdapter {
    private Stage menu;

    public MenuScreen() {
        Assets.loadAssets();
        UISkin.load();

        menu = new TitleUI();
        Gdx.input.setInputProcessor(menu);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(menu);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        menu.act(delta);
        menu.draw();
    }

    @Override
    public void resize(int width, int height) {
        menu.getViewport().setScreenSize(width, height);
    }

    public static MenuScreen i;

    {
        i = this;
    }
}
