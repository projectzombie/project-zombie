package com.orionswift.projectzombie.event;

import com.badlogic.ashley.core.Entity;

public class EDamage extends Event {
    private Entity damagee;
    private Entity source;
    private Entity sourceSource;
    private float damage;
    private float knockback;
    private KnockbackMode knockbackMode;
    private DamageType damageType;

    public EDamage(Entity damagee, Entity source, Entity sourceSource, float damage, float knockback, KnockbackMode knockbackMode, DamageType damageType) {
        this.damagee = damagee;
        this.source = source;
        this.sourceSource = sourceSource;
        this.damage = damage;
        this.knockback = knockback;
        this.knockbackMode = knockbackMode;
        this.damageType = damageType;
    }

    public Entity getDamagee() {
        return damagee;
    }

    public Entity getSource() {
        return source;
    }

    public Entity getSourceSource() {
        return sourceSource;
    }

    public float getDamage() {
        return damage;
    }

    public float getKnockback() {
        return knockback;
    }

    public DamageType getDamageType() {
        return damageType;
    }

    public KnockbackMode getKnockbackMode() {
        return knockbackMode;
    }

    public void setDamage(float damage) {
        this.damage = damage;
    }

    public enum KnockbackMode {
        SourcePosition,
        SourceVelocity
    }

    public enum DamageType {
        Bullet,
        Melee
    }
}
