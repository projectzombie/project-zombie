package com.orionswift.projectzombie.event;

import com.badlogic.ashley.core.Entity;

public class EStopFiringWeapon extends Event {
    private Entity entity;

    public EStopFiringWeapon(Entity entity) {
        this.entity = entity;
    }

    public Entity getEntity() {
        return entity;
    }
}
