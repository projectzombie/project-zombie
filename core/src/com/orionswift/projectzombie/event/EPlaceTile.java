package com.orionswift.projectzombie.event;

import com.orionswift.projectzombie.tile.TilePosition;
import com.orionswift.projectzombie.tile.TileType;

public class EPlaceTile extends Event {
    private TilePosition pos;
    private TileType type;

    public EPlaceTile(TilePosition pos, TileType type) {
        this.pos = pos;
        this.type = type;
    }

    public TilePosition getPos() {
        return pos;
    }

    public TileType getType() {
        return type;
    }
}
