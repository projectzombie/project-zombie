package com.orionswift.projectzombie.event;

import com.badlogic.ashley.core.Entity;
import com.orionswift.projectzombie.weapon.Gun;

public class EGunReload extends Event {
	
	private Entity entity;
	
	public EGunReload(Entity entity) {
		this.entity = entity;
	}
	
	public Entity getEntity() {
		return entity;
	}
}
