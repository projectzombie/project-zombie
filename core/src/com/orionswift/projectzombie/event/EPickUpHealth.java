package com.orionswift.projectzombie.event;

import com.badlogic.ashley.core.Entity;

public class EPickUpHealth extends Event {
	
	private Entity target, healthBox;
	
	public EPickUpHealth(Entity target, Entity healthBox) {
		this.target = target;
		this.healthBox = healthBox;
	}

	public Entity getTarget() {
		return target;
	}

	public Entity getHealthBox() {
		return healthBox;
	}
	
}
