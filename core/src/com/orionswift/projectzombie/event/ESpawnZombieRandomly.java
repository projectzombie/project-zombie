package com.orionswift.projectzombie.event;

import com.orionswift.projectzombie.util.ZombieType;

public class ESpawnZombieRandomly extends Event {
    private ZombieType type;
    private float multiplier;

    public ESpawnZombieRandomly(ZombieType type, float multiplier) {
        this.type = type;
        this.multiplier = multiplier;
    }

    public ZombieType getType() {
        return type;
    }

    public float getMultiplier() {
        return multiplier;
    }
}
