package com.orionswift.projectzombie.event;

import com.badlogic.ashley.core.Entity;

public class EStartFiringWeapon extends Event {
    private Entity entity;

    public EStartFiringWeapon(Entity entity) {
        this.entity = entity;
    }

    public Entity getEntity() {
        return entity;
    }
}
