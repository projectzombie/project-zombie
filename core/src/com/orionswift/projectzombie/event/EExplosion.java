package com.orionswift.projectzombie.event;

import com.badlogic.ashley.core.Entity;

public class EExplosion extends Event {
    private Entity source;
    private float damage;

    public EExplosion(Entity source, float damage) {
        this.source = source;
        this.damage = damage;
    }

    public Entity getSource() {
        return source;
    }
    
    public float getDamage() {
    	return damage;
    }
}
