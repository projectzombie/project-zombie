package com.orionswift.projectzombie.event;

import com.orionswift.projectzombie.tile.TileType;

public class EStartTilePlacement extends Event {
	private TileType tileType;

	public EStartTilePlacement(TileType tileType) {
		this.tileType = tileType;
	}

	public TileType getTileType() {
		return tileType;
	}
}
