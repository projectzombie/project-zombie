package com.orionswift.projectzombie.event;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;

public class EShootBullet extends Event {
    private Entity shooter;
    private Vector2 muzzleOffset;
    private float mouseAngle;

    public EShootBullet(Entity shooter, Vector2 muzzleOffset, float mouseAngle) {
        this.shooter = shooter;
        this.muzzleOffset = muzzleOffset;
        this.mouseAngle = mouseAngle;
    }

    public Entity getShooter() {
        return shooter;
    }

    public float getMouseAngle() {
        return mouseAngle;
    }

    public Vector2 getMuzzleOffset() {
        return muzzleOffset;
    }
}
