package com.orionswift.projectzombie.event;

import com.badlogic.ashley.core.Entity;

public class EPlayAnimation extends Event {
    private Entity entity;
    private String animation;
    private int priority;

    public EPlayAnimation(Entity entity, String animation, int priority) {
        this.entity = entity;
        this.animation = animation;
        this.priority = priority;
    }

    public Entity getEntity() {
        return entity;
    }

    public String getAnimation() {
        return animation;
    }

    public int getPriority() {
        return priority;
    }
}
