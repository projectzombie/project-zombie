package com.orionswift.projectzombie.event;

public class EWaveStart extends Event {
    private int waveNumber;

    public EWaveStart(int waveNumber) {
        this.waveNumber = waveNumber;
    }

    public int getWaveNumber() {
        return waveNumber;
    }
}
