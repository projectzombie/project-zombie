package com.orionswift.projectzombie.event;

import com.badlogic.ashley.core.Entity;

public class EEntityDeath extends Event {
    private Entity entity;

    public EEntityDeath(Entity entity) {
        this.entity = entity;
    }

    public Entity getEntity() {
        return entity;
    }
}
