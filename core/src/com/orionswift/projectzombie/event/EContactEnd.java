package com.orionswift.projectzombie.event;

import com.badlogic.gdx.physics.box2d.Contact;

public class EContactEnd extends Event {
    private Contact contact;

    public EContactEnd(Contact contact) {
        this.contact = contact;
    }

    public Contact getContact() {
        return contact;
    }
}
