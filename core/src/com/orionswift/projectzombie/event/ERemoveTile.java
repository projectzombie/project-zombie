package com.orionswift.projectzombie.event;

import com.orionswift.projectzombie.tile.TilePosition;

public class ERemoveTile extends Event {
    private TilePosition pos;
    private boolean isFullRefund;

    public ERemoveTile(TilePosition pos, boolean isFullRefund) {
        this.pos = pos;
        this.isFullRefund = isFullRefund;
    }

    public TilePosition getPos() {
        return pos;
    }

    public boolean isFullRefund() {
        return isFullRefund;
    }
}
