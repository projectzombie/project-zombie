package com.orionswift.projectzombie.event;

public abstract class Event {
    private boolean isCancelled;

    public Event() {
    }

    public boolean isCancelled() {
        return isCancelled;
    }

    public void setCancelled(boolean cancelled) {
        isCancelled = cancelled;
    }
}
