package com.orionswift.projectzombie.event;

import com.badlogic.ashley.core.Entity;
import com.orionswift.projectzombie.weapon.Weapon;

public class ESwitchWeapon extends Event {
    private Entity entity;
    private Weapon oldWeapon;
    private Weapon newWeapon;

    public ESwitchWeapon(Entity entity, Weapon oldWeapon, Weapon weapon) {
        this.entity = entity;
        this.oldWeapon = oldWeapon;
        this.newWeapon = weapon;
    }

    public Entity getEntity() {
        return entity;
    }

    public Weapon getOldWeapon() {
        return oldWeapon;
    }

    public Weapon getNewWeapon() {
        return newWeapon;
    }
}
