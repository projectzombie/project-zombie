package com.orionswift.projectzombie.event;

import com.badlogic.ashley.core.Entity;

public class EStopAnimation extends Event {
    private Entity entity;
    private String animation;

    public EStopAnimation(Entity entity, String animation) {
        this.entity = entity;
        this.animation = animation;
    }

    public Entity getEntity() {
        return entity;
    }

    public String getAnimation() {
        return animation;
    }
}
