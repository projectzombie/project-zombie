package com.orionswift.projectzombie.event;

import com.badlogic.ashley.core.Entity;

public class EPickUpAmmo extends Event {
	
	private Entity target, ammoBox;
	
	public EPickUpAmmo(Entity target, Entity ammoBox) {
		this.target = target;
		this.ammoBox = ammoBox;
	}

	public Entity getTarget() {
		return target;
	}

	public Entity getAmmoBox() {
		return ammoBox;
	}
}
