package com.orionswift.projectzombie.event;


import com.badlogic.ashley.core.Entity;

public class EEntitiesCollide extends Event {
    private Entity entityA;
    private Entity entityB;

    public EEntitiesCollide(Entity entityA, Entity entityB) {
        this.entityA = entityA;
        this.entityB = entityB;
    }

    public Entity getEntityB() {
        return entityB;
    }

    public Entity getEntityA() {
        return entityA;
    }
}
