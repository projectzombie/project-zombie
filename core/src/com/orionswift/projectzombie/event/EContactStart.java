package com.orionswift.projectzombie.event;

import com.badlogic.gdx.physics.box2d.Contact;

public class EContactStart extends Event {
    private Contact contact;

    public EContactStart(Contact contact) {
        this.contact = contact;
    }

    public Contact getContact() {
        return contact;
    }
}
