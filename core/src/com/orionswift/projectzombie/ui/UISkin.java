package com.orionswift.projectzombie.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.orionswift.projectzombie.util.Assets;

public class UISkin extends Skin {
    public static UISkin i;
    public static void load() {
        i = new UISkin();
    }

    private UISkin() {
        addRegions(Assets.sprites);

        {
            TextButton.TextButtonStyle def = new TextButton.TextButtonStyle(getDrawable("ui/buttonup"), getDrawable("ui/buttondown"), null, Assets.smallFont);
            TextButton.TextButtonStyle deftoggle = new TextButton.TextButtonStyle(getDrawable("ui/buttonup"), getDrawable("ui/buttondown"), getDrawable("ui/buttondown"), Assets.smallFont);
            TextButton.TextButtonStyle defup = new TextButton.TextButtonStyle(getDrawable("ui/buttonup"), getDrawable("ui/buttonup"), null, Assets.smallFont);
            TextButton.TextButtonStyle defdown = new TextButton.TextButtonStyle(getDrawable("ui/buttondown"), getDrawable("ui/buttondown"), null, Assets.smallFont);

            add("default", def);
            add("default_toggle", deftoggle);
            add("default_up", defup);
            add("default_down", defdown);
        }

        {
            Label.LabelStyle def = new Label.LabelStyle(Assets.smallFont, Color.WHITE);
            add("default", def);

            Label.LabelStyle def2 = new Label.LabelStyle(Assets.largeFont, Color.WHITE);
            add("large", def2);
        }

        add("default", Assets.smallFont);
    }
}
