package com.orionswift.projectzombie.ui;

public class Money {
	public float amount;
	
	public Money(float amount) {
		this.amount = amount;
	}
	
	public void set(float amount) {
		this.amount = amount;
	}
	
	public void add(float add) {
		this.amount += add;
	}
	
	public void sub(float sub) {
		this.amount -= sub;
	}
	
	public float get() {
		return amount;
	}
}
