package com.orionswift.projectzombie.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.orionswift.projectzombie.screen.GS;
import com.orionswift.projectzombie.shop.Category;
import com.orionswift.projectzombie.shop.ShopItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ShopUI extends Stage {
    private Color blue = new Color(0x1EA7E1FF);
    private Color red = new Color(0xE82817FF);

    private Table table;
    private Table innerTable;
    private java.util.List<TextButton> tabButtons;
    private Map<TextButton, ShopItem> shopButtons;

    private boolean justChanged = false;

    public ShopUI() {
        super(new ScreenViewport());

        table = new Table(UISkin.i);

        Stack inner = new Stack();

        Image bg = new Image(UISkin.i, "ui/buttonup");
        innerTable = new Table();

        TextButton exitButton = new TextButton("Exit", UISkin.i);
        exitButton.setColor(red);
        exitButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                GS.i.switchStage(new HUDUI());
                GS.i.pause = false;
            }
        });

        shopButtons = new HashMap<>();
        tabButtons = new ArrayList<>();
        for (Category cat : GS.i.getShopCategories()) {
            createTabButton(cat);
        }

        tabButtons.get(0).toggle();

        inner.add(bg);
        inner.add(innerTable);

        table.setFillParent(true);

        table.center().pad(20);

        for (TextButton button : tabButtons) {
            table.add(button).space(5).width(80).height(40);
        }
        table.add(new Actor()).expandX().fill();
        table.add(exitButton).width(80).height(40);
        table.row();

        table.add(inner).space(5).fill().expand().colspan(tabButtons.size() + 2);

        addActor(table);
    }

    private void createTabButton(final Category cat) {
        final TextButton tab = new TextButton(cat.getName(), UISkin.i, "default_toggle");
        tab.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (tab.isChecked()) {
                    for (TextButton button : tabButtons) {
                        justChanged = true;
                        if (button != tab) {
                            button.setChecked(false);
                        }
                        justChanged = false;
                    }

                    switchCategory(cat);
                } else if (!justChanged) {
                    event.cancel();
                }
            }
        });
        tab.setColor(blue);

        tabButtons.add(tab);
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        for (Map.Entry<TextButton, ShopItem> entry : shopButtons.entrySet()) {
            if (GS.i.money.get() < entry.getValue().getPrice()) {
                entry.getKey().setColor(red);
            } else {
                entry.getKey().setColor(blue);
            }

            entry.getKey().setDisabled(entry.getValue().shouldDisableButton());

            if (entry.getKey().isDisabled()) {
                entry.getKey().setStyle(UISkin.i.get("default", TextButton.TextButtonStyle.class));
            } else {
                entry.getKey().setStyle(UISkin.i.get("default", TextButton.TextButtonStyle.class));
            }
        }
    }

    private void switchCategory(Category category) {
        innerTable.clearChildren();
        innerTable.top().left().pad(20);

        for (final ShopItem item : category.getItems()) {
            innerTable.add(new Image(item.getIcon())).size(64, 64).space(10);

            Label name = new Label(item.getName().replaceAll("_", " "), UISkin.i);
            name.setColor(Color.BLACK);
            innerTable.add(name).space(10).expandX().left();

            TextButton buy = new TextButton(item.getBuyButtonMessage(), UISkin.i, "default");
            buy.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    item._purchase();
                }
            });
            buy.setColor(blue);
            shopButtons.put(buy, item);

            innerTable.add(buy).size(150, 40).center();
            innerTable.row();
        }
    }

    private String prettyPrintFloat(float f) {
        if (f == (long) f)
            return String.format("%d", (long) f);
        else
            return String.format("%s", f);
    }
}
