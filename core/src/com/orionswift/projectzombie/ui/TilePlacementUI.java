package com.orionswift.projectzombie.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.orionswift.projectzombie.event.EStopTilePlacement;
import com.orionswift.projectzombie.screen.GS;

public class TilePlacementUI extends Stage {
    private Color blue = new Color(0x1EA7E1FF);
    private Color red = new Color(0xE82817FF);

    private Label moneyLabel;
    private TextButton exitButton;
    private Table table;

    public TilePlacementUI() {
        super(new ScreenViewport());

        table = new Table();

        moneyLabel = new Label("", UISkin.i, "default");

        exitButton = new TextButton("Exit", UISkin.i, "default");
        exitButton.setColor(red);

        exitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                GS.i.getBus().publish(new EStopTilePlacement());
                GS.i.switchStage(new HUDUI());
            }
        });

        table.setFillParent(true);
        table.top().left().pad(20);

        table.add(exitButton).width(150).height(50).top().right();
        table.row();
        table.add(moneyLabel);

        addActor(table);
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        moneyLabel.setText("$" + prettyPrintFloat(GS.i.money.get()));
    }

    private String prettyPrintFloat(float f) {
        if (f == (long) f)
            return String.format("%d", (long) f);
        else
            return String.format("%s", f);
    }
}
