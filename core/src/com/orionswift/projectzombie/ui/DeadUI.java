package com.orionswift.projectzombie.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.orionswift.projectzombie.ProjectZombie;
import com.orionswift.projectzombie.screen.GS;

public class DeadUI extends Stage {
	
	private Label message;
	private TextButton restart;
	private TextButton quit;
	
	private Table table;
	
	public DeadUI() {
		super(new ScreenViewport());
		
		table = new Table(UISkin.i);
		
		message = new Label("GAME OVER", UISkin.i, "large");
		message.setColor(new Color(1, 1, 1, 0)); // WHITE.
		message.addAction(Actions.fadeIn(1));
		
		message.setColor(new Color(1, 1, 1, 0)); // WHITE.
		message.addAction(Actions.fadeIn(1));
		
		restart = new TextButton("Restart", UISkin.i, "default");
		restart.setColor(new Color(0, 1, 0, 0)); // GREEN.
		restart.addAction(Actions.fadeIn(1));
		restart.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				ProjectZombie.i.setScreen(new GS());
			}
		});
		
		quit = new TextButton("Quit", UISkin.i, "default");
		quit.setColor(new Color(1, 0, 0, 0)); // RED.
		quit.addAction(Actions.fadeIn(1));
		quit.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				ProjectZombie.i.dispose();
				Gdx.app.exit();
			}
		});
		
        table.setFillParent(true);
        table.top().left().pad(20);
		
		table.add(message).pad(50).expandX().center().row();
		table.add(restart).width(150).height(50).row();
		table.add(quit).pad(30).width(150).height(50).row();;
		addActor(table);
	}
}
