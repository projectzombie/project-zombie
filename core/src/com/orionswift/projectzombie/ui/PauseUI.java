package com.orionswift.projectzombie.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.orionswift.projectzombie.ProjectZombie;
import com.orionswift.projectzombie.screen.GS;

public class PauseUI extends Stage {
    private Color blue = new Color(0x1EA7E1FF);

    private Stage old;

    public PauseUI() {
        super(new ScreenViewport());
        old = GS.i.getUIStage();

        Table table = new Table();
        table.top();

        Label name = new Label("PAUSED", UISkin.i, "large");
        name.setColor(0, 0, 0, 1);
        table.add(name).pad(50).expandX().center().row();

        TextButton button = new TextButton("Resume", UISkin.i, "default");
        table.add(button).expandX().width(150).center();
        button.setColor(blue);
        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                resume();
            }
        });

        table.setFillParent(true);
        addActor(table);
    }

    @Override
    public void act(float delta) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            ProjectZombie.i.setScreen(GS.i);
        }
    }

    @Override
    public boolean keyDown(int keyCode) {
        if (keyCode == Input.Keys.ESCAPE) {
            resume();
            return true;
        }
        return false;
    }

    private void resume() {
        GS.i.justResumed = true;
        GS.i.switchStage(old);
        GS.i.pause = false;
    }
}
