package com.orionswift.projectzombie.ui;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.scenes.scene2d.actions.DelayAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.orionswift.projectzombie.component.CAmmoBox;
import com.orionswift.projectzombie.component.CAmmoHUD;
import com.orionswift.projectzombie.component.CHealthBox;
import com.orionswift.projectzombie.component.CPipeBombs;
import com.orionswift.projectzombie.component.CStopFiringOnUI;
import com.orionswift.projectzombie.component.CWeapon;
import com.orionswift.projectzombie.event.EStopFiringWeapon;
import com.orionswift.projectzombie.screen.GS;
import com.orionswift.projectzombie.system.STime;
import com.orionswift.projectzombie.util.Assets;
import com.orionswift.projectzombie.weapon.Gun;

public class HUDUI extends Stage {
	public static boolean disableShopAtNight = true;
	
    static boolean hasShownHelp1 = false;
    static boolean hasShownHelp2 = false;
    private final Label timeLabel;

    private Color blue = new Color(0x1EA7E1FF);

    private Label moneyLabel;
    private TextButton shopButton;
    private TextButton skipButton;
    // TODO: Add bullet icon next to the bullet info
    private Image bulletImage;
    private Label bulletLabel;
    private Image hpbImage;
    private Label hpbLabel;
    private Image ambImage;
    private Label ambLabel;
    private Image pipeBombImage;
    private Label pipeBombLabel;
    private Table table;

    private Label helpText;

    public HUDUI() {
        super(new ScreenViewport());

        table = new Table(UISkin.i);

        moneyLabel = new Label("", UISkin.i, "default");

        shopButton = new TextButton("Shop", UISkin.i, "default");
        shopButton.setColor(blue);

        shopButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                for (Entity entity : GS.i.getEngine().getEntitiesFor(Family.all(CStopFiringOnUI.class, CWeapon.class).get())) {
                    GS.i.getBus().publish(new EStopFiringWeapon(entity));
                }
                GS.i.pause = true;
                GS.i.switchStage(new ShopUI());
            }
        });

        skipButton = new TextButton("Skip to Night", UISkin.i, "default");
        skipButton.setColor(Color.ORANGE);

        skipButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (STime.isDay())
                    STime.setToNight();
            }
        });

        timeLabel = new Label("00:00", UISkin.i, "large");
        timeLabel.setAlignment(Align.center);
        
        bulletLabel = new Label("0", UISkin.i, "default");
        bulletLabel.setColor(Color.WHITE);

        hpbImage = new Image(Assets.healthBox.getKeyFrames()[0]);
        hpbImage.setSize(64, 64);
        hpbLabel = new Label("x?", UISkin.i, "default");
        hpbLabel.setColor(Color.WHITE);
        
        ambImage = new Image(Assets.ammoBox.getKeyFrames()[0]);
        ambImage.setSize(64, 64);
        ambLabel = new Label("x?", UISkin.i, "default");
        ambLabel.setColor(Color.WHITE);

        pipeBombImage = new Image(Assets.sprites.findRegion("ballistic/pipebomb"));
        pipeBombImage.setSize(64, 64);
        pipeBombLabel = new Label("x?", UISkin.i, "default");
        pipeBombLabel.setColor(Color.WHITE);

        helpText = new Label("It's daytime! Scavenge around for resources, build a fortress,\nor jump directly into the night!", UISkin.i, "large");
        helpText.getColor().a = 0;
        if (!hasShownHelp1) {
            hasShownHelp1 = true;

            AlphaAction a1 = new AlphaAction();
            a1.setAlpha(1);
            a1.setDuration(1);

            AlphaAction a2 = new AlphaAction();
            a2.setAlpha(0);
            a2.setDuration(1);

            helpText.addAction(new SequenceAction(a1, new DelayAction(10), a2));
        }
        helpText.setAlignment(Align.center);

        table.setFillParent(true);
        table.top().left().pad(20);

        for (Actor e : table.getChildren()) {
            e.getColor().a = 0;
            e.addAction(Actions.fadeIn(2));
        }

        Table bottomRow = new Table();
        bottomRow.add(bulletLabel).space(50);
        bottomRow.add(hpbImage).width(32).height(32);
        bottomRow.add(hpbLabel);
        bottomRow.add(ambImage).width(32).height(32);;
        bottomRow.add(ambLabel);
        bottomRow.add(pipeBombImage).width(32).height(32);
        bottomRow.add(pipeBombLabel);
        
        table.add(shopButton).width(150).height(50).top().left();
        table.add(timeLabel).expandX().center();
        table.add(skipButton).width(150).height(50).row();
        table.add(moneyLabel).row();
        table.add(helpText).expandX().height(200).colspan(3).center().bottom().row();
        table.add(new Widget()).expandY().row();
        table.add(bottomRow).expandX().fillX().colspan(3);

        addActor(table);
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if (!STime.isDay()) {
            if (!hasShownHelp2) {
                hasShownHelp2 = true;

                AlphaAction a1 = new AlphaAction();
                a1.setAlpha(1);
                a1.setDuration(1);

                AlphaAction a2 = new AlphaAction();
                a2.setAlpha(0);
                a2.setDuration(1);

                helpText.setText("It's night! Defend yourself against zombies,\nand remember to use your supplies!");
                helpText.addAction(new SequenceAction(a1, new DelayAction(10), a2));
            }
        }

        moneyLabel.setText("$" + prettyPrintFloat(GS.i.money.get()));

        ImmutableArray<Entity> healths = GS.i.getEngine().getEntitiesFor(Family.all(CHealthBox.class).get());
        if (healths.size() > 0) {
            CHealthBox healthBox = healths.first().getComponent(CHealthBox.class);
            hpbLabel.setText("" + healthBox.getCount() + " (Press H)");
        }

        ImmutableArray<Entity> huds = GS.i.getEngine().getEntitiesFor(Family.all(CAmmoHUD.class).get());
        if (huds.size() > 0) {
            CWeapon weapon = huds.first().getComponent(CWeapon.class);
            if (weapon != null && weapon.getWeapon() instanceof Gun) {
                Gun gun = (Gun) weapon.getWeapon();
                bulletLabel.setText(gun.getName() + ": " + gun.getAmmo().getCurrent() + "/" + gun.getAmmo().getTotal());
            }
        }

        ImmutableArray<Entity> pipeBombs = GS.i.getEngine().getEntitiesFor(Family.all(CPipeBombs.class).get());
        if (pipeBombs.size() > 0) {
            CPipeBombs pb = pipeBombs.first().getComponent(CPipeBombs.class);
            pipeBombLabel.setText("" + pb.getCount() + " (Press T)");
        }
        
        ImmutableArray<Entity> ammos = GS.i.getEngine().getEntitiesFor(Family.all(CAmmoBox.class).get());
        if (ammos.size() > 0) {
            CAmmoBox ammoBox = ammos.first().getComponent(CAmmoBox.class);
            ambLabel.setText("" + ammoBox.getCount() + " (Press F)");
        }

        int hour = (int) ((STime.getCurrentTime() * 24 + 11) % 12 + 1);
        boolean pm = STime.getCurrentTime() > 0.5f;

        int minute = (int) ((STime.getCurrentTime() * (24 * 60)) % 60);
        timeLabel.setText(String.format("%d:%02d %s", hour, minute, pm ? "PM" : "AM"));
        
        if(disableShopAtNight) {
	        boolean isDay = STime.isDay();
	        if (!isDay) {
	            shopButton.setTouchable(Touchable.disabled);
	            shopButton.setColor(Color.DARK_GRAY);
	
	            skipButton.setTouchable(Touchable.disabled);
	            skipButton.setVisible(false);
	        } else {
	            shopButton.setTouchable(Touchable.enabled);
	            shopButton.setColor(blue);
	
	            skipButton.setTouchable(Touchable.enabled);
	            skipButton.setVisible(true);
	        }
        }
    }

    private String prettyPrintFloat(float f) {
        if (f == (long) f)
            return String.format("%d", (long) f);
        else
            return String.format("%.2f", f);
    }
}
