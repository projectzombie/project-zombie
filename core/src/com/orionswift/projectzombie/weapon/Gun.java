package com.orionswift.projectzombie.weapon;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Timer;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.event.EDamage;
import com.orionswift.projectzombie.event.EShootBullet;
import com.orionswift.projectzombie.screen.GS;
import com.orionswift.projectzombie.util.Assets;
import com.orionswift.projectzombie.util.SoundEffect;

public class Gun extends Weapon {
    private final float spread;
    private Vector2 muzzleOffset;
    private Ammo ammo;

    public Gun(Entity entity, String name, WeaponSpriteConfig config, float damage, float rate, boolean isAutomatic, float knockback, SoundEffect fireSound, Vector2 muzzleOffset, Ammo ammo, float spread) {
        super(entity, name, config, damage, rate, isAutomatic, knockback, EDamage.KnockbackMode.SourceVelocity, fireSound);
        this.muzzleOffset = muzzleOffset;
        this.ammo = ammo;
        this.spread = spread;
    }

    @Override
    public void _fire() {    	
        if (!ammo.isReloading()) {
            if (ammo.getCapacity() > 0) {
                fire();
                ammo.fire();
                if (fireSound != null) {
                    fireSound.play(entity.getComponent(CBody.class).getPosition());
                }
            } else {
                Assets.outOfAmmo.play(entity.getComponent(CBody.class).getPosition());
            }
        }
    }

    @Override
    public void fire() {
        float spreadOffset = (float) ((Math.random() * spread * 2) - spread);
        GS.i.getBus().publish(new EShootBullet(entity, muzzleOffset, angle + spreadOffset));
    }
    
    public Ammo getAmmo() {
    	return ammo;
    }
}
