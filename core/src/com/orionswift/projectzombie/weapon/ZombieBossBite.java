package com.orionswift.projectzombie.weapon;

import com.badlogic.ashley.core.Entity;
import com.orionswift.projectzombie.event.EDamage;
import com.orionswift.projectzombie.util.EntityCreator;

public class ZombieBossBite extends Melee {
	public ZombieBossBite(Entity entity) {
		super(entity, "Zombie Bite", null, 5, 0.1f, true, 20, EDamage.KnockbackMode.SourcePosition, null, 90, 1, EntityCreator.ZOMBIE_DAMAGE_MASK);
	}
}
