package com.orionswift.projectzombie.weapon;

import com.orionswift.projectzombie.util.Assets;

public class UnlimitedAmmo extends Ammo {

	public UnlimitedAmmo() {
		super(2147483647, 2147483647, 2147483647, Assets.pistolReload);
	}
	
	@Override public void fire() {}
	@Override public void reload() {}
	@Override public void add(int ammo) {}
	@Override public void drop() {}
}
