package com.orionswift.projectzombie.weapon;

import com.badlogic.ashley.core.Entity;
import com.orionswift.projectzombie.event.EDamage;
import com.orionswift.projectzombie.util.EntityCreator;

public class ZombieBite extends Melee {
    public ZombieBite(Entity entity, float damage, float knockback, float hitDistance) {
        super(entity, "Zombie Bite", null, damage, 0.3f, true, knockback, EDamage.KnockbackMode.SourcePosition, null, 90, hitDistance, EntityCreator.ZOMBIE_DAMAGE_MASK);
    }
}
