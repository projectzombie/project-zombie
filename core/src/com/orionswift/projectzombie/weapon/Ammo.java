package com.orionswift.projectzombie.weapon;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Timer;
import com.orionswift.projectzombie.util.Assets;

public class Ammo {
	
	// MAX amount of ammo you can hold
	private int full;
	// MAX amount of ammo the gun can shoot before reloading
	private int capacity;
	// AMOUNT of ammo in capacity left to shoot (current / capacity)
	private int current;
	// AMOUNT of ammo the gun is holding (total / full)
	private int total;
	
    private Timer reloadTimer;
    private float reloadTime;
    private Sound reloadSound;
    private boolean isReloading = false;
    
	public Ammo(int capacity, int full, float reloadTime, Sound reloadSound) {
		this.capacity = capacity;
		this.full = full;
		this.total = full;
		this.current = capacity;
		this.reloadTime = reloadTime;
		this.reloadSound = reloadSound;
	}
	
	public void fire() {
		drop();
        if (current == 0) {
            reload();
        }
	}
	
	public void reload() {
        if (!isReloading && total > 0 && current < capacity) {
            reloadSound.play();

            isReloading = true;
            Timer.instance().scheduleTask(new Timer.Task() {
                @Override
                public void run() {
                    isReloading = false;

                    int toAdd = Math.min(capacity, total);
                    total -= toAdd;
                    current = toAdd;
                }
            }, reloadTime);
        }
	}
	
	public void add(int add) {
		if(add < (capacity - current)) {
			setCurrent(current + add);
		} else {
			int bulletcapacitydiff = (capacity - current);
			int newTotal = MathUtils.clamp(full + (add - bulletcapacitydiff), 0, full + (int) (full * 0.3f));
			setCurrent(capacity);
			setTotal(newTotal);
		}
	}
	
	public void drop() {
		current--;
	}

	public int getFull() {
		return full;
	}

	public void setFull(int full) {
		this.full = full;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public int getCurrent() {
		return current;
	}

	public void setCurrent(int current) {
		this.current = current;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public Timer getReloadTimer() {
		return reloadTimer;
	}

	public void setReloadTimer(Timer reloadTimer) {
		this.reloadTimer = reloadTimer;
	}
	
	public boolean isReloading() {
		return isReloading;
	}
	
	public int getRealTotal() {
		return total + current;
	}
	
	public boolean hasReachedMax() {
		return getRealTotal() >= getFull() + getCapacity();
	}
}
