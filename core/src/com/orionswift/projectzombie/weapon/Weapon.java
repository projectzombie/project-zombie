package com.orionswift.projectzombie.weapon;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.event.EDamage;
import com.orionswift.projectzombie.util.SoundEffect;
import com.orionswift.projectzombie.util.SpriteInfo;

public abstract class Weapon {
    private String name;
    private WeaponSpriteConfig spriteConfig;
    protected float damage;
    protected float rate;
    private boolean isAutomatic;
    protected float knockback;
    protected EDamage.KnockbackMode knockbackMode;
    protected SoundEffect fireSound;
    private SpriteInfo weaponSprite;
    protected Entity entity;

    private float timer;
    protected float angle;
    private boolean firing;
    private boolean hasNonAutomaticFired;

    public Weapon(Entity entity, String name, WeaponSpriteConfig spriteConfig, float damage, float rate, boolean isAutomatic, float knockback, EDamage.KnockbackMode knockbackMode, SoundEffect fireSound) {
        this.entity = entity;
        this.name = name;
        this.spriteConfig = spriteConfig;
        this.damage = damage;
        this.rate = rate;
        this.isAutomatic = isAutomatic;
        this.knockback = knockback;
        this.knockbackMode = knockbackMode;
        this.fireSound = fireSound;

        if (spriteConfig != null) {
            Sprite sprite = new Sprite(spriteConfig.getTexture());
            sprite.setSize(spriteConfig.getSize().x, spriteConfig.getSize().y);
            sprite.setOrigin(spriteConfig.getOrigin().x, spriteConfig.getOrigin().y);

            weaponSprite = new SpriteInfo(sprite, spriteConfig.getOffset(), 0);
        }
    }

    public void _fire() {
        fire();
        if (fireSound != null) {
            fireSound.play(entity.getComponent(CBody.class).getPosition());
        }
    }

    public void update(float deltaTime) {
        if (firing) {
            while (timer <= 0 && !hasNonAutomaticFired) {
                _fire();
                timer += rate;
                if (!isAutomatic) {
                    hasNonAutomaticFired = true;
                }
            }
        }

        timer -= deltaTime;
    }

    public void startFiring() {
        firing = true;

        if (timer <= 0) {
            timer = 0;
        }
        hasNonAutomaticFired = false;
    }

    public void stopFiring() {
        firing = false;
    }

    public abstract void fire();

    public float getDamage() {
        return damage;
    }

    public float getKnockback() {
        return knockback;
    }

    public EDamage.KnockbackMode getKnockbackMode() {
        return knockbackMode;
    }

    public SoundEffect getFireSound() {
        return fireSound;
    }

    public String getName() {
        return name;
    }

    public WeaponSpriteConfig getSpriteConfig() {
        return spriteConfig;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    public SpriteInfo getWeaponSprite() {
        return weaponSprite;
    }

    public boolean isFiring() {
        return firing;
    }

    public Entity getEntity() {
        return entity;
    }
}
