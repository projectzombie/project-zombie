package com.orionswift.projectzombie.weapon;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class WeaponSpriteConfig {
    private TextureRegion texture;
    private Vector2 offset;
    private Vector2 origin;
    private Vector2 size;

    public WeaponSpriteConfig(TextureRegion texture, Vector2 offset, Vector2 origin, Vector2 size) {
        this.texture = texture;
        this.offset = offset;
        this.origin = origin;
        this.size = size;
    }

    public TextureRegion getTexture() {
        return texture;
    }

    public Vector2 getOffset() {
        return offset;
    }

    public Vector2 getOrigin() {
        return origin;
    }

    public Vector2 getSize() {
        return size;
    }
}
