package com.orionswift.projectzombie.weapon;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;
import com.orionswift.projectzombie.util.Assets;

public class TurretGun extends Gun {
    public TurretGun(Entity entity) {
        super(entity, "Turret Gun", null, 0.4f, 0.2f, true, 7, Assets.machineGun, new Vector2(0.5f, 0), new UnlimitedAmmo(), 1);
    }
}