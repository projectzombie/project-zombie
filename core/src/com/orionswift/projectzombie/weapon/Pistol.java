package com.orionswift.projectzombie.weapon;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;
import com.orionswift.projectzombie.util.Assets;

public class Pistol extends Gun {
    public Pistol(Entity entity) {
    	super(entity, "Pistol", Assets.gunConfig, 4f, 0.5f, false, 8, Assets.pistol, new Vector2(0.7f, 0), new Ammo(8, 80, 1, Assets.pistolReload), 1.5f);
    }
}
