package com.orionswift.projectzombie.weapon;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;
import com.orionswift.projectzombie.util.Assets;

public class Shotgun extends Gun {
    public Shotgun(Entity entity) {
        super(entity, "Shotgun", Assets.gunConfig, 1.5f, 1.3f, false, 7, Assets.shotgun, new Vector2(0.5f, 0), new Ammo(6, 30, 6, Assets.shotgunReload), 10);
    }

    @Override
    public void fire() {
        for (int i = 0; i < 8; i++) {
            super.fire();
        }
    }
}
