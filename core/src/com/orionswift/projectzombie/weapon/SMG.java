package com.orionswift.projectzombie.weapon;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;
import com.orionswift.projectzombie.util.Assets;

public class SMG extends Gun {
    public SMG(Entity entity) {
        super(entity, "SMG", Assets.gunConfig, 0.9f, 0.13f, true, 4, Assets.machineGun, new Vector2(0.46f, 0), new Ammo(60, 240, 3, Assets.machineGunReload), 4);
    }
}
