package com.orionswift.projectzombie.weapon;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.QueryCallback;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CHealth;
import com.orionswift.projectzombie.event.EDamage;
import com.orionswift.projectzombie.screen.GS;
import com.orionswift.projectzombie.util.SoundEffect;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Melee extends Weapon {
    private float coneAngle;
    private float coneDistance;
    private int hitMask;

    public Melee(Entity entity, String name, WeaponSpriteConfig config, float damage, float rate, boolean isAutomatic, float knockback, EDamage.KnockbackMode knockbackMode, SoundEffect fireSound, float coneAngle, float coneDistance, int hitMask) {
        super(entity, name, config, damage, rate, isAutomatic, knockback, knockbackMode, fireSound);
        this.coneAngle = coneAngle;
        this.coneDistance = coneDistance;
        this.hitMask = hitMask;
    }

    @Override
    public void fire() {
        final CBody body = entity.getComponent(CBody.class);

        final Set<Body> bodiesDone = new HashSet<>();
        final List<Entity> affected = new ArrayList<>();
        GS.i.getWorld().QueryAABB(new QueryCallback() {
            @Override
            public boolean reportFixture(Fixture fixture) {
                Body entityBody = fixture.getBody();

                if ((fixture.getFilterData().categoryBits & hitMask) > 0) {
                    if (!bodiesDone.contains(entityBody)) {
                        bodiesDone.add(entityBody);

                        if (entityBody.getUserData() instanceof Entity) {
                            if (entityBody.getPosition().dst2(body.getPosition()) < coneDistance * coneDistance) {
                                float targetAngle = entityBody.getPosition().cpy().sub(body.getPosition()).angle(new Vector2(0, 1).rotate(angle - coneAngle / 2));
                                if (targetAngle <= coneAngle && targetAngle >= 0) {
                                    Entity target = (Entity) entityBody.getUserData();
                                    if (target != entity) {
                                        if (target.getComponent(CHealth.class) != null) {
                                            affected.add(target);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return true;
            }
        }, body.getPosition().x - coneDistance, body.getPosition().y - coneDistance, body.getPosition().x + coneDistance, body.getPosition().y + coneDistance);

        for (Entity e : affected) {
            GS.i.getBus().publish(new EDamage(e, entity, entity, damage, knockback, EDamage.KnockbackMode.SourcePosition, EDamage.DamageType.Melee));
        }
    }
}
