package com.orionswift.projectzombie.wave;

import com.orionswift.projectzombie.util.WeightedRandom;
import com.orionswift.projectzombie.util.ZombieType;

public class WaveConfiguration {
    private int zombieCount;
    private float multiplierMultiplier;
    private WeightedRandom<ZombieType> randomType;

    public WaveConfiguration(int zombieCount, float multiplierMultiplier, WeightedRandom<ZombieType> randomType) {
        this.zombieCount = zombieCount;
        this.multiplierMultiplier = multiplierMultiplier;
        this.randomType = randomType;
    }

    public int getZombieCount() {
        return zombieCount;
    }

    public float getMultiplierMultiplier() {
        return multiplierMultiplier;
    }

    public WeightedRandom<ZombieType> getRandomType() {
        return randomType;
    }
}