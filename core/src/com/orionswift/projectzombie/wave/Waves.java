package com.orionswift.projectzombie.wave;

import java.util.HashMap;
import java.util.Map;

public class Waves {
    private Map<Integer, WaveConfiguration> wavePoints = new HashMap<>();

    public WaveConfiguration getWaveConfiguration(int wave) {
        while (!wavePoints.containsKey(wave)) wave--;
        return wavePoints.get(wave);
    }

    public int getZombieCount(int wave) {
        int waveCtr = wave;
        while (!wavePoints.containsKey(waveCtr)) waveCtr--;
        WaveConfiguration point = wavePoints.get(waveCtr);

        return (int) (point.getZombieCount() * Math.pow(1.1f, (wave - waveCtr)));
    }

    public Waves addWave(int wave, WaveConfiguration waveConfiguration) {
        wavePoints.put(wave, waveConfiguration);
        return this;
    }
}
