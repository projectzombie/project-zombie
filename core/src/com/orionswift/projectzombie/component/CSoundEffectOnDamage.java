package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;
import com.orionswift.projectzombie.util.SoundEffect;

public class CSoundEffectOnDamage implements Component {
    private SoundEffect bulletSound;
    private SoundEffect meleeSound;

    public CSoundEffectOnDamage(SoundEffect bulletSound, SoundEffect meleeSound) {
        this.bulletSound = bulletSound;
        this.meleeSound = meleeSound;
    }

    public SoundEffect getBulletSound() {
        return bulletSound;
    }

    public SoundEffect getMeleeSound() {
        return meleeSound;
    }
}
