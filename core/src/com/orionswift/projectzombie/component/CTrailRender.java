package com.orionswift.projectzombie.component;

import java.util.ArrayList;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

public class CTrailRender implements Component {
    private float time;
    private ArrayList<TrailPos> trailPositions;
    private float topWidth;
    private float bottomWidth;
    private Color topColor;
    private Color bottomColor;

    public CTrailRender(float time, float topWidth, float bottomWidth, Color topColor, Color bottomColor) {
        this.time = time;
        this.topWidth = topWidth;
        this.bottomWidth = bottomWidth;
        this.topColor = topColor;
        this.bottomColor = bottomColor;
        trailPositions = new ArrayList<>();
    }

    public float getTime() {
        return time;
    }


    public Color getTopColor() {
        return topColor;
    }

    public Color getBottomColor() {
        return bottomColor;
    }

    public float getTopWidth() {
        return topWidth;
    }

    public float getBottomWidth() {
        return bottomWidth;
    }

    public ArrayList<TrailPos> getTrailPositions() {
        return trailPositions;
    }

    public static class TrailPos {
        private long milliTime;
        private Vector2 pos;

        public TrailPos(long milliTime, Vector2 pos) {
            this.milliTime = milliTime;
            this.pos = pos;
        }

        public long getMilliTime() {
            return milliTime;
        }

        public Vector2 getPos() {
            return pos;
        }
    }
}
