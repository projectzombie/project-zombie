package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;

public class CShootOnAggroProximity implements Component {
    private float distance;

    public CShootOnAggroProximity(float distance) {
        this.distance = distance;
    }

    public float getDistance() {
        return distance;
    }
}
