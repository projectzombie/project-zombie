package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.Sprite;
import net.dermetfan.gdx.graphics.g2d.AnimatedSprite;

public class CPlayerLegTorsoControls implements Component {
    private Sprite legs;
    private Sprite torso;
    private Sprite gun;
    private Sprite head;

    public CPlayerLegTorsoControls(AnimatedSprite legs, Sprite torso, Sprite gun, Sprite head) {
        this.legs = legs;
        this.torso = torso;
        this.gun = gun;
        this.head = head;
    }

    public Sprite getTorso() {
        return torso;
    }

    public void setTorso(Sprite torso) {
        this.torso = torso;
    }

    public Sprite getLegs() {
        return legs;
    }

    public void setLegs(Sprite legs) {
        this.legs = legs;
    }

    public Sprite getGun() {
        return gun;
    }

    public void setGun(Sprite gun) {
        this.gun = gun;
    }

    public Sprite getHead() {
        return head;
    }

    public void setHead(Sprite head) {
        this.head = head;
    }
}
