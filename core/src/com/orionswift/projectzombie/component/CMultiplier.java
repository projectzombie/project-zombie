package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;

public class CMultiplier implements Component {
    private float damage;

    public CMultiplier() {
        damage = 1;
    }

    public float getDamage() {
        return damage;
    }

    public void setDamage(float damage) {
        this.damage = damage;
    }
}
