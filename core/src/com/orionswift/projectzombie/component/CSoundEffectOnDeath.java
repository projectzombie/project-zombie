package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;
import com.orionswift.projectzombie.util.SoundEffect;

public class CSoundEffectOnDeath implements Component {
    private SoundEffect sound;

    public CSoundEffectOnDeath(SoundEffect sound) {
        this.sound = sound;
    }

    public SoundEffect getSound() {
        return sound;
    }
}
