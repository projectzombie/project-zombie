package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;
import com.orionswift.projectzombie.weapon.Weapon;

public class CWeapon implements Component {
    private Weapon weapon;

    public CWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }
}
