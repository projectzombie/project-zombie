package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.FloatArray;

import net.dermetfan.gdx.math.MathUtils;

public class CAbsorbHealthOnDamage implements Component {
	private float percentage = 0;
	
	public CAbsorbHealthOnDamage(float percentage) {
		this.percentage = MathUtils.clamp(percentage, 0, 1);
	}

	public float getPercentage() {
		return percentage;
	}

	public void setPercentage(float percentage) {
		this.percentage = percentage;
	}
}
