package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;

public class CExplodeSoon implements Component {
    private float time;
    private float damage;

    public CExplodeSoon(float time, float damage) {
        this.time = time;
        this.damage = damage;
    }

    public float getTime() {
        return time;
    }

    public void setTime(float time) {
        this.time = time;
    }
    
    public float getDamage() {
    	return damage;
    }
}
