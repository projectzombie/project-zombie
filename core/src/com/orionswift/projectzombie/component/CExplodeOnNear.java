package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;

public class CExplodeOnNear implements Component {
	private Entity source;
	private float damage;
	private float radius;
	
	public CExplodeOnNear(Entity source, float damage, float radius) {
		this.source = source;
		this.damage = damage;
		this.radius = radius;
	}

	public Entity getSource() {
		return source;
	}
	
	public float getDamage() {
		return damage;
	}
	
	public void setDamage(float damage) {
		this.damage = damage;
	}
	
	public float getRadius() {
		return radius;
	}
	
	public void setRadius(float radius) {
		this.radius = radius;
	}
}
