package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.google.common.base.Optional;

import java.util.BitSet;

public class CAggro implements Component {
    private Optional<Entity> aggro;
    private boolean requireLineOfSight;
    private BitSet mask;

    public CAggro(boolean requireLineOfSight, BitSet mask) {
        this.requireLineOfSight = requireLineOfSight;
        this.mask = mask;
        aggro = Optional.absent();
    }

    public Optional<Entity> getAggro() {
        return aggro;
    }

    public void setAggro(Optional<Entity> aggro) {
        this.aggro = aggro;
    }

    public BitSet getMask() {
        return mask;
    }

    public boolean isRequireLineOfSight() {
        return requireLineOfSight;
    }
}
