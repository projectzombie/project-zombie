package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;

public class CBody implements Component {
    private Body body;

    public CBody(Body body) {
        this.body = body;
    }

    public Body getBody() {
        return body;
    }

    // DELEGATE STUFF UNDER THIS
    // DELEGATE STUFF UNDER THIS
    // DELEGATE STUFF UNDER THIS


    /** Creates a fixture and attach it to this body. Use this function if you need to set some fixture parameters, like friction.
     * Otherwise you can create the fixture directly from a shape. If the density is non-zero, this function automatically updates
     * the mass of the body. Contacts are not created until the next time step.
     * @param def the fixture definition.
     * @warning This function is locked during callbacks. */
    public Fixture createFixture(FixtureDef def) {
        return body.createFixture(def);
    }

    /** Set the mass properties to override the mass properties of the fixtures. Note that this changes the center of mass position.
     * Note that creating or destroying fixtures can also alter the mass. This function has no effect if the body isn't dynamic.
     * @param data the mass properties. */
    public void setMassData(MassData data) {
        body.setMassData(data);
    }

    /** Gets a local point relative to the body's origin given a world point.
     * Note that the same Vector2 instance is returned each time this method is called.
     * @param worldPoint a point in world coordinates.
     * @return the corresponding local point relative to the body's origin. */
    public Vector2 getLocalPoint(Vector2 worldPoint) {
        return body.getLocalPoint(worldPoint);
    }

    /** Set the linear velocity of the center of mass.
     * @param v*/
    public void setLinearVelocity(Vector2 v) {
        body.setLinearVelocity(v);
    }

    /** Get the list of all fixtures attached to this body. Do not modify the list! */
    public Array<Fixture> getFixtureList() {
        return body.getFixtureList();
    }

    /** Apply an angular impulse.
     * @param impulse the angular impulse in units of kg*m*m/s
     * @param wake*/
    public void applyAngularImpulse(float impulse, boolean wake) {
        body.applyAngularImpulse(impulse, wake);
    }

    /** Get the parent world of this body. */
    public World getWorld() {
        return body.getWorld();
    }

    /** Set the linear damping of the body.
     * @param linearDamping*/
    public void setLinearDamping(float linearDamping) {
        body.setLinearDamping(linearDamping);
    }

    /** Apply a force at a world point. If the force is not applied at the center of mass, it will generate a torque and affect the
     * angular velocity. This wakes up the body.
     * @param forceX the world force vector on x, usually in Newtons (N).
     * @param forceY the world force vector on y, usually in Newtons (N).
     * @param pointX the world position of the point of application on x.
     * @param pointY the world position of the point of application on y.
     * @param wake up the body  */
    public void applyForce(float forceX, float forceY, float pointX, float pointY, boolean wake) {
        body.applyForce(forceX, forceY, pointX, pointY, wake);
    }

    /** Is this body treated like a bullet for continuous collision detection? */
    public boolean isBullet() {
        return body.isBullet();
    }

    /** Set the position of the body's origin and rotation. This breaks any contacts and wakes the other bodies. Manipulating a
     * body's transform may cause non-physical behavior.
     * @param position the world position of the body's local origin.
     * @param angle the world rotation in radians. */
    public void setTransform(Vector2 position, float angle) {
        body.setTransform(position, angle);
    }

    /** Apply an impulse at a point. This immediately modifies the velocity. It also modifies the angular velocity if the point of
     * application is not at the center of mass. This wakes up the body.
     * @param impulseX the world impulse vector on the x-axis, usually in N-seconds or kg-m/s.
     * @param impulseY the world impulse vector on the y-axis, usually in N-seconds or kg-m/s.
     * @param pointX the world position of the point of application on the x-axis.
     * @param pointY the world position of the point of application on the y-axis.
     * @param wake up the body  */
    public void applyLinearImpulse(float impulseX, float impulseY, float pointX, float pointY, boolean wake) {
        body.applyLinearImpulse(impulseX, impulseY, pointX, pointY, wake);
    }

    /** Get the list of all joints attached to this body. Do not modify the list! */
    public Array<JointEdge> getJointList() {
        return body.getJointList();
    }

    /** Set the linear velocity of the center of mass.
     * @param vX
     * @param vY*/
    public void setLinearVelocity(float vX, float vY) {
        body.setLinearVelocity(vX, vY);
    }

    /** You can disable sleeping on this body. If you disable sleeping, the
     * @param flag*/
    public void setSleepingAllowed(boolean flag) {
        body.setSleepingAllowed(flag);
    }

    /** Get the user data */
    public Object getUserData() {
        return body.getUserData();
    }

    /** Apply a force to the center of mass. This wakes up the body.
     * @param forceX the world force vector, usually in Newtons (N).
     * @param forceY the world force vector, usually in Newtons (N).
     * @param wake*/
    public void applyForceToCenter(float forceX, float forceY, boolean wake) {
        body.applyForceToCenter(forceX, forceY, wake);
    }

    /** Apply a force at a world point. If the force is not applied at the center of mass, it will generate a torque and affect the
     * angular velocity. This wakes up the body.
     * @param force the world force vector, usually in Newtons (N).
     * @param point the world position of the point of application.
     * @param wake up the body  */
    public void applyForce(Vector2 force, Vector2 point, boolean wake) {
        body.applyForce(force, point, wake);
    }

    /** Set this body to have fixed rotation. This causes the mass to be reset.
     * @param flag*/
    public void setFixedRotation(boolean flag) {
        body.setFixedRotation(flag);
    }

    /** This resets the mass properties to the sum of the mass properties of the fixtures. This normally does not need to be called
     * unless you called SetMassData to override the mass and you later want to reset the mass. */
    public void resetMassData() {
        body.resetMassData();
    }

    /** Get the world linear velocity of a world point attached to this body.
     * Note that the same Vector2 instance is returned each time this method is called.
     * @param worldPoint a point in world coordinates.
     * @return the world velocity of a point. */
    public Vector2 getLinearVelocityFromWorldPoint(Vector2 worldPoint) {
        return body.getLinearVelocityFromWorldPoint(worldPoint);
    }

    /** Get the active state of the body. */
    public boolean isActive() {
        return body.isActive();
    }

    /** Set the active state of the body. An inactive body is not simulated and cannot be collided with or woken up. If you pass a
     * flag of true, all fixtures will be added to the broad-phase. If you pass a flag of false, all fixtures will be removed from
     * the broad-phase and all contacts will be destroyed. Fixtures and joints are otherwise unaffected. You may continue to
     * create/destroy fixtures and joints on inactive bodies. Fixtures on an inactive body are implicitly inactive and will not
     * participate in collisions, ray-casts, or queries. Joints connected to an inactive body are implicitly inactive. An inactive
     * body is still owned by a b2World object and remains in the body list.
     * @param flag*/
    public void setActive(boolean flag) {
        body.setActive(flag);
    }

    /** Set the angular damping of the body.
     * @param angularDamping*/
    public void setAngularDamping(float angularDamping) {
        body.setAngularDamping(angularDamping);
    }

    /** Get the sleeping state of this body.
     * @return true if the body is sleeping. */
    public boolean isAwake() {
        return body.isAwake();
    }

    /** Set the user data
     * @param userData*/
    public void setUserData(Object userData) {
        body.setUserData(userData);
    }

    /** Is this body allowed to sleep */
    public boolean isSleepingAllowed() {
        return body.isSleepingAllowed();
    }

    /** Gets a local vector given a world vector.
     * Note that the same Vector2 instance is returned each time this method is called.
     * @param worldVector a vector in world coordinates.
     * @return the corresponding local vector. */
    public Vector2 getLocalVector(Vector2 worldVector) {
        return body.getLocalVector(worldVector);
    }

    /** Creates a fixture from a shape and attach it to this body. This is a convenience function. Use b2FixtureDef if you need to
     * set parameters like friction, restitution, user data, or filtering. If the density is non-zero, this function automatically
     * updates the mass of the body.
     * @param shape the shape to be cloned.
     * @param density the shape density (set to zero for static bodies).
     * @warning This function is locked during callbacks. */
    public Fixture createFixture(Shape shape, float density) {
        return body.createFixture(shape, density);
    }

    /** Get the angle in radians.
     * @return the current world rotation angle in radians. */
    public float getAngle() {
        return body.getAngle();
    }

    /** Set the position of the body's origin and rotation. This breaks any contacts and wakes the other bodies. Manipulating a
     * body's transform may cause non-physical behavior.
     * @param x the world position on the x-axis
     * @param y the world position on the y-axis
     * @param angle the world rotation in radians.  */
    public void setTransform(float x, float y, float angle) {
        body.setTransform(x, y, angle);
    }

    /** Set the type of this body. This may alter the mass and velocity.
     * @param type*/
    public void setType(BodyDef.BodyType type) {
        body.setType(type);
    }

    /** Get the world position of the center of mass.
     * Note that the same Vector2 instance is returned each time this method is called. */
    public Vector2 getWorldCenter() {
        return body.getWorldCenter();
    }

    /** Destroy a fixture. This removes the fixture from the broad-phase and destroys all contacts associated with this fixture.
     * This will automatically adjust the mass of the body if the body is dynamic and the fixture has positive density. All
     * fixtures attached to a body are implicitly destroyed when the body is destroyed.
     * @param fixture the fixture to be removed.
     * @warning This function is locked during callbacks. */
    public void destroyFixture(Fixture fixture) {
        body.destroyFixture(fixture);
    }

    /** Apply an impulse at a point. This immediately modifies the velocity. It also modifies the angular velocity if the point of
     * application is not at the center of mass. This wakes up the body.
     * @param impulse the world impulse vector, usually in N-seconds or kg-m/s.
     * @param point the world position of the point of application.
     * @param wake up the body*/
    public void applyLinearImpulse(Vector2 impulse, Vector2 point, boolean wake) {
        body.applyLinearImpulse(impulse, point, wake);
    }

    /** Set the sleep state of the body. A sleeping body has very low CPU cost.
     * @param flag set to true to wake the body, false to put it to sleep. */
    public void setAwake(boolean flag) {
        body.setAwake(flag);
    }

    /** Get the angular damping of the body. */
    public float getAngularDamping() {
        return body.getAngularDamping();
    }

    /** Get the type of this body. */
    public BodyDef.BodyType getType() {
        return body.getType();
    }

    /** Get the linear velocity of the center of mass.
     * Note that the same Vector2 instance is returned each time this method is called. */
    public Vector2 getLinearVelocity() {
        return body.getLinearVelocity();
    }

    /** Get the rotational inertia of the body about the local origin.
     * @return the rotational inertia, usually in kg-m^2. */
    public float getInertia() {
        return body.getInertia();
    }

    /** Apply a force to the center of mass. This wakes up the body.
     * @param force the world force vector, usually in Newtons (N).
     * @param wake*/
    public void applyForceToCenter(Vector2 force, boolean wake) {
        body.applyForceToCenter(force, wake);
    }

    /** Get the world coordinates of a vector given the local coordinates.
     * Note that the same Vector2 instance is returned each time this method is called.
     * @param localVector a vector fixed in the body.
     * @return the same vector expressed in world coordinates. */
    public Vector2 getWorldVector(Vector2 localVector) {
        return body.getWorldVector(localVector);
    }

    /** Get the body transform for the body's origin. */
    public Transform getTransform() {
        return body.getTransform();
    }

    /** Apply a torque. This affects the angular velocity without affecting the linear velocity of the center of mass. This wakes up
     * the body.
     * @param torque about the z-axis (out of the screen), usually in N-m.
     * @param wake up the body */
    public void applyTorque(float torque, boolean wake) {
        body.applyTorque(torque, wake);
    }

    /** Get the mass data of the body.
     * @return a struct containing the mass, inertia and center of the body. */
    public MassData getMassData() {
        return body.getMassData();
    }

    /** Get the world velocity of a local point.
     * Note that the same Vector2 instance is returned each time this method is called.
     * @param localPoint a point in local coordinates.
     * @return the world velocity of a point. */
    public Vector2 getLinearVelocityFromLocalPoint(Vector2 localPoint) {
        return body.getLinearVelocityFromLocalPoint(localPoint);
    }

    /** @return Get the gravity scale of the body. */
    public float getGravityScale() {
        return body.getGravityScale();
    }

    /** Set the angular velocity.
     * @param omega*/
    public void setAngularVelocity(float omega) {
        body.setAngularVelocity(omega);
    }

    /** Get the total mass of the body.
     * @return the mass, usually in kilograms (kg). */
    public float getMass() {
        return body.getMass();
    }

    /** Should this body be treated like a bullet for continuous collision detection?
     * @param flag*/
    public void setBullet(boolean flag) {
        body.setBullet(flag);
    }

    /** Sets the gravity scale of the body
     * @param scale*/
    public void setGravityScale(float scale) {
        body.setGravityScale(scale);
    }

    /** Get the linear damping of the body. */
    public float getLinearDamping() {
        return body.getLinearDamping();
    }

    /** Get the world coordinates of a point given the local coordinates.
     * Note that the same Vector2 instance is returned each time this method is called.
     * @param localPoint a point on the body measured relative the the body's origin.
     * @return the same point expressed in world coordinates. */
    public Vector2 getWorldPoint(Vector2 localPoint) {
        return body.getWorldPoint(localPoint);
    }

    /** Does this body have fixed rotation? */
    public boolean isFixedRotation() {
        return body.isFixedRotation();
    }

    /** Get the world body origin position.
     * Note that the same Vector2 instance is returned each time this method is called.
     * @return the world position of the body's origin. */
    public Vector2 getPosition() {
        return body.getPosition();
    }

    /** Get the angular velocity. */
    public float getAngularVelocity() {
        return body.getAngularVelocity();
    }

    /** Get the local position of the center of mass.
     * Note that the same Vector2 instance is returned each time this method is called. */
    public Vector2 getLocalCenter() {
        return body.getLocalCenter();
    }
}
