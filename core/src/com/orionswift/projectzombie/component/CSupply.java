package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;
import com.orionswift.projectzombie.util.SupplyType;

public class CSupply implements Component {
    private SupplyType type;

    public CSupply(SupplyType type) {
        this.type = type;
    }

    public SupplyType getType() {
        return type;
    }
}
