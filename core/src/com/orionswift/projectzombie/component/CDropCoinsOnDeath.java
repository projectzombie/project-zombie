package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;

public class CDropCoinsOnDeath implements Component {
	private float coins;
	
	public CDropCoinsOnDeath(float coins) {
		this.coins = coins;
	}

	public float getCoins() {
		return coins;
	}

	public void setCoins(float coins) {
		this.coins = coins;
	}
	
}
