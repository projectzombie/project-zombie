package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;
import com.orionswift.projectzombie.weapon.Weapon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CWeaponStash implements Component {
    private List<Weapon> ownedWeapons;
    private int weaponIndex;

    public CWeaponStash(int weaponIndex, Weapon... ownedWeapons) {
        this.weaponIndex = weaponIndex;
        this.ownedWeapons = new ArrayList<>(Arrays.asList(ownedWeapons));
    }

    public List<Weapon> getOwnedWeapons() {
        return ownedWeapons;
    }

    public int getWeaponIndex() {
        return weaponIndex;
    }

    public void setWeaponIndex(int weaponIndex) {
        this.weaponIndex = weaponIndex;
    }
}
