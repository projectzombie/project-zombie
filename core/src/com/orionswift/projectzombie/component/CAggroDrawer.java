package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;

public class CAggroDrawer implements Component {
    private int maskIndex;

    public CAggroDrawer(int maskIndex) {
        this.maskIndex = maskIndex;
    }

    /**
     * Returns this entity's index in the aggro BitSet (similar to box2d's category)
     */
    public int getMaskIndex() {
        return maskIndex;
    }
}
