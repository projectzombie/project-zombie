package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;
import com.orionswift.projectzombie.util.SoundEffect;

public class CStepSound implements Component {
    private SoundEffect sound;

    public CStepSound(SoundEffect sound) {
        this.sound = sound;
    }

    public SoundEffect getSound() {
        return sound;
    }
}
