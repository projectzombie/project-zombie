package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.orionswift.projectzombie.event.EDamage;

public class CDamageOnTouch implements Component {
    private float damage;
    private float knockback;
    private EDamage.KnockbackMode knockbackMode;
    private EDamage.DamageType damageType;
    private Entity source;

    public CDamageOnTouch(float damage, float knockback, EDamage.KnockbackMode knockbackMode, EDamage.DamageType damageType, Entity source) {
        this.damage = damage;
        this.knockback = knockback;
        this.knockbackMode = knockbackMode;
        this.damageType = damageType;
        this.source = source;
    }

    public float getDamage() {
        return damage;
    }

    public float getKnockback() {
        return knockback;
    }

    public EDamage.KnockbackMode getKnockbackMode() {
        return knockbackMode;
    }

    public EDamage.DamageType getDamageType() {
        return damageType;
    }

    public Entity getSource() {
        return source;
    }
}
