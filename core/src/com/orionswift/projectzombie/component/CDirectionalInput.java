package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;

public class CDirectionalInput implements Component {
    private Vector2 moveDirection = new Vector2();

    public CDirectionalInput() {
    }

    public Vector2 getMoveDirection() {
        return moveDirection;
    }
}
