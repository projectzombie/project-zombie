package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;

public class CRenderHealthBar implements Component {
    private float width;

    public CRenderHealthBar(float width) {
        this.width = width;
    }

    public float getWidth() {
        return width;
    }
}
