package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;

public class CPlayAnimationOnInput implements Component {
    private String animation;
    private int priority;

    private boolean isMoving;

    public CPlayAnimationOnInput(String animation, int priority) {
        this.animation = animation;
        this.priority = priority;
    }

    public String getAnimation() {
        return animation;
    }

    public int getPriority() {
        return priority;
    }

    public boolean isMoving() {
        return isMoving;
    }

    public void setMoving(boolean moving) {
        isMoving = moving;
    }
}
