package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;

public class CAmmoBox implements Component {
    private int count;

    public CAmmoBox(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
