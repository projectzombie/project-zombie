package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;

public class CRepel implements Component {
    private float radius;

    public CRepel(float radius) {
        this.radius = radius;
    }

    public float getRadius() {
        return radius;
    }
}
