package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;

public class CMovement implements Component {
    private float speed;
    private float groundFriction;
    private Vector2 springVelocity = new Vector2();

    public CMovement(float speed, float groundFriction) {
        this.speed = speed;
        this.groundFriction = groundFriction;
    }

    public float getSpeed() {
        return speed;
    }

    public float getGroundFriction() {
        return groundFriction;
    }

    public Vector2 getSpringVelocity() {
        return springVelocity;
    }
}
