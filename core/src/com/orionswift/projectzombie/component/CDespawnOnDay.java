package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;

public class CDespawnOnDay implements Component {
    private float timeOffset = (float) (Math.random());

    public float getTimeOffset() {
        return timeOffset;
    }
}
