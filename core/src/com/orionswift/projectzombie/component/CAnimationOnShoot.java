package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;

public class CAnimationOnShoot implements Component {
    private String animation;
    private int priority;

    public CAnimationOnShoot(String animation, int priority) {
        this.animation = animation;
        this.priority = priority;
    }

    public String getAnimation() {
        return animation;
    }

    public int getPriority() {
        return priority;
    }
}
