package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;

public class CRaycastCollision implements Component {
    private short mask;
    private Vector2 oldPos;

    public CRaycastCollision(short mask, Vector2 oldPos) {
        this.mask = mask;
        this.oldPos = oldPos;
    }

    public short getMask() {
        return mask;
    }

    public Vector2 getOldPos() {
        return oldPos;
    }
}
