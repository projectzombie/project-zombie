package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.orionswift.projectzombie.util.SpriteInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class CRender implements Component {
	private List<SpriteInfo> sprites;

    public CRender(SpriteInfo... sprite) {
        sprites = new ArrayList<>(Arrays.asList(sprite));
    }

    public List<SpriteInfo> getSprites() {
        return sprites;
    }
}
