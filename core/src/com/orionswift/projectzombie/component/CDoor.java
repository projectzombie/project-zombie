package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJoint;

public class CDoor implements Component {
    private Body anchorBody;
    private RevoluteJoint joint;
    private float openProximity;
    private boolean vertical;

    public CDoor(Body anchorBody, RevoluteJoint joint, float openProximity, boolean vertical) {
        this.anchorBody = anchorBody;
        this.joint = joint;
        this.openProximity = openProximity;
        this.vertical = vertical;
    }

    public Body getAnchorBody() {
        return anchorBody;
    }

    public RevoluteJoint getJoint() {
        return joint;
    }

    public float getOpenProximity() {
        return openProximity;
    }

    public boolean isVertical() {
        return vertical;
    }
}
