package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Timer;
import com.orionswift.projectzombie.util.SoundEffect;

public class CRepeatSoundEffectRandomly implements Component {
	// Random interval between sounds min and max 
	private float[] range = {0, 0};
	private float randomTime = 0;
	private SoundEffect soundEffect;
	private boolean isPlaying = false;
	
	public CRepeatSoundEffectRandomly(float min, float max, SoundEffect soundEffect) {
		this.range[0] = min;
		this.range[1] = max;
		this.soundEffect = soundEffect;
	}

	public float[] getRange() {
		return range;
	}
	
	public void setRange(float[] range) {
		this.range = range;
	}
	
	public float getRandomTime() {
		return randomTime;
	}
	
	public void setRandomTime(float randomTime) {
		this.randomTime = randomTime;
	}
	
	public SoundEffect getSoundEffect() {
		return soundEffect;
	}

	public void setSoundEffect(SoundEffect soundEffect) {
		this.soundEffect = soundEffect;
	}

	public boolean isPlaying() {
		return isPlaying;
	}

	public void setPlaying(boolean isPlaying) {
		this.isPlaying = isPlaying;
	}
}
