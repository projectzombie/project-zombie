package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.Input.Buttons;

public class CPlayerInput implements Component {
	private int upKey;
	private int leftKey;
	private int downKey;
	private int rightKey;

	public CPlayerInput(int upKey, int leftKey, int downKey, int rightKey) {
		this.upKey = upKey;
		this.leftKey = leftKey;
		this.downKey = downKey;
		this.rightKey = rightKey;
	}

	public int getUpKey() {
		return upKey;
	}

	public void setUpKey(int upKey) {
		this.upKey = upKey;
	}

	public int getLeftKey() {
		return leftKey;
	}

	public void setLeftKey(int leftKey) {
		this.leftKey = leftKey;
	}

	public int getDownKey() {
		return downKey;
	}

	public void setDownKey(int downKey) {
		this.downKey = downKey;
	}

	public int getRightKey() {
		return rightKey;
	}

	public void setRightKey(int rightKey) {
		this.rightKey = rightKey;
	}
}
