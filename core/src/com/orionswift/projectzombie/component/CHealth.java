package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;

public class CHealth implements Component {
    private float health;
    private float maxHealth;

    public CHealth(float health, float maxHealth) {
        this.health = health;
        this.maxHealth = maxHealth;
    }
    
    public void refill() {
    	health = maxHealth;
    }
    
    public float getHealth() {
        return health;
    }

    public float getMaxHealth() {
        return maxHealth;
    }

    public void setHealth(float health) {
        this.health = health;
    }
}
