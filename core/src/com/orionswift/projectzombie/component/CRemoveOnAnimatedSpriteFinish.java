package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;
import net.dermetfan.gdx.graphics.g2d.AnimatedSprite;

public class CRemoveOnAnimatedSpriteFinish implements Component {
    private AnimatedSprite sprite;

    public CRemoveOnAnimatedSpriteFinish(AnimatedSprite sprite) {
        this.sprite = sprite;
    }

    public AnimatedSprite getSprite() {
        return sprite;
    }
}
