package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;

import java.util.BitSet;

public class CIgnoreDamage implements Component {
    private int group;
    private BitSet mask;

    public CIgnoreDamage(int group, BitSet mask) {
        this.group = group;
        this.mask = mask;
    }

    public int getGroup() {
        return group;
    }

    public BitSet getMask() {
        return mask;
    }
}
