package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;

public class CCameraFollow implements Component {
    private float dampingRatio;
    private float angularFrequency;

    public CCameraFollow(float dampingRatio, float angularFrequency) {
        this.dampingRatio = dampingRatio;
        this.angularFrequency = angularFrequency;
    }

    public float getDampingRatio() {
        return dampingRatio;
    }

    public float getAngularFrequency() {
        return angularFrequency;
    }
}
