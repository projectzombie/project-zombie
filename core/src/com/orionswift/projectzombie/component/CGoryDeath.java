package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;

public class CGoryDeath implements Component {
    private float size;

    public CGoryDeath(float size) {
        this.size = size;
    }

    public float getSize() {
        return size;
    }
}
