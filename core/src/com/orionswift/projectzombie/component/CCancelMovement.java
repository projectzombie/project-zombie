package com.orionswift.projectzombie.component;

import com.badlogic.ashley.core.Component;

public class CCancelMovement implements Component {
    private float duration;
    private float timer;
    private CMovement movement;

    public CCancelMovement(float duration, float timer, CMovement movement) {
        this.duration = duration;
        this.timer = timer;
        this.movement = movement;
    }

    public float getDuration() {
        return duration;
    }

    public float getTimer() {
        return timer;
    }

    public void setTimer(float timer) {
        this.timer = timer;
    }

    public CMovement getMovement() {
        return movement;
    }
}
