package com.orionswift.projectzombie.util.hack;

import com.orionswift.projectzombie.event.Event;
import net.engio.mbassy.bus.IMessagePublication;
import net.engio.mbassy.dispatch.IHandlerInvocation;
import net.engio.mbassy.dispatch.MessageDispatcher;
import net.engio.mbassy.subscription.SubscriptionContext;

// Dirty, dirty hack to prevent MBassador from delivering cancelled events
public class MessageDispatcherHack extends MessageDispatcher {
    public MessageDispatcherHack(SubscriptionContext context, IHandlerInvocation invocation) {
        super(context, invocation);
    }

    @Override
    public void dispatch(IMessagePublication publication, Object message, Iterable listeners) {
        publication.markDelivered();
        for (Object listener : listeners) {
            if (!(message instanceof Event) || !((Event) message).isCancelled()) {
                getInvocation().invoke(listener, message);
            }
        }
    }
}
