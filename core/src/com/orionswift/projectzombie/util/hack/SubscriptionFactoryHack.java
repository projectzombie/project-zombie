package com.orionswift.projectzombie.util.hack;

import net.engio.mbassy.dispatch.EnvelopedMessageDispatcher;
import net.engio.mbassy.dispatch.FilteredMessageDispatcher;
import net.engio.mbassy.dispatch.IHandlerInvocation;
import net.engio.mbassy.dispatch.IMessageDispatcher;
import net.engio.mbassy.subscription.SubscriptionContext;
import net.engio.mbassy.subscription.SubscriptionFactory;

// Dirty, dirty hack to prevent MBassador from delivering cancelled events
public class SubscriptionFactoryHack extends SubscriptionFactory {
    @Override
    protected IMessageDispatcher buildDispatcher(SubscriptionContext context, IHandlerInvocation invocation) {
        IMessageDispatcher disp = new MessageDispatcherHack(context, invocation);
        if (context.getHandler().isEnveloped()) {
            disp = new EnvelopedMessageDispatcher(disp);
        }
        if (context.getHandler().isFiltered()) {
            disp = new FilteredMessageDispatcher(disp);
        }
        return disp;
    }
}
