package com.orionswift.projectzombie.util;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class NumericalSpringing {
    // Source: http://allenchou.net/2015/04/game-math-precise-control-over-numeric-springing/

    public static class SpringResult {
        public float value;
        public float velocity;
    }

    private static SpringResult springResult = new SpringResult();

    public static SpringResult spring(float value, float velocity, float target, float dampingRatio, float angularFrequency, float deltaTime) {
        angularFrequency *= MathUtils.PI2;

        float f = 1.0f + 2.0f * deltaTime * dampingRatio * angularFrequency;
        float oo = angularFrequency * angularFrequency;
        float hoo = deltaTime * oo;
        float hhoo = deltaTime * hoo;
        float detInv = 1.0f / (f + hhoo);
        float detX = f * value + deltaTime * velocity + hhoo * target;
        float detV = velocity + hoo * (target - value);
        springResult.value = detX * detInv;
        springResult.velocity = detV * detInv;
        return springResult;
    }

    // Keep in mind positionOut and velocityOut is both read from AND set to.
    public static void springVector(Vector2 positionOut, Vector2 velocityOut, Vector2 target, float dampingRatio, float angularFrequency, float deltaTime) {
        SpringResult result = spring(positionOut.x, velocityOut.x, target.x, dampingRatio, angularFrequency, deltaTime);
        float xValue = result.value;
        float xVelocity = result.velocity;

        result = spring(positionOut.y, velocityOut.y, target.y, dampingRatio, angularFrequency, deltaTime);
        float yValue = result.value;
        float yVelocity = result.velocity;

        positionOut.set(xValue, yValue);
        velocityOut.set(xVelocity, yVelocity);
    }
}
