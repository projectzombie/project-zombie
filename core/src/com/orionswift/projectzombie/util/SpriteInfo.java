package com.orionswift.projectzombie.util;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

public class SpriteInfo {
    private Sprite sprite;
    private Vector2 offset;
    private int z;

    public SpriteInfo(Sprite sprite, Vector2 offset, int z) {
        this.offset = offset;
        this.sprite = sprite;
        this.z = z;
    }

    public SpriteInfo(Sprite sprite) {
        this(sprite, new Vector2(), 0);
    }

    public Sprite getSprite() {
        return sprite;
    }

    public Vector2 getOffset() {
        return offset;
    }

    public int getZ() {
        return z;
    }
}