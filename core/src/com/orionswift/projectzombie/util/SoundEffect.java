package com.orionswift.projectzombie.util;

import java.util.Arrays;
import java.util.List;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.orionswift.projectzombie.screen.GS;

public class SoundEffect {
    private List<Sound> soundChoices;
    private float pitchDeviation;

    public SoundEffect(Sound... sounds) {
        this(0, sounds);
    }

    public SoundEffect(float pitchDeviation, Sound... sounds) {
        this.soundChoices = Arrays.asList(sounds);
        this.pitchDeviation = pitchDeviation;
    }

    public List<Sound> getSoundChoices() {
        return soundChoices;
    }

    public float getPitchDeviation() {
        return pitchDeviation;
    }
    
    public void play(float volume) {
        Sound choice = soundChoices.get((int) (Math.random() * soundChoices.size()));
        float pitch = (float) ((Math.random()*2-1) * pitchDeviation + 1);
        choice.play(volume / 2, pitch, 0);
    }

    public void play(Vector2 source) {
        float distance = Vector2.dst(source.x, source.y, GS.i.getCamera().position.x, GS.i.getCamera().position.y);
        play(1 - MathUtils.clamp(distance / 30, 0, 1));
    }
}
