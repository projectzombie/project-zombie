package com.orionswift.projectzombie.util;

import com.badlogic.ashley.core.Entity;
import com.google.common.base.Function;
import com.orionswift.projectzombie.event.ESwitchWeapon;
import com.orionswift.projectzombie.screen.GS;
import com.orionswift.projectzombie.weapon.ZombieBite;

public enum ZombieType {
    NORMAL(new Function<Float, Entity>() {
        @Override
        public Entity apply(Float input) {
            Entity entity = EntityCreator.createZombie(input);
            GS.i.getBus().publish(new ESwitchWeapon(entity, null, new ZombieBite(entity, 2 * input, 10, 1)));
            return entity;
        }
    }),
    MOOSE(new Function<Float, Entity>() {
        @Override
        public Entity apply(Float input) {
            Entity entity = EntityCreator.createMoose(input);
            GS.i.getBus().publish(new ESwitchWeapon(entity, null, new ZombieBite(entity, 3 * input, 50, 1)));
            return entity;
        }
    }),
    KAMIKAZE(new Function<Float, Entity>() {
        @Override
        public Entity apply(Float input) {
            Entity entity = EntityCreator.createKamikaze(input);
            GS.i.getBus().publish(new ESwitchWeapon(entity, null, new ZombieBite(entity, 0.1f * input, 50, 1)));
            return entity;
        }
    }),
    CROW(new Function<Float, Entity>() {
    	@Override
    	public Entity apply(Float input) {
    		Entity entity = EntityCreator.createCrow(input);
    		GS.i.getBus().publish(new ESwitchWeapon(entity, null, new ZombieBite(entity, 0.7f * input, 10, 1.5f)));
    		return entity;
    	}
    }),
    BOSS(new Function<Float, Entity>() {
        @Override
        public Entity apply(Float input) {
            Entity entity = EntityCreator.createBossZombie();
            GS.i.getBus().publish(new ESwitchWeapon(entity, null, new ZombieBite(entity, 5 * input, 50, 3)));
            return entity;
        }
    });

    private Function<Float, Entity> factory;

    ZombieType(Function<Float, Entity> factory) {
        this.factory = factory;
    }

    public Function<Float, Entity> getFactory() {
        return factory;
    }
}
