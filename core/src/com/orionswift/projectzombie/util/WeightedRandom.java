package com.orionswift.projectzombie.util;

import java.util.HashMap;
import java.util.Map;

public class WeightedRandom<T> {
    private Map<T, Float> weights;
    private float totalWeight;

    public WeightedRandom() {
        weights = new HashMap<>();
    }

    public WeightedRandom<T> add(T obj, float weight) {
        weights.put(obj, weight);
        recalculateTotal();
        return this;
    }

    public T get() {
        float random = (float) (Math.random() * totalWeight);
        float acc = 0;
        for (Map.Entry<T, Float> entry : weights.entrySet()) {
            acc += entry.getValue();
            if (acc > random) {
                return entry.getKey();
            }
        }
        return null;
    }

    private void recalculateTotal() {
        totalWeight = 0;
        for (float val : weights.values()) {
            totalWeight += val;
        }
    }
}
