package com.orionswift.projectzombie.util;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.orionswift.projectzombie.weapon.WeaponSpriteConfig;

public class Assets {
    private static AssetManager assetManager;

    public static TextureAtlas sprites;
    public static Pixmap crosshair;
    public static BitmapFont smallFont;
    public static BitmapFont largeFont;
    public static Sound machineGun1;
    public static Sound pistol1;
    public static Sound shotgunShoot;
    public static Sound playerOw1;
    public static Sound playerOw2;
    public static Sound playerOw3;
    public static Sound playerOwWithHit1;
    public static Sound playerOwWithHit2;
    public static Sound playerOwWithHit3;
    public static Sound shotgun1;
    public static Sound sniper1;
    public static Sound zombieGroan;
    public static Sound zombieGrowl;
    public static Sound zombieOw1;
    public static Sound zombieOw2;
    public static Sound zombieOw3;
    public static Sound itemBuy;
    public static Sound playerStep1;
    public static Sound playerStep2;
    public static Sound insufficientFunds;
    public static Sound tileBreak;
    public static Sound outOfAmmoSound;
    public static Sound pistolReload;
    public static Sound machineGunReload;
    public static Sound shotgunReload;
    public static Sound bloodSplatterSound;
    public static Sound explodeSound;
    public static Sound mooseHurtSound;
    public static Sound mooseDeathSound;
    public static Sound crowHurtSound;
    public static Sound crowDeathSound;
    public static Sound crowIdleSound;
    public static Sound weaponSwitchSound;

    public static Animation playerLegsWalk;
    public static Animation zombieWalk;
    public static Animation mooseWalk;
    public static Animation kamikazeWalk;
    public static Animation crowWalk;
    public static Animation despawn;
    public static Animation turretShoot;
    public static Animation ammoBox;
    public static Animation healthBox;
    public static Animation headBob;
    public static Animation bossWalk;

    public static Texture bulletImage;

    public static SoundEffect playerOw;
    public static SoundEffect playerOwWithHit;
    public static SoundEffect zombieOw;
    public static SoundEffect zombieNoise;
    public static SoundEffect pistol;
    public static SoundEffect machineGun;
    public static SoundEffect shotgun;
    public static SoundEffect playerStep;
    public static SoundEffect outOfAmmo;
    public static SoundEffect bloodSplatter;
    public static SoundEffect explode;
    public static SoundEffect mooseHurt;
    public static SoundEffect mooseDeath;
    public static SoundEffect crowHurt;
    public static SoundEffect crowDeath;
    public static SoundEffect crowIdle;
    public static SoundEffect tileBreakFx;
    public static SoundEffect weaponSwitch;

    public static Music nightMusic;
    public static Music dayMusic;

    public static WeaponSpriteConfig gunConfig;

    public static void loadAssets() {
        assetManager = new AssetManager();
        assetManager.load("_packed_sprites/sprites.atlas", TextureAtlas.class);
        assetManager.load("ui/crosshair.png", Pixmap.class);
        assetManager.load("ui/bullet.png", Texture.class);
        assetManager.load("ui/font/small.fnt", BitmapFont.class);
        assetManager.load("ui/font/large.fnt", BitmapFont.class);
        assetManager.load("sound/sfx/MachineGun1.ogg", Sound.class);
        assetManager.load("sound/sfx/Pistol1.ogg", Sound.class);
        assetManager.load("sound/sfx/PlayerOw1.ogg", Sound.class);
        assetManager.load("sound/sfx/PlayerOw2.ogg", Sound.class);
        assetManager.load("sound/sfx/PlayerOw3.ogg", Sound.class);
        assetManager.load("sound/sfx/PlayerOwWithHit1.ogg", Sound.class);
        assetManager.load("sound/sfx/PlayerOwWithHit2.ogg", Sound.class);
        assetManager.load("sound/sfx/PlayerOwWithHit3.ogg", Sound.class);
        assetManager.load("sound/sfx/Shotgun1.ogg", Sound.class);
        assetManager.load("sound/sfx/Sniper1.ogg", Sound.class);
        assetManager.load("sound/sfx/ZombieGroan.ogg", Sound.class);
        assetManager.load("sound/sfx/ZombieGrowl.ogg", Sound.class);
        assetManager.load("sound/sfx/ZombieOw1.ogg", Sound.class);
        assetManager.load("sound/sfx/ZombieOw2.ogg", Sound.class);
        assetManager.load("sound/sfx/ZombieOw3.ogg", Sound.class);
        assetManager.load("sound/sfx/ItemBuy.ogg", Sound.class);
        assetManager.load("sound/sfx/PlayerStepA.wav", Sound.class);
        assetManager.load("sound/sfx/PlayerStepB.wav", Sound.class);
        assetManager.load("sound/music/DayMusicV3.ogg", Music.class);
        assetManager.load("sound/music/NightMusicV3.ogg", Music.class);
        assetManager.load("sound/sfx/InsufficientFunds.wav", Sound.class);
        assetManager.load("sound/sfx/TileBreak.wav", Sound.class);
        assetManager.load("sound/sfx/OutOfAmmo.ogg", Sound.class);
        assetManager.load("sound/sfx/PistolReload.ogg", Sound.class);
        assetManager.load("sound/sfx/MachineGunReload.ogg", Sound.class);
        assetManager.load("sound/sfx/Shotgun_Reload.ogg", Sound.class);
        assetManager.load("sound/sfx/BloodSquirt.ogg", Sound.class);
        assetManager.load("sound/sfx/PipeBombExplode.ogg", Sound.class);
        assetManager.load("sound/sfx/MooseDeath.ogg", Sound.class);
        assetManager.load("sound/sfx/MooseHit.ogg", Sound.class);
        assetManager.load("sound/sfx/CrowDamage.ogg", Sound.class);
        assetManager.load("sound/sfx/CrowDeath.ogg", Sound.class);
        assetManager.load("sound/sfx/CrowIdle.ogg", Sound.class);
        assetManager.load("sound/sfx/WeaponSwitch.ogg", Sound.class);
        assetManager.finishLoading();

        sprites = assetManager.get("_packed_sprites/sprites.atlas");
        crosshair = assetManager.get("ui/crosshair.png");
        smallFont = assetManager.get("ui/font/small.fnt");
        largeFont = assetManager.get("ui/font/large.fnt");
        machineGun1 = assetManager.get("sound/sfx/MachineGun1.ogg");
        pistol1 = assetManager.get("sound/sfx/Pistol1.ogg");
        playerOw1 = assetManager.get("sound/sfx/PlayerOw1.ogg");
        playerOw2 = assetManager.get("sound/sfx/PlayerOw2.ogg");
        playerOw3 = assetManager.get("sound/sfx/PlayerOw3.ogg");
        playerOwWithHit1 = assetManager.get("sound/sfx/PlayerOwWithHit1.ogg");
        playerOwWithHit2 = assetManager.get("sound/sfx/PlayerOwWithHit2.ogg");
        playerOwWithHit3 = assetManager.get("sound/sfx/PlayerOwWithHit3.ogg");
        shotgun1 = assetManager.get("sound/sfx/Shotgun1.ogg");
        sniper1 = assetManager.get("sound/sfx/Sniper1.ogg");
        zombieGroan = assetManager.get("sound/sfx/ZombieGroan.ogg");
        zombieGrowl = assetManager.get("sound/sfx/ZombieGrowl.ogg");
        dayMusic = assetManager.get("sound/music/DayMusicV3.ogg");
        nightMusic = assetManager.get("sound/music/NightMusicV3.ogg");
        zombieOw1 = assetManager.get("sound/sfx/ZombieOw1.ogg");
        zombieOw2 = assetManager.get("sound/sfx/ZombieOw2.ogg");
        zombieOw3 = assetManager.get("sound/sfx/ZombieOw3.ogg");
        itemBuy = assetManager.get("sound/sfx/ItemBuy.ogg");
        playerStep1 = assetManager.get("sound/sfx/PlayerStepA.wav");
        playerStep2 = assetManager.get("sound/sfx/PlayerStepB.wav");
        insufficientFunds = assetManager.get("sound/sfx/InsufficientFunds.wav");
        tileBreak = assetManager.get("sound/sfx/TileBreak.wav");
        outOfAmmoSound = assetManager.get("sound/sfx/OutOfAmmo.ogg");
        pistolReload = assetManager.get("sound/sfx/PistolReload.ogg");
        machineGunReload = assetManager.get("sound/sfx/MachineGunReload.ogg");
        shotgunReload = assetManager.get("sound/sfx/Shotgun_Reload.ogg");
        bloodSplatterSound = assetManager.get("sound/sfx/BloodSquirt.ogg");
        explodeSound = assetManager.get("sound/sfx/PipeBombExplode.ogg");
        mooseDeathSound = assetManager.get("sound/sfx/MooseDeath.ogg");
        mooseHurtSound = assetManager.get("sound/sfx/MooseHit.ogg");
        crowHurtSound = assetManager.get("sound/sfx/CrowDamage.ogg");
        crowDeathSound = assetManager.get("sound/sfx/CrowDeath.ogg");
        crowIdleSound = assetManager.get("sound/sfx/CrowIdle.ogg");
        weaponSwitchSound = assetManager.get("sound/sfx/WeaponSwitch.ogg");
        
        playerLegsWalk = new Animation(0.1f,
                sprites.findRegions("player/legs/walk"));
        playerLegsWalk.setPlayMode(Animation.PlayMode.LOOP);

        zombieWalk = new Animation(0.1f,
                sprites.findRegions("zombie/move/move"));
        zombieWalk.setPlayMode(Animation.PlayMode.LOOP);

        mooseWalk = new Animation(0.1f,
                sprites.findRegions("moose/move/move"));
        mooseWalk.setPlayMode(Animation.PlayMode.LOOP);

        kamikazeWalk = new Animation(0.1f,
                sprites.findRegions("kamikaze/move/move"));
        kamikazeWalk.setPlayMode(Animation.PlayMode.LOOP);

        despawn = new Animation(0.3f,
                sprites.findRegions("zombie/spawn/spawn"));

        headBob = new Animation(0.3f, sprites.findRegions("player/head/head"));
        headBob.setPlayMode(Animation.PlayMode.LOOP);

        bossWalk = new Animation(0.3f, sprites.findRegions("fatso/move/move"));
        bossWalk.setPlayMode(Animation.PlayMode.LOOP);

        crowWalk = new Animation(0.3f, sprites.findRegions("crow/move/move"));
        crowWalk.setPlayMode(Animation.PlayMode.LOOP);

        turretShoot = new Animation(0.1f,
                sprites.findRegions("turrets/machinegun/machinegun_fire")
        );
        turretShoot.setPlayMode(Animation.PlayMode.LOOP);

        ammoBox = new Animation(0.3f,
                sprites.findRegions("ballistic/ammobox"));
        ammoBox.setPlayMode(Animation.PlayMode.LOOP);

        healthBox = new Animation(0.3f,
                sprites.findRegions("ballistic/healthbox"));
        healthBox.setPlayMode(Animation.PlayMode.LOOP);

        bulletImage = assetManager.get("ui/bullet.png");

        zombieNoise = new SoundEffect(0.3f, zombieGroan, zombieGrowl);
        playerOw = new SoundEffect(0.15f, playerOw1, playerOw2, playerOw3);
        playerOwWithHit = new SoundEffect(0.15f, playerOwWithHit1, playerOwWithHit2, playerOwWithHit3);
        zombieOw = new SoundEffect(0.15f, zombieOw1, zombieOw2, zombieOw3);
        pistol = new SoundEffect(0.1f, pistol1);
        machineGun = new SoundEffect(0.1f, machineGun1);
        playerStep = new SoundEffect(0.15f, playerStep1, playerStep2);
        outOfAmmo = new SoundEffect(0.1f, outOfAmmoSound);
        shotgun = new SoundEffect(0f, shotgun1);
        bloodSplatter = new SoundEffect(0.1f, bloodSplatterSound);
        explode = new SoundEffect(0.1f, explodeSound);
        mooseDeath = new SoundEffect(0.1f, mooseDeathSound);
        mooseHurt = new SoundEffect(0.1f, mooseHurtSound);
        crowDeath = new SoundEffect(0.1f, crowDeathSound);
        crowHurt = new SoundEffect(0.1f, crowHurtSound);
        crowIdle = new SoundEffect(0.1f, crowIdleSound);
        tileBreakFx = new SoundEffect(0.1f, tileBreak);
        weaponSwitch = new SoundEffect(0.1f, weaponSwitchSound);
        
        gunConfig = new WeaponSpriteConfig(sprites.findRegion("weapons/gun"), new Vector2(0.25f, 0f), new Vector2(0.25f, 0.6875f / 2f), new Vector2(1, 0.6875f));
    }
}
