package com.orionswift.projectzombie.util;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.orionswift.projectzombie.event.EContactEnd;
import com.orionswift.projectzombie.event.EContactStart;
import com.orionswift.projectzombie.event.EEntitiesCollide;
import com.orionswift.projectzombie.screen.GS;

public class CollisionListener implements ContactListener {
    @Override
    public void beginContact(Contact contact) {
        GS.i.getBus().publish(new EContactStart(contact));

        if (contact.getFixtureA().getBody().getUserData() instanceof Entity && contact.getFixtureB().getBody().getUserData() instanceof Entity) {
            GS.i.getBus().publish(new EEntitiesCollide((Entity) contact.getFixtureA().getBody().getUserData(), (Entity) contact.getFixtureB().getBody().getUserData()));
        }
    }

    @Override
    public void endContact(Contact contact) {
        GS.i.getBus().post(new EContactEnd(contact));
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
