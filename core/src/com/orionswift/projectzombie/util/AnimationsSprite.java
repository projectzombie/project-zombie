package com.orionswift.projectzombie.util;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.google.common.collect.BiMap;
import net.dermetfan.gdx.graphics.g2d.AnimatedSprite;

import java.util.HashMap;
import java.util.Map;

public class AnimationsSprite extends AnimatedSprite {
    private BiMap<String, Animation> animations;
    private Map<Animation, Integer> priorityMap;

    public AnimationsSprite(BiMap<String, Animation> animations) {
        super(animations.values().iterator().next());
        this.animations = animations;
        priorityMap = new HashMap<>();
    }

    public void playAnimation(String name, int priority) {
        priorityMap.put(animations.get(name), priority);
        applyTop();
    }

    public void removeAnimation(String name) {
        if (animations.containsKey(name)) {
            priorityMap.remove(animations.get(name));
            applyTop();
        }
    }

    @Override
    public void update() {
        if (isAnimationFinished() && getAnimation().getPlayMode() == Animation.PlayMode.NORMAL) {
            removeAnimation(animations.inverse().get(getAnimation()));
        }
        super.update();
    }

    private void applyTop() {
        Animation top = null;
        int topPriority = -9999999;
        for (Map.Entry<Animation, Integer> entry : priorityMap.entrySet()) {
            if (entry.getValue() > topPriority) {
                top = entry.getKey();
                topPriority = entry.getValue();
            }
        }

        if (top != null) {
            setAnimation(top);
        }
    }

    public BiMap<String, Animation> getAnimations() {
        return animations;
    }
}
