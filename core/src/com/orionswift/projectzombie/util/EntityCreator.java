package com.orionswift.projectzombie.util;


import java.util.BitSet;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJoint;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
//projectzombieteam@bitbucket.org/projectzombie/project-zombie.git
import com.google.common.collect.ImmutableBiMap;
import com.orionswift.projectzombie.component.CAbsorbHealthOnDamage;
import com.orionswift.projectzombie.component.CAggro;
import com.orionswift.projectzombie.component.CAggroDrawer;
import com.orionswift.projectzombie.component.CAggroWeaponAngle;
import com.orionswift.projectzombie.component.CAmmoBox;
import com.orionswift.projectzombie.component.CAmmoHUD;
import com.orionswift.projectzombie.component.CAnimationOnShoot;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CCameraFollow;
import com.orionswift.projectzombie.component.CDamageOnTouch;
import com.orionswift.projectzombie.component.CDespawnOnDay;
import com.orionswift.projectzombie.component.CDirectionalInput;
import com.orionswift.projectzombie.component.CDoor;
import com.orionswift.projectzombie.component.CDoorOpener;
import com.orionswift.projectzombie.component.CDropCoinsOnDeath;
import com.orionswift.projectzombie.component.CExplodeOnNear;
import com.orionswift.projectzombie.component.CExplodeSoon;
import com.orionswift.projectzombie.component.CFaceAggro;
import com.orionswift.projectzombie.component.CGoryDeath;
import com.orionswift.projectzombie.component.CHeadTowardsAggroInput;
import com.orionswift.projectzombie.component.CHealth;
import com.orionswift.projectzombie.component.CHealthBox;
import com.orionswift.projectzombie.component.CIgnoreAbsorbHealthOnDamage;
import com.orionswift.projectzombie.component.CIgnoreDamage;
import com.orionswift.projectzombie.component.CMovement;
import com.orionswift.projectzombie.component.CPickUpSupply;
import com.orionswift.projectzombie.component.CPipeBombs;
import com.orionswift.projectzombie.component.CPlayAnimationOnInput;
import com.orionswift.projectzombie.component.CPlayerFireWeapon;
import com.orionswift.projectzombie.component.CPlayerInput;
import com.orionswift.projectzombie.component.CPlayerLegTorsoControls;
import com.orionswift.projectzombie.component.CPlayerWeaponAngle;
import com.orionswift.projectzombie.component.CRaycastCollision;
import com.orionswift.projectzombie.component.CRemoveOnTouch;
import com.orionswift.projectzombie.component.CRender;
import com.orionswift.projectzombie.component.CRenderHealthBar;
import com.orionswift.projectzombie.component.CRepeatSoundEffectRandomly;
import com.orionswift.projectzombie.component.CRepel;
import com.orionswift.projectzombie.component.CShootOnAggroProximity;
import com.orionswift.projectzombie.component.CShowGameOverOnDeath;
import com.orionswift.projectzombie.component.CSoundEffectOnDamage;
import com.orionswift.projectzombie.component.CSoundEffectOnDeath;
import com.orionswift.projectzombie.component.CSpawnAround;
import com.orionswift.projectzombie.component.CStepSound;
import com.orionswift.projectzombie.component.CStopFiringOnUI;
import com.orionswift.projectzombie.component.CSupply;
import com.orionswift.projectzombie.component.CTile;
import com.orionswift.projectzombie.component.CTrailRender;
import com.orionswift.projectzombie.component.CWeaponStash;
import com.orionswift.projectzombie.event.EDamage;
import com.orionswift.projectzombie.event.ESwitchWeapon;
import com.orionswift.projectzombie.screen.GS;
import com.orionswift.projectzombie.tile.TileType;
import com.orionswift.projectzombie.weapon.Pistol;
import com.orionswift.projectzombie.weapon.TurretGun;
import com.orionswift.projectzombie.weapon.Weapon;

import box2dLight.ConeLight;

public class EntityCreator {
    public static short PLAYER_CAT = 0b000000000000010;
    public static short ZOMBIE_CAT = 0b000000000000100;
    public static short BULLET_CAT = 0b000000000001000;
    public static short TILE_CAT = 0b000000000010000;
    public static short SUPPLY_CAT = 0b000000000100000;

    public static short PLAYER_MASK = 0b000000000110001;
    public static short ZOMBIE_MASK = 0b000000000011001;
    public static short BULLET_MASK = 0b000000000010101;
    public static short TILE_MASK = 0b0000000000011111;
    public static short SUPPLY_MASK = 0b0000000000011111;

    public static short LIGHT_MASK = 0b000000000010001;
    public static short ZOMBIE_DAMAGE_MASK = 0b0000000000010010;

    public static Body createBody(float radius, boolean sensor, short cat, short mask, Entity entity, float damping, boolean box, BodyDef.BodyType type, float aspect) {
        Body body = GS.i.getWorld().createBody(new BodyDef());
        body.setType(type);
        body.setUserData(entity);
        body.setLinearDamping(damping);

        Fixture fix;
        if (box) {
            PolygonShape shape = new PolygonShape();
            shape.setAsBox(radius * aspect, radius);
            fix = body.createFixture(shape, 1);
        } else {
            CircleShape shape = new CircleShape();
            shape.setRadius(radius);
            fix = body.createFixture(shape, 1);
        }
        fix.setSensor(sensor);

        Filter filter = new Filter();
        filter.categoryBits = cat;
        filter.maskBits = mask;

        fix.setFilterData(filter);

        return body;
    }

    public static Entity createPlayer() {
        AnimationsSprite torsoSprite = new AnimationsSprite(
                ImmutableBiMap.<String, Animation>builder()
                        .put("static", new Animation(0.1f, Assets.sprites.findRegion("player/torso/holding"))).build());

        torsoSprite.getAnimations().get("static").setPlayMode(Animation.PlayMode.LOOP);
        torsoSprite.setSize(1, 0.6875f);
        torsoSprite.playAnimation("static", -1);
        torsoSprite.setOrigin(0.25f, torsoSprite.getHeight() / 2f);

        AnimationsSprite legsSprite = new AnimationsSprite(
                ImmutableBiMap.<String, Animation>builder()
                        .put("static", new Animation(0.1f, Assets.sprites.findRegion("player/head/head")))
                        .put("walk", Assets.playerLegsWalk)
                        .build());
        legsSprite.playAnimation("static", -1);
        legsSprite.getAnimations().get("static").setPlayMode(Animation.PlayMode.LOOP);

        legsSprite.setSize(1, 0.6875f);
        legsSprite.setOrigin(0.25f, legsSprite.getHeight() / 2f);

        AnimationsSprite headSprite = new AnimationsSprite(
                ImmutableBiMap.<String, Animation>builder()
                        .put("static", new Animation(0.1f, Assets.sprites.findRegion("player/head/head")))
                        .put("walk", Assets.headBob)
                        .build()
        );
        headSprite.setSize(1, 0.6875f);
        headSprite.setOrigin(0.25f, legsSprite.getHeight() / 2f);
        headSprite.getAnimations().get("static").setPlayMode(Animation.PlayMode.LOOP);
        headSprite.playAnimation("static", -1);

        Entity entity = new Entity();
        Body body = createBody(0.3f, false, PLAYER_CAT, PLAYER_MASK, entity, 50, false, BodyDef.BodyType.DynamicBody, 1f);

        Pistol defWeapon = new Pistol(entity);

        entity.add(new CBody(body))
                .add(new CRender(new SpriteInfo(legsSprite, new Vector2(0.25f, 0), -1), new SpriteInfo(torsoSprite, new Vector2(0.25f, 0), 0), new SpriteInfo(headSprite, new Vector2(0.25f, 0), 1)))
                .add(new CMovement(5, 50))
                .add(new CDirectionalInput())
                .add(new CPlayerInput(Input.Keys.W, Input.Keys.A, Input.Keys.S, Input.Keys.D))
                .add(new CPlayerLegTorsoControls(legsSprite, torsoSprite, null, headSprite))
                .add(new CPlayerFireWeapon())
                .add(new CAggroDrawer(0))
                .add(new CPlayerWeaponAngle())
                .add(new CStopFiringOnUI())
                .add(new CHealth(20, 20))
                .add(new CCameraFollow(1, 10))
                .add(new CWeaponStash(0, defWeapon))
                .add(new CRenderHealthBar(2))
                .add(new CSpawnAround())
                .add(new CPlayAnimationOnInput("walk", 10))
                .add(new CStepSound(Assets.playerStep))
                .add(new CGoryDeath(100))
                .add(new CAmmoHUD())
                .add(new CHealthBox(0))
                .add(new CAmmoBox(0))
                .add(new CDoorOpener())
                .add(new CIgnoreDamage(0, new BitSet()))
                .add(new CPickUpSupply())
                .add(new CPipeBombs(0))
                .add(new CShowGameOverOnDeath());

        GS.i.getBus().publish(new ESwitchWeapon(entity, null, defWeapon));
        createFlashlight(entity);

        return entity;
    }

    public static void createFlashlight(Entity entity) {
        ConeLight light = new ConeLight(GS.i.getRayHandler(), 100, Color.WHITE, 8, 0, 0, 0, 20);
        light.attachToBody(entity.getComponent(CBody.class).getBody());
        light.setContactFilter((short) 1, (short) 0, LIGHT_MASK);
    }

    public static Entity createBullet(Weapon weapon) {
        Entity entity = new Entity();

        // Mask is zero here because of raycast collision, normal and raycast shouldn't conflict
        Body bulletBody = createBody(0.025f, true, BULLET_CAT, (short) 0, entity, 0, false, BodyDef.BodyType.DynamicBody, 1f);
        bulletBody.setBullet(true);

        return entity
                .add(new CBody(bulletBody))
                .add(new CRaycastCollision(BULLET_MASK, new Vector2()))
                .add(new CTrailRender(0.03f, 0.08f, 0f, Color.GOLD, Color.GOLDENROD))
                .add(new CRemoveOnTouch())
                .add(new CIgnoreDamage(0, BitSet.valueOf(new byte[]{0})))
                .add(new CDamageOnTouch(weapon.getDamage(), weapon.getKnockback(), EDamage.KnockbackMode.SourceVelocity, EDamage.DamageType.Bullet, weapon.getEntity()));
    }

    public static Entity createZombie(float multiplier) {
        AnimationsSprite sprite = new AnimationsSprite(
                ImmutableBiMap.<String, Animation>builder()
                        .put("walk", Assets.zombieWalk).build());

        sprite.setSize(1 * multiplier, 1 * multiplier);
        sprite.setOriginCenter();
        sprite.playAnimation("walk", -1);
        
        return createBaseZombie(multiplier, sprite, Assets.zombieOw, Assets.zombieNoise, 7, 3.5f, 0.4f);
    }
    
    public static Entity createBossZombie() {
    	Entity entity = createZombie(5);
    	entity.remove(CRender.class);
    	entity.remove(CDropCoinsOnDeath.class);
    	
    	AnimationsSprite sprite = new AnimationsSprite(
                ImmutableBiMap.<String, Animation>builder()
                        .put("walk", Assets.bossWalk).build());
        sprite.setSize(5, 5);
        sprite.setOriginCenter();
        sprite.playAnimation("walk", -1);
        
    	return entity.add(new CRender(new SpriteInfo(sprite))).add(new CDropCoinsOnDeath(50));
    }

    public static Entity createMoose(float multiplier) {
        AnimationsSprite sprite = new AnimationsSprite(
                ImmutableBiMap.<String, Animation>builder()
                        .put("walk", Assets.mooseWalk).build());

        sprite.setSize(2 * multiplier, 2 * multiplier);
        sprite.setOriginCenter();
        sprite.playAnimation("walk", -1);
        
        return createBaseZombie(multiplier, sprite, Assets.mooseHurt, Assets.mooseDeath, 18, 7, 0.8f);
    }
    
    public static Entity createKamikaze(float multiplier) {
        AnimationsSprite sprite = new AnimationsSprite(
                ImmutableBiMap.<String, Animation>builder()
                        .put("walk", Assets.kamikazeWalk).build());

        sprite.setSize(1 * multiplier, 1 * multiplier);
        sprite.setOriginCenter();
        sprite.playAnimation("walk", -1);

        Entity entity = createBaseZombie(multiplier, sprite, Assets.zombieOw, Assets.zombieNoise, 13, 4, 0.3f);
        
        return entity.add(new CExplodeOnNear(entity, 3f * multiplier, 2));
    }

    public static Entity createCrow(float multiplier) {
        AnimationsSprite sprite = new AnimationsSprite(
                ImmutableBiMap.<String, Animation>builder()
                        .put("walk", Assets.crowWalk).build());
        
        sprite.setSize(0.8f * multiplier, 0.8f * multiplier);
        sprite.setOriginCenter();
        sprite.playAnimation("walk", -1);
        return createBaseZombie(multiplier, sprite, Assets.crowHurt, Assets.crowDeath, 6.3f, 4, 0.3f)
        		.add(new CAbsorbHealthOnDamage(1))
        		.add(new CRepeatSoundEffectRandomly(3, 5, Assets.crowIdle));
    }

    public static Entity createBaseZombie(float multiplier, AnimationsSprite sprite, SoundEffect damage, SoundEffect deathSound, float coinDrop, float speed, float size) {
        float tripleMultiplier = ((multiplier - 1) * 3) + 1;
        tripleMultiplier = Math.max(tripleMultiplier, 0.1f);

        Entity entity = new Entity();
        entity.add(new CBody(createBody(size * multiplier, false, ZOMBIE_CAT, ZOMBIE_MASK, entity, 50, false, BodyDef.BodyType.DynamicBody, 1f)))
                .add(new CRender(new SpriteInfo(sprite)))
                .add(new CAggroDrawer(1))
                .add(new CAggro(false, BitSet.valueOf(new byte[]{0b00000001})))
                .add(new CHeadTowardsAggroInput())
                .add(new CDirectionalInput())
                .add(new CMovement(MathUtils.clamp(speed * (1 / tripleMultiplier), speed / 2f, speed * 2f), 50))
                .add(new CFaceAggro())
                .add(new CSoundEffectOnDeath(deathSound))
                .add(new CSoundEffectOnDamage(damage, damage))
                .add(new CDropCoinsOnDeath(coinDrop * multiplier))
                .add(new CAggroWeaponAngle())
                .add(new CDespawnOnDay())
                .add(new CHealth(10 * tripleMultiplier, 10 * tripleMultiplier))
                .add(new CRepel(0.5f))
                .add(new CRenderHealthBar(1))
                .add(new CGoryDeath(tripleMultiplier));
        return entity;
    }

    public static Entity createPipeBomb() {
        Sprite sprite = new Sprite(Assets.sprites.findRegion("ballistic/pipebomb"));
        sprite.setSize(1, 1);
        sprite.setOriginCenter();

        Entity entity = new Entity();
        entity.add(new CBody(createBody(0.3f, false, PLAYER_CAT, PLAYER_MASK, entity, 0, false, BodyDef.BodyType.DynamicBody, 1f)))
                .add(new CRender(new SpriteInfo(sprite)))
                .add(new CExplodeSoon(5, 20))
                .add(new CAggroDrawer(0));
        entity.getComponent(CBody.class).getBody().setLinearDamping(2);
        entity.getComponent(CBody.class).getBody().setAngularDamping(3);

        return entity;
    }
    

    public static Entity createTile(TileType type) {
        if (type == TileType.TURRET) return createTurret().add(new CTile());
        if (type == TileType.HORIZONTAL_DOOR) return createDoor(false).add(new CTile());
        if (type == TileType.VERTICAL_DOOR) return createDoor(true).add(new CTile());

        Sprite sprite = new Sprite(type.getTexture());
        sprite.setSize(1, 1);
        sprite.setOriginCenter();

        Entity entity = new Entity();
        return entity.add(new CBody(createBody(0.5f, false, TILE_CAT, TILE_MASK, entity, 0, true, BodyDef.BodyType.StaticBody, 1f)))
                .add(new CRender(new SpriteInfo(sprite)))
                .add(new CIgnoreDamage(1, BitSet.valueOf(new byte[]{0b00000001})))
                .add(new CHealth(type.getMaxHealth(), type.getMaxHealth()))
                .add(new CIgnoreAbsorbHealthOnDamage())
                .add(new CSoundEffectOnDamage(Assets.tileBreakFx, Assets.tileBreakFx))
                .add(new CTile());
    }

    public static Entity createDoor(boolean vertical) {
        Sprite sprite = new Sprite(Assets.sprites.findRegion("tiles/hdoor"));
        sprite.setSize(1, 1);
        sprite.setOriginCenter();

        Entity entity = new Entity();

        Body body = createBody(0.004f, false, TILE_CAT, TILE_MASK, entity, 0, true, BodyDef.BodyType.DynamicBody, 100);
        body.setTransform(0.5f, 0.5f, vertical ? MathUtils.PI / 2f : 0);

        Body anchor = createBody(0.02f, false, (short) 1, (short) 0, entity, 0, false, BodyDef.BodyType.StaticBody, 1);
        anchor.setTransform(vertical ? 0.5f : 0f, !vertical ? 0.5f : 0f, 0);

        RevoluteJointDef jointDef = new RevoluteJointDef();
        jointDef.initialize(body, anchor, vertical ? new Vector2(0.5f, 0) : new Vector2(0, 0.5f));

        RevoluteJoint joint = (RevoluteJoint) GS.i.getWorld().createJoint(jointDef);
        joint.setLimits(0, 0);

        return entity.add(new CBody(body))
                .add(new CRender(new SpriteInfo(sprite)))
                .add(new CDoor(anchor, joint, 1.5f, vertical))
                .add(new CIgnoreDamage(1, BitSet.valueOf(new byte[]{0b00000001})))
                .add(new CHealth(TileType.HORIZONTAL_DOOR.getMaxHealth(), TileType.HORIZONTAL_DOOR.getMaxHealth()))
                .add(new CIgnoreAbsorbHealthOnDamage());
    }

    public static Entity createTurret() {
        AnimationsSprite sprite = new AnimationsSprite(
                ImmutableBiMap.<String, Animation>builder()
                        .put("static", new Animation(0.1f, Assets.sprites.findRegion("turrets/machinegun/machinegun_still")))
                        .put("shoot", Assets.turretShoot).build());
        sprite.getAnimations().get("static").setPlayMode(Animation.PlayMode.LOOP);
        sprite.playAnimation("static", -1);
        sprite.setSize(1, 1);
        sprite.setOriginCenter();

        Entity entity = new Entity();
        entity.add(new CBody(createBody(0.3f, false, TILE_CAT, TILE_MASK, entity, 0, false, BodyDef.BodyType.StaticBody, 1f)))
                .add(new CRender(new SpriteInfo(sprite)))
                .add(new CAggro(true, BitSet.valueOf(new byte[]{0b00000010})))
                .add(new CAggroWeaponAngle())
                .add(new CShootOnAggroProximity(10))
                .add(new CAnimationOnShoot("shoot", 10))
                .add(new CFaceAggro())
                .add(new CHealth(TileType.TURRET.getMaxHealth(), TileType.TURRET.getMaxHealth()))
                .add(new CRenderHealthBar(0.8f))
                .add(new CIgnoreDamage(1, BitSet.valueOf(new byte[]{0b00000001})))
                .add(new CAggroDrawer(0))
                .add(new CIgnoreAbsorbHealthOnDamage());
        GS.i.getBus().publish(new ESwitchWeapon(entity, null, new TurretGun(entity)));

        return entity;
    }

    public static Entity createSupplyBox(TextureRegion texture, SupplyType type) {
        Sprite sprite = new Sprite(texture);
        sprite.setSize(1, 1);
        sprite.setOriginCenter();

        Entity entity = new Entity();
        entity.add(new CBody(createBody(0.3f, true, SUPPLY_CAT, SUPPLY_MASK, entity, 0, false, BodyDef.BodyType.StaticBody, 1f)))
                .add(new CRender(new SpriteInfo(sprite)))
                .add(new CSupply(type));
        GS.i.getBus().publish(new ESwitchWeapon(entity, null, new TurretGun(entity)));

        return entity;
    }
}
