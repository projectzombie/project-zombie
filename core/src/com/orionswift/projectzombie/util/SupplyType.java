package com.orionswift.projectzombie.util;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;
import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import com.orionswift.projectzombie.component.CHealthBox;
import com.orionswift.projectzombie.component.CWeapon;
import com.orionswift.projectzombie.weapon.Gun;
import java.util.HashSet;
import java.util.Set;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;
import com.orionswift.projectzombie.component.CAmmoBox;
import com.orionswift.projectzombie.component.CHealthBox;

public enum SupplyType {
    // Using valueOf here, because otherwise the compiler will complain about a self-reference within the lambda

    HEALTH(new Supplier<Entity>() {
        @Override
        public Entity get() {
            return EntityCreator.createSupplyBox(Assets.sprites.findRegion("ballistic/healthbox"), SupplyType.valueOf("HEALTH"));
        }
    }, new Predicate<Entity>() {
        @Override
        public boolean apply(Entity entity) {
            if (entity.getComponent(CHealthBox.class) != null) {
                entity.getComponent(CHealthBox.class).setCount(entity.getComponent(CHealthBox.class).getCount() + 1);
                return true;
            }
            return false;
        }
    }, 0.01f),
	
    AMMO(new Supplier<Entity>() {
        @Override
        public Entity get() {
            return EntityCreator.createSupplyBox(Assets.sprites.findRegion("ballistic/ammobox"), SupplyType.valueOf("AMMO"));
        }
    }, new Predicate<Entity>() {
        @Override
        public boolean apply(Entity entity) {
            if (entity.getComponent(CAmmoBox.class) != null) {
                entity.getComponent(CAmmoBox.class).setCount(entity.getComponent(CAmmoBox.class).getCount() + 1);
                return true;
            }
            return false;
            // Code to add ammo
        }
    }, 0.02f);
	
    private Supplier<Entity> entityFactory;
    private Predicate<Entity> onPickUp;
    private float rarity;

    private Set<Vector2> spawned;

    SupplyType(Supplier<Entity> entityFactory, Predicate<Entity> onPickUp, float rarity) {
        this.entityFactory = entityFactory;
        this.onPickUp = onPickUp;
        this.rarity = rarity;

        spawned = new HashSet<>();
    }

    public Supplier<Entity> getEntityFactory() {
        return entityFactory;
    }

    public Predicate<Entity> getOnPickUp() {
        return onPickUp;
    }

    public float getRarity() {
        return rarity;
    }

    public Set<Vector2> getSpawned() {
        return spawned;
    }
}
