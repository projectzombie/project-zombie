package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.physics.box2d.graphics.ParticleEmitterBox2D;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CGoryDeath;
import com.orionswift.projectzombie.event.EEntityDeath;
import com.orionswift.projectzombie.screen.GS;
import com.orionswift.projectzombie.util.Assets;
import net.engio.mbassy.listener.Handler;

public class SGoryDeath extends EntitySystem {
    @Handler
    public void death(EEntityDeath entityDeath) {
        if (entityDeath.getEntity().getComponent(CGoryDeath.class) != null) {
            Entity entity = entityDeath.getEntity();
            CGoryDeath gd = entity.getComponent(CGoryDeath.class);

            CBody body = entity.getComponent(CBody.class);

            ParticleEffect eff = new ParticleEffect();
            eff.load(Gdx.files.internal("particles/guts.p"), Assets.sprites, "zombie/guts/");

            for (int i = 0; i < eff.getEmitters().size; i++) {
                ParticleEmitterBox2D newEmitter = new ParticleEmitterBox2D(GS.i.getWorld(), eff.getEmitters().get(i));
                newEmitter.getEmission().setHigh((int) (newEmitter.getEmission().getHighMax() * gd.getSize()));
                newEmitter.getEmission().setLow((int) (newEmitter.getEmission().getLowMax() * gd.getSize()));
                eff.getEmitters().set(i, newEmitter);
            }

            eff.setPosition(body.getPosition().x, body.getPosition().y);
            eff.start();

            GS.i.getEngine().getSystem(SParticleEmitters.class).getEffects().add(eff);

            Assets.bloodSplatter.play(body.getPosition());
        }
    }
}
