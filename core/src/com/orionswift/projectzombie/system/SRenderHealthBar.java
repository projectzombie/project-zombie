package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CHealth;
import com.orionswift.projectzombie.component.CRenderHealthBar;
import com.orionswift.projectzombie.screen.GS;

public class SRenderHealthBar extends IteratingSystem {
    public SRenderHealthBar() {
        super(Family.all(CBody.class, CHealth.class, CRenderHealthBar.class).get());
    }

    @Override
    public void update(float deltaTime) {
        GS.i.getShapeRenderer().begin(ShapeRenderer.ShapeType.Filled);
        super.update(deltaTime);
        GS.i.getShapeRenderer().end();
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        CBody body = entity.getComponent(CBody.class);
        CHealth health = entity.getComponent(CHealth.class);
        CRenderHealthBar rhb = entity.getComponent(CRenderHealthBar.class);

        GS.i.getShapeRenderer().setColor(0, 0, 0, 1);
        GS.i.getShapeRenderer().rect(body.getPosition().x - rhb.getWidth() / 2, body.getPosition().y + 0.5f, rhb.getWidth(), 0.2f);

        GS.i.getShapeRenderer().setColor(1, 0, 0, 1);
        GS.i.getShapeRenderer().rect(body.getPosition().x - rhb.getWidth() / 2, body.getPosition().y + 0.5f, rhb.getWidth() * (health.getHealth() / health.getMaxHealth()), 0.2f);
    }
}
