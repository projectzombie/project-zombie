package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Timer;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CRepeatSoundEffectRandomly;
public class SRepeatSoundEffectRandomly extends IteratingSystem {
	public SRepeatSoundEffectRandomly() {
		super(Family.all(CRepeatSoundEffectRandomly.class).get());
	}

	@Override
	protected void processEntity(Entity entity, float deltaTime) {
		final CRepeatSoundEffectRandomly rser = entity.getComponent(CRepeatSoundEffectRandomly.class);
		
		if(!rser.isPlaying()) {
			rser.setRandomTime(MathUtils.random(rser.getRange()[0], rser.getRange()[1]));
			rser.getSoundEffect().play(entity.getComponent(CBody.class).getPosition());
			rser.setPlaying(true);
			
	        Timer.instance().scheduleTask(new Timer.Task() {
	            @Override
	            public void run() {
	            	rser.setPlaying(false);
	            }
	        }, rser.getRandomTime());
		}
	}
}
