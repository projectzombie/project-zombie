package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.orionswift.projectzombie.component.CAggro;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CDirectionalInput;
import com.orionswift.projectzombie.component.CHeadTowardsAggroInput;

public class SHeadTowardsAggroInput extends IteratingSystem {
    public SHeadTowardsAggroInput() {
        super(Family.all(CBody.class, CAggro.class, CHeadTowardsAggroInput.class, CDirectionalInput.class).get());
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        CBody body = entity.getComponent(CBody.class);
        CAggro aggro = entity.getComponent(CAggro.class);
        CDirectionalInput di = entity.getComponent(CDirectionalInput.class);

        if (aggro.getAggro().isPresent()) {
            di.getMoveDirection().set(aggro.getAggro().get().getComponent(CBody.class).getPosition().cpy().sub(body.getPosition()).nor());
            if (aggro.getAggro().get().getComponent(CBody.class).getPosition().dst(body.getPosition()) < 0.5) {
                di.getMoveDirection().setZero();
            }
        }
    }
}