package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.event.EPlaceTile;
import com.orionswift.projectzombie.event.ERemoveTile;
import com.orionswift.projectzombie.screen.GS;
import com.orionswift.projectzombie.util.Assets;
import com.orionswift.projectzombie.util.SoundEffect;
import net.engio.mbassy.listener.Handler;

public class STileCost extends EntitySystem {
    @Handler(priority = 1)
    public void placeTile(EPlaceTile placeTile) {
        if (GS.i.money.get() >= placeTile.getType().getPrice()) {
            GS.i.money.sub(placeTile.getType().getPrice());
        } else {
            Assets.insufficientFunds.play();
            placeTile.setCancelled(true);
        }
    }

    @Handler(priority = 1)
    public void removeTile(ERemoveTile removeTile) {
        float refundMoney = GS.i.getTiles().get(removeTile.getPos()).getType().getPrice();
        if (!removeTile.isFullRefund()) {
            refundMoney /= 2;
        }

        GS.i.money.add(refundMoney);
    }
}
