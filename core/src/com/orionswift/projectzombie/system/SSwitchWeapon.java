package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.component.CWeapon;
import com.orionswift.projectzombie.event.ESwitchWeapon;
import net.engio.mbassy.listener.Handler;

public class SSwitchWeapon extends EntitySystem {
    @Handler
    public void switchWeapon(ESwitchWeapon switchWeapon) {
        if (switchWeapon.getEntity().getComponent(CWeapon.class) == null) {
            switchWeapon.getEntity().add(new CWeapon(switchWeapon.getNewWeapon()));
        } else {
            switchWeapon.getEntity().getComponent(CWeapon.class).setWeapon(switchWeapon.getNewWeapon());
        }
    }
}
