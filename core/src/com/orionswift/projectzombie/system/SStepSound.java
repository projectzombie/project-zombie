package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IntervalIteratingSystem;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CDirectionalInput;
import com.orionswift.projectzombie.component.CStepSound;

public class SStepSound extends IntervalIteratingSystem {
    public SStepSound() {
        super(Family.all(CBody.class, CDirectionalInput.class, CStepSound.class).get(), 0.3f);
    }

    @Override
    protected void processEntity(Entity entity) {
        CDirectionalInput di = entity.getComponent(CDirectionalInput.class);
        CStepSound ss = entity.getComponent(CStepSound.class);
        CBody body = entity.getComponent(CBody.class);

        if (!di.getMoveDirection().isZero(0.01f)) {
            ss.getSound().play(body.getPosition());
        }
    }
}
