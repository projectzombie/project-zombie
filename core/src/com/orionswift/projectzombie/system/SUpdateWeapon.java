package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.orionswift.projectzombie.component.CWeapon;
import com.orionswift.projectzombie.event.EStartFiringWeapon;
import com.orionswift.projectzombie.event.EStopFiringWeapon;
import net.engio.mbassy.listener.Handler;

public class SUpdateWeapon extends IteratingSystem {
    public SUpdateWeapon() {
        super(Family.all(CWeapon.class).get());
    }

    @Handler
    public void startFiring(EStartFiringWeapon event) {
        CWeapon weapon = event.getEntity().getComponent(CWeapon.class);
        weapon.getWeapon().startFiring();
    }

    @Handler
    public void stopFiring(EStopFiringWeapon event) {
        CWeapon weapon = event.getEntity().getComponent(CWeapon.class);
        weapon.getWeapon().stopFiring();
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        CWeapon weapon = entity.getComponent(CWeapon.class);
        weapon.getWeapon().update(deltaTime);
    }
}
