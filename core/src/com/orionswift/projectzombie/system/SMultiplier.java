package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.component.CMultiplier;
import com.orionswift.projectzombie.event.EDamage;
import net.engio.mbassy.listener.Handler;

public class SMultiplier extends EntitySystem {
    @Handler(priority = 2)
    public void damage(EDamage damage) {
        if (damage.getDamagee().getComponent(CMultiplier.class) != null) {
            damage.setDamage(damage.getDamage() * damage.getDamagee().getComponent(CMultiplier.class).getDamage());
        }
    }
}
