package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.component.CDamageOnTouch;
import com.orionswift.projectzombie.component.CHealth;
import com.orionswift.projectzombie.event.EDamage;
import com.orionswift.projectzombie.event.EEntitiesCollide;
import com.orionswift.projectzombie.screen.GS;
import net.engio.mbassy.listener.Handler;

public class SDamageOnTouch extends EntitySystem {
    @Handler
    public void entitiesCollide(EEntitiesCollide entitiesCollide) {
        Entity damager = null;
        Entity damagee = null;

        if (entitiesCollide.getEntityA().getComponent(CDamageOnTouch.class) != null) {
            damager = entitiesCollide.getEntityA();
            damagee = entitiesCollide.getEntityB();
        } else if (entitiesCollide.getEntityB().getComponent(CDamageOnTouch.class) != null) {
            damagee = entitiesCollide.getEntityA();
            damager = entitiesCollide.getEntityB();
        }

        if (damager != null && damagee != null && damagee.getComponent(CHealth.class) != null) {
            CDamageOnTouch dot = damager.getComponent(CDamageOnTouch.class);
            Entity sourceSource = damager;
            if (dot.getSource() != null) {
                sourceSource = dot.getSource();
            }
            GS.i.getBus().publish(new EDamage(damagee, damager, sourceSource, dot.getDamage(), dot.getKnockback(), dot.getKnockbackMode(), dot.getDamageType()));
        }
    }
}
