package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.component.CHealth;
import com.orionswift.projectzombie.event.EDamage;
import net.engio.mbassy.listener.Handler;

public class SSubtractHealth extends EntitySystem {
    @Handler
    public void damage(EDamage damage) {
        Entity damagee = damage.getDamagee();
        CHealth h = damagee.getComponent(CHealth.class);
        h.setHealth(h.getHealth() - damage.getDamage());
    }
}
