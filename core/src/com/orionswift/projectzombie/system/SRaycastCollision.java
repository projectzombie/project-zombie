package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.RayCastCallback;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CRaycastCollision;
import com.orionswift.projectzombie.event.EEntitiesCollide;
import com.orionswift.projectzombie.screen.GS;

import java.util.ArrayList;
import java.util.List;

public class SRaycastCollision extends IteratingSystem {
    public SRaycastCollision() {
        super(Family.all(CBody.class, CRaycastCollision.class).get());
    }

    private List<Entity> hits = new ArrayList<>();

    @Override
    protected void processEntity(final Entity entity, float deltaTime) {
        CBody body = entity.getComponent(CBody.class);
        CRaycastCollision rc = entity.getComponent(CRaycastCollision.class);

        Vector2 to = body.getPosition().cpy();

        hits.clear();
        // We DO need to do it in BOTH directions, as Box2D ignores any entity inside the start position.
        run(entity, rc.getOldPos(), to);
        run(entity, to, rc.getOldPos());

        for (Entity hit : hits) {
            GS.i.getBus().publish(new EEntitiesCollide(entity, hit));
        }

        rc.getOldPos().set(to);
    }

    public void run(final Entity bullet, Vector2 from, Vector2 to) {
        // Box2D crashes if distance == 0
        if (from.dst2(to) > 0.001f) {
            GS.i.getWorld().rayCast(new RayCastCallback() {
                @Override
                public float reportRayFixture(Fixture fixture, Vector2 point, Vector2 normal, float fraction) {
                    if (fixture.getBody().getUserData() instanceof Entity) {
                        // Sometimes the bullet hits itself
                        if (fixture.getBody().getUserData() != bullet) {
                            hits.add((Entity) fixture.getBody().getUserData());
                        }
                    }
                    return 1;
                }
            }, from, to);
        }
    }
}
