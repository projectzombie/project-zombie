package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.event.EWaveEnd;
import com.orionswift.projectzombie.event.EWaveStart;
import com.orionswift.projectzombie.screen.GS;

public class SWave extends EntitySystem {
    private boolean isDoingWave;

    @Override
    public void update(float deltaTime) {
        if (!isDoingWave && (GS.i.getTime() % 1) > STime.nightStart) {
            isDoingWave = true;
            GS.i.setWave((int) GS.i.getTime() + 1);
            GS.i.getBus().publish(new EWaveStart(GS.i.getWave()));
        } else if (isDoingWave && (GS.i.getTime() - (GS.i.getWave() - 1)) > (1 + STime.nightEnd)) {
            isDoingWave = false;
            GS.i.getBus().publish(new EWaveEnd());
        }
    }
}
