package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.math.Vector2;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.event.ESpawnZombieRandomly;
import com.orionswift.projectzombie.event.EStartFiringWeapon;
import com.orionswift.projectzombie.event.ESwitchWeapon;
import com.orionswift.projectzombie.screen.GS;
import com.orionswift.projectzombie.util.EntityCreator;
import com.orionswift.projectzombie.weapon.Weapon;
import com.orionswift.projectzombie.weapon.ZombieBite;
import com.orionswift.projectzombie.weapon.ZombieBossBite;
import net.engio.mbassy.listener.Handler;

import java.lang.reflect.InvocationTargetException;

public class SSpawnZombieRandomly extends EntitySystem {
    @Handler
    public void spawnZombieRandomly(ESpawnZombieRandomly szr) {
        float circumscribingCircleRadius = new Vector2(GS.i.getCamera().viewportWidth / 2, GS.i.getCamera().viewportHeight / 2).len();
        Vector2 position = new Vector2(circumscribingCircleRadius, 0).rotate((float) (Math.random() * 360)).add(GS.i.getCamera().position.x, GS.i.getCamera().position.y);

        Entity zombie = szr.getType().getFactory().apply(szr.getMultiplier());
        zombie.getComponent(CBody.class).setTransform(position, 0);
        GS.i.getEngine().addEntity(zombie);

        GS.i.getBus().publish(new EStartFiringWeapon(zombie));
    }
}
