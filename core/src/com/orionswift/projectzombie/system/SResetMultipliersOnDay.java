package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.orionswift.projectzombie.component.CMultiplier;
import com.orionswift.projectzombie.event.EWaveEnd;
import com.orionswift.projectzombie.screen.GS;
import net.engio.mbassy.listener.Handler;

public class SResetMultipliersOnDay extends EntitySystem {
    @Handler
    public void waveEnd(EWaveEnd we) {
        for (Entity entity : GS.i.getEngine().getEntitiesFor(Family.all(CMultiplier.class).get())) {
            CMultiplier mul = entity.getComponent(CMultiplier.class);
            mul.setDamage(1);
        }
    }
}
