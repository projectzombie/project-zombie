package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CDespawnOnDay;
import com.orionswift.projectzombie.component.CRemoveOnAnimatedSpriteFinish;
import com.orionswift.projectzombie.component.CRender;
import com.orionswift.projectzombie.event.EWaveEnd;
import com.orionswift.projectzombie.event.EWaveStart;
import com.orionswift.projectzombie.util.Assets;
import com.orionswift.projectzombie.util.SpriteInfo;
import net.dermetfan.gdx.graphics.g2d.AnimatedSprite;
import net.engio.mbassy.listener.Handler;

public class SDespawnOnDay extends IteratingSystem {
    private float timer;

    public SDespawnOnDay() {
        super(Family.all(CDespawnOnDay.class, CRender.class).get());
    }

    @Handler
    public void waveStart(EWaveStart event) {
        timer = -1;
    }

    @Handler
    public void waveEnd(EWaveEnd event) {
        timer = 0;
    }

    @Override
    public void update(float deltaTime) {
        if (timer >= 0) {
            timer += deltaTime;
        }

        super.update(deltaTime);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        CDespawnOnDay dod = entity.getComponent(CDespawnOnDay.class);
        CRender render = entity.getComponent(CRender.class);

        if (dod.getTimeOffset() < timer) {
            entity.remove(CDespawnOnDay.class);

            AnimatedSprite sprite = new AnimatedSprite(Assets.despawn);
            sprite.setSize(1, 1);


            for (Component c : entity.getComponents()) {
                if (!(c instanceof CRender || c instanceof CBody)) {
                    entity.remove(c.getClass());
                }
            }

            render.getSprites().clear();
            render.getSprites().add(new SpriteInfo(sprite));

            entity.add(new CRemoveOnAnimatedSpriteFinish(sprite));
        }
    }
}
