package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.orionswift.projectzombie.component.CAmmoBox;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CDirectionalInput;
import com.orionswift.projectzombie.component.CHealth;
import com.orionswift.projectzombie.component.CHealthBox;
import com.orionswift.projectzombie.component.CPipeBombs;
import com.orionswift.projectzombie.component.CPlayerInput;
import com.orionswift.projectzombie.component.CWeapon;
import com.orionswift.projectzombie.component.CWeaponStash;
import com.orionswift.projectzombie.event.EGunReload;
import com.orionswift.projectzombie.event.ESwitchWeapon;
import com.orionswift.projectzombie.screen.GS;
import com.orionswift.projectzombie.util.EntityCreator;
import com.orionswift.projectzombie.weapon.Gun;

public class SPlayerInput extends IteratingSystem implements InputProcessor {

    @SuppressWarnings("unchecked")
    public SPlayerInput() {
        super(Family.all(CDirectionalInput.class, CPlayerInput.class, CWeaponStash.class).get());
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        CDirectionalInput input = entity.getComponent(CDirectionalInput.class);
        CPlayerInput playerInput = entity.getComponent(CPlayerInput.class);

        input.getMoveDirection().setZero();
        if (Gdx.input.isKeyPressed(playerInput.getUpKey()))
            input.getMoveDirection().add(0, 1);
        if (Gdx.input.isKeyPressed(playerInput.getLeftKey()))
            input.getMoveDirection().add(-1, 0);
        if (Gdx.input.isKeyPressed(playerInput.getDownKey()))
            input.getMoveDirection().add(0, -1);
        if (Gdx.input.isKeyPressed(playerInput.getRightKey()))
            input.getMoveDirection().add(1, 0);
        if (Gdx.input.isKeyJustPressed(Input.Keys.R))
            GS.i.getBus().publish(new EGunReload(entity));
        if (Gdx.input.isKeyJustPressed(Input.Keys.M)) {
            SGameMusic.toggleMute();
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.H)) {
            CHealthBox healthBox = entity.getComponent(CHealthBox.class);
            CHealth health = entity.getComponent(CHealth.class);
            if (healthBox != null && health != null) {
            	if(health.getHealth() < health.getMaxHealth()) {
	                if (healthBox.getCount() > 0) {
	                    healthBox.setCount(healthBox.getCount() - 1);
	
	                    CHealth h = entity.getComponent(CHealth.class);
	                    h.setHealth(Math.min(h.getHealth() + 10, h.getMaxHealth()));
	                }
            	}
            }
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.T)) {
            CPipeBombs pb = entity.getComponent(CPipeBombs.class);
            if (pb.getCount() > 0) {
                pb.setCount(pb.getCount() - 1);

                Entity newPb = EntityCreator.createPipeBomb();
                newPb.getComponent(CBody.class).setTransform(entity.getComponent(CBody.class).getPosition(), 0);

                Vector3 mousePositionInWorldSpace = GS.i.getCamera().unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
                float mouseAngle = new Vector2(mousePositionInWorldSpace.x, mousePositionInWorldSpace.y)
                        .sub(entity.getComponent(CBody.class).getPosition()).angle();

                newPb.getComponent(CBody.class).setLinearVelocity(new Vector2(10, 0).rotate(mouseAngle));
                newPb.getComponent(CBody.class).setAngularVelocity(5);

                GS.i.getEngine().addEntity(newPb);
            }
        }
        
        if (Gdx.input.isKeyJustPressed(Input.Keys.F)) {
            CAmmoBox ammoBox = entity.getComponent(CAmmoBox.class);
            CWeapon cw = entity.getComponent(CWeapon.class);
            Gun gun = (Gun) cw.getWeapon();
            if (ammoBox != null && cw != null && gun != null) {
                if (ammoBox.getCount() > 0 && !gun.getAmmo().hasReachedMax()) {
	                	ammoBox.setCount(ammoBox.getCount() - 1);
		                if(gun.getAmmo().getRealTotal() < gun.getAmmo().getFull() + gun.getAmmo().getCapacity())
		                	
		                if (gun.getAmmo().getTotal() + gun.getAmmo().getCurrent() < gun.getAmmo().getFull() + gun.getAmmo().getCapacity()) {
		                    gun.getAmmo().add((int) (gun.getAmmo().getFull() * 0.5f));
		                }
                	}
            }
        }

        input.getMoveDirection().nor();
    }

    @Override
    public boolean keyDown(int keycode) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        for (Entity entity : GS.i.getEngine().getEntitiesFor(Family.all(CWeaponStash.class).get())) {
            CWeaponStash weaponStash = entity.getComponent(CWeaponStash.class);
            CWeapon weapon = entity.getComponent(CWeapon.class);

            int dir = (int) Math.signum(amount);

            if (dir != 0) {
                if (weaponStash.getOwnedWeapons().size() > 0) {
                    weaponStash.setWeaponIndex((weaponStash.getWeaponIndex() - 1) % weaponStash.getOwnedWeapons().size());
                    if (weaponStash.getWeaponIndex() < 0)
                        weaponStash.setWeaponIndex(weaponStash.getWeaponIndex() + weaponStash.getOwnedWeapons().size());
                    GS.i.getBus().publish(new ESwitchWeapon(entity, weapon == null ? null : weapon.getWeapon(), weaponStash.getOwnedWeapons().get(weaponStash.getWeaponIndex())));
                }
            }
        }
        return true;
    }
}
