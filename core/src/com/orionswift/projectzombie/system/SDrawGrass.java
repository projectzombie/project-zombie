package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.screen.GS;
import com.orionswift.projectzombie.util.Assets;

public class SDrawGrass extends EntitySystem {
    @Override
    public void update(float deltaTime) {
        GS.i.getBatch().begin();
        int x1 = (int) (GS.i.getCamera().position.x - GS.i.getCamera().viewportWidth / 2) - 1;
        int x2 = (int) (GS.i.getCamera().position.x + GS.i.getCamera().viewportWidth / 2) + 1;
        int y1 = (int) (GS.i.getCamera().position.y - GS.i.getCamera().viewportHeight / 2) - 1;
        int y2 = (int) (GS.i.getCamera().position.y + GS.i.getCamera().viewportHeight / 2) + 1;

        for (int x = x1; x < x2; x++) {
            for (int y = y1; y < y2; y++) {
                GS.i.getBatch().draw(Assets.sprites.findRegion("tiles/grass"), x, y, 1, 1);
            }
        }


        GS.i.getBatch().end();
    }
}
