package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.Vector2;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CCameraFollow;
import com.orionswift.projectzombie.screen.GS;
import com.orionswift.projectzombie.util.NumericalSpringing;

public class SCameraFollow extends IteratingSystem {
    public SCameraFollow() {
        super(Family.all(CCameraFollow.class, CBody.class).get(), -99999);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        CBody body = entity.getComponent(CBody.class);
        CCameraFollow cameraFollow = entity.getComponent(CCameraFollow.class);

        // This is called every frame so no need for the velocity parameter
        Vector2 pos = new Vector2(GS.i.getCamera().position.x, GS.i.getCamera().position.y);
        NumericalSpringing.springVector(pos, new Vector2(), body.getPosition(), cameraFollow.getDampingRatio(), cameraFollow.getAngularFrequency(), deltaTime);

        GS.i.getCamera().position.set(pos, 0);
        GS.i.getCamera().update();

        GS.i.getBatch().setProjectionMatrix(GS.i.getCamera().combined);
        GS.i.getShapeRenderer().setProjectionMatrix(GS.i.getCamera().combined);
    }
}
