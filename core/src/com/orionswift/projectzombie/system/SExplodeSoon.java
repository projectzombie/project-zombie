package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CExplodeSoon;
import com.orionswift.projectzombie.event.EExplosion;
import com.orionswift.projectzombie.screen.GS;

public class SExplodeSoon extends IteratingSystem {
    public SExplodeSoon() {
        super(Family.all(CExplodeSoon.class, CBody.class).get());
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        CExplodeSoon ces = entity.getComponent(CExplodeSoon.class);
        ces.setTime(ces.getTime() - deltaTime);

        if (ces.getTime() <= 0) {
            GS.i.getBus().publish(new EExplosion(entity, ces.getDamage()));
            GS.i.getEngine().removeEntity(entity);
        }
    }
}
