package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.audio.Music;
import com.orionswift.projectzombie.screen.GS;
import com.orionswift.projectzombie.util.Assets;

// In-game day;night cycle music system
public class SGameMusic extends EntitySystem {

    private static Music dayMusic;
    private static Music nightMusic;

    public static boolean muted = false;

    public static boolean isTurningDay = false;
    public static boolean isTurningNight = false;

    private float oldDayVolume;
    private float oldNightVolume;

    public SGameMusic() {
        // Music is set to day by default
        dayMusic = Assets.dayMusic;
        nightMusic = Assets.nightMusic;
        dayMusic.setVolume(0);
        nightMusic.setVolume(0);
        dayMusic.play();
        nightMusic.play();

        dayMusic.setLooping(true);
        nightMusic.setLooping(true);
    }

    @Override
    public void update(float deltaTime) {
        if (GS.i.pause)
            return;

        float dayVolume = 0;
        float frac = GS.i.getTime() % 1;
        if (frac < STime.nightEnd) dayVolume = 0;
        if (frac >= STime.nightEnd && frac < STime.dayStart)
            dayVolume = (frac - STime.nightEnd) / (STime.dayStart - STime.nightEnd);
        if (frac >= STime.dayStart && frac < STime.dayEnd) dayVolume = 1;
        if (frac >= STime.dayEnd && frac < STime.nightStart)
            dayVolume = 1 - ((frac - STime.dayEnd) / (STime.nightStart - STime.dayEnd));
        if (frac >= STime.nightStart) dayVolume = 0;

        float nightVolume = 1 - dayVolume;

        if (this.oldNightVolume == 0 && nightVolume > 0) {
            float startTime = (STime.getCurrentTime() - STime.dayEnd) * STime.secondsPerDay;
            nightMusic.setPosition(startTime);
        }

        if (this.oldDayVolume == 0 && dayVolume > 0) {
            float startTime = (STime.getCurrentTime() - STime.nightEnd) * STime.secondsPerDay;
            dayMusic.setPosition(startTime);
        }

        dayMusic.setVolume(muted ? 0 : dayVolume);
        nightMusic.setVolume(muted ? 0 : nightVolume);

        this.oldDayVolume = dayVolume;
        this.oldNightVolume = nightVolume;
    }

    public static void toggleMute() {
        muted = !muted;
    }

    public static void pause() {
        dayMusic.pause();
        nightMusic.pause();
    }

    public static void play() {
        dayMusic.play();
        nightMusic.play();
    }
}
