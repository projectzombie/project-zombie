package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.orionswift.projectzombie.component.CAggro;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CFaceAggro;

public class SFaceAggro extends IteratingSystem {
    public SFaceAggro() {
        super(Family.all(CBody.class, CAggro.class, CFaceAggro.class).get());
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        CBody body = entity.getComponent(CBody.class);
        CAggro aggro = entity.getComponent(CAggro.class);

        if (aggro.getAggro().isPresent()) {
            body.setTransform(body.getPosition(), aggro.getAggro().get().getComponent(CBody.class).getPosition().cpy().sub(body.getPosition()).angleRad());
        }
    }
}
