package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.component.CHealth;
import com.orionswift.projectzombie.event.EDamage;
import com.orionswift.projectzombie.event.EEntityDeath;
import com.orionswift.projectzombie.screen.GS;
import net.engio.mbassy.listener.Handler;

public class SKillOnHealthZero extends EntitySystem {
    @Handler(priority = -1)
    public void damage(EDamage damage) {
        Entity damagee = damage.getDamagee();
        CHealth h = damagee.getComponent(CHealth.class);
        
        if(h == null)
        	return;
        
        if (h.getHealth() <= 0 && h.getHealth() + damage.getDamage() > 0) {
            GS.i.getBus().publish(new EEntityDeath(damagee));
        }
    }
}
