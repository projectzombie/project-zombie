package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.Vector2;
import com.orionswift.projectzombie.component.CAggro;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CExplodeOnNear;
import com.orionswift.projectzombie.event.EExplosion;
import com.orionswift.projectzombie.screen.GS;

public class SExplodeOnNear extends IteratingSystem {
	public SExplodeOnNear() {
		super(Family.all(CBody.class, CExplodeOnNear.class, CAggro.class).get());
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void processEntity(Entity entity, float deltaTime) {
		CAggro aggro = entity.getComponent(CAggro.class);
		if(aggro.getAggro().isPresent()) {
			CExplodeOnNear eon = entity.getComponent(CExplodeOnNear.class);
			if(eon == null)
				return;
			Vector2 pos = entity.getComponent(CBody.class).getPosition();
			Vector2 targ = aggro.getAggro().get().getComponent(CBody.class).getPosition();
			if(Vector2.dst2(pos.x, pos.y, targ.x, targ.y) < eon.getRadius()) {
				GS.i.getBus().publish(new EExplosion(entity, eon.getDamage()));
				GS.i.getEngine().removeEntity(entity);
			}
		}
	}
}
