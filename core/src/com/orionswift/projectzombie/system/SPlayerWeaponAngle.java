package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CPlayerWeaponAngle;
import com.orionswift.projectzombie.component.CWeapon;
import com.orionswift.projectzombie.screen.GS;

public class SPlayerWeaponAngle extends IteratingSystem {
    public SPlayerWeaponAngle() {
        super(Family.all(CBody.class, CPlayerWeaponAngle.class, CWeapon.class).get());
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        CBody body = entity.getComponent(CBody.class);
        CWeapon weapon = entity.getComponent(CWeapon.class);

        Vector3 mousePositionInWorldSpace = GS.i.getCamera().unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
        float mouseAngle = new Vector2(mousePositionInWorldSpace.x, mousePositionInWorldSpace.y)
                .sub(body.getPosition()).angle();

        weapon.getWeapon().setAngle(mouseAngle);
    }
}
