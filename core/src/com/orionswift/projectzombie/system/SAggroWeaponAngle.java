package com.orionswift.projectzombie.system;


import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.orionswift.projectzombie.component.CAggro;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CAggroWeaponAngle;
import com.orionswift.projectzombie.component.CWeapon;

public class SAggroWeaponAngle extends IteratingSystem {
    public SAggroWeaponAngle() {
        super(Family.all(CBody.class, CWeapon.class, CAggro.class, CAggroWeaponAngle.class).get());
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        if (entity.getComponent(CAggro.class).getAggro().isPresent()) {
            entity.getComponent(CWeapon.class).getWeapon().setAngle(entity.getComponent(CAggro.class).getAggro().get().getComponent(CBody.class).getPosition().cpy().sub(entity.getComponent(CBody.class).getPosition()).angle());
        }
    }
}
