package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.component.CDropCoinsOnDeath;
import com.orionswift.projectzombie.event.EEntityDeath;
import com.orionswift.projectzombie.screen.GS;

import net.engio.mbassy.listener.Handler;

public class SDropCoinsOnDeath extends EntitySystem {
	@Handler
	public void dropCoins(EEntityDeath ed) {
		if(ed.getEntity().getComponent(CDropCoinsOnDeath.class) != null) {
			float coins = ed.getEntity().getComponent(CDropCoinsOnDeath.class).getCoins();
			GS.i.money.add(coins);
		}
	}
}
