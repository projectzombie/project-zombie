package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.gdx.InputProcessor;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CPlayerFireWeapon;
import com.orionswift.projectzombie.component.CWeapon;
import com.orionswift.projectzombie.event.EStartFiringWeapon;
import com.orionswift.projectzombie.event.EStopFiringWeapon;
import com.orionswift.projectzombie.screen.GS;

public class SPlayerFireWeapon extends EntitySystem implements InputProcessor {
    private Family fam = Family.all(CBody.class, CPlayerFireWeapon.class, CWeapon.class).get();

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        for (Entity entity : GS.i.getEngine().getEntitiesFor(fam)) {
            GS.i.getBus().publish(new EStartFiringWeapon(entity));
        }
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        for (Entity entity : GS.i.getEngine().getEntitiesFor(fam)) {
            GS.i.getBus().publish(new EStopFiringWeapon(entity));
        }
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
