package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IntervalIteratingSystem;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.RayCastCallback;
import com.google.common.base.Optional;
import com.orionswift.projectzombie.component.CAggro;
import com.orionswift.projectzombie.component.CAggroDrawer;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.screen.GS;

public class SAggro extends IntervalIteratingSystem {
    private int counter;

    public SAggro() {
        super(Family.all(CAggro.class).get(), 0.02f);
    }

    @Override
    protected void updateInterval() {
        super.updateInterval();
        counter++;
    }

    @Override
    protected void processEntity(Entity entity) {
        if (entity.getId() % 10 == counter % 10) {
            CAggro aggro = entity.getComponent(CAggro.class);
            CBody thisBody = entity.getComponent(CBody.class);

            float closestDistSq = 9999999;
            Entity closest = null;

            // O(n^2) :(
            for (final Entity currEntity : GS.i.getEngine().getEntitiesFor(Family.all(CBody.class, CAggroDrawer.class).get())) {
                if (currEntity != entity) {
                    CAggroDrawer ad = currEntity.getComponent(CAggroDrawer.class);
                    CBody body = currEntity.getComponent(CBody.class);

                    if (aggro.getMask().get(ad.getMaskIndex())) {
                        float distSq = body.getPosition().dst2(thisBody.getPosition());
                        if (distSq < closestDistSq) {
                            final boolean[] hasLineOfSight = {true};
                            if (!aggro.isRequireLineOfSight()) {
                                hasLineOfSight[0] = true;
                            } else {
                                GS.i.getWorld().rayCast(new RayCastCallback() {
                                    @Override
                                    public float reportRayFixture(Fixture fixture, Vector2 point, Vector2 normal, float fraction) {
                                        if (fixture.getBody().getUserData() != currEntity) {
                                            hasLineOfSight[0] = false;
                                            return 0;
                                        }
                                        return 1;
                                    }
                                }, thisBody.getPosition(), body.getPosition());
                            }

                            if (hasLineOfSight[0]) {
                                closestDistSq = distSq;
                                closest = currEntity;
                            }
                        }
                    }
                }
            }

            aggro.setAggro(Optional.fromNullable(closest));
        }
    }
}
