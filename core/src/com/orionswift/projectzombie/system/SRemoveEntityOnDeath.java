package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CTile;
import com.orionswift.projectzombie.event.EEntityDeath;
import com.orionswift.projectzombie.event.ERemoveTile;
import com.orionswift.projectzombie.screen.GS;
import com.orionswift.projectzombie.tile.TilePosition;

import net.engio.mbassy.listener.Handler;

public class SRemoveEntityOnDeath extends EntitySystem {
    @Handler
    public void entityDeath(EEntityDeath entityDeath) {
    	if(entityDeath.getEntity().getComponent(CTile.class) != null) {
    		CBody transform = entityDeath.getEntity().getComponent(CBody.class);
    		GS.i.getBus().publish(new ERemoveTile(
    					new TilePosition((int) Math.floor(transform.getPosition().x), (int) Math.floor(transform.getPosition().y)), false
    		));
    	}
        GS.i.getEngine().removeEntity(entityDeath.getEntity());
    }
}
