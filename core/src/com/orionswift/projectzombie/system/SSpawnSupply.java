package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IntervalSystem;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.math.Vector2;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CSpawnAround;
import com.orionswift.projectzombie.screen.GS;
import com.orionswift.projectzombie.util.SupplyType;

public class SSpawnSupply extends IntervalSystem {
    public SSpawnSupply() {
        super(0.25f);
    }

    @Override
    protected void updateInterval() {
        for (SupplyType type : SupplyType.values()) {
            // The world is split up into 8x8 imaginary chunks
            // For every chunk that's close to the player, if it hasn't been processed yet, we'll spawn a box there (maybe)

            ImmutableArray<Entity> player = GS.i.getEngine().getEntitiesFor(Family.all(CSpawnAround.class).get());

            if (player.size() > 0) {
                Vector2 playerChunk = player.first().getComponent(CBody.class).getPosition().cpy().scl(1 / 8f);
                playerChunk.x = (float) Math.floor(playerChunk.x);
                playerChunk.y = (float) Math.floor(playerChunk.y);
                Vector2 tmp = new Vector2();

                for (int xx = -3; xx <= 3; xx++) {
                    for (int yy = -3; yy <= 3; yy++) {
                        tmp.set(playerChunk.x + xx, playerChunk.y + yy);
                        if (!type.getSpawned().contains(tmp)) {

                            boolean spawn = Math.random() < type.getRarity();

                            if (spawn) {
                                Entity entity = type.getEntityFactory().get();
                                entity.getComponent(CBody.class).setTransform(tmp.x * 8 + (float) (Math.random() * 8f), tmp.y * 8 + (float) (Math.random() * 8f), 0);
                                GS.i.getEngine().addEntity(entity);
                            }

                            type.getSpawned().add(tmp.cpy());
                        }
                    }
                }
            }
        }
    }
}
