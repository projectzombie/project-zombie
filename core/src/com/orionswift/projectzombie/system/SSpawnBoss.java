package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.event.ESpawnZombieRandomly;
import com.orionswift.projectzombie.screen.GS;
import com.orionswift.projectzombie.util.ZombieType;

public class SSpawnBoss extends EntitySystem {
    private boolean hasSpawned;

    @Override
    public void update(float deltaTime) {
        if (!hasSpawned && (GS.i.getTime() % 1) > 0 && (GS.i.getTime() % 1) < STime.nightEnd) {
            hasSpawned = true;

            GS.i.getBus().publish(new ESpawnZombieRandomly(ZombieType.BOSS, 1));
        } else if ((GS.i.getTime() % 1) > STime.nightStart) {
            hasSpawned = false;
        }
    }
}
