package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.graphics.Color;
import com.orionswift.projectzombie.screen.GS;

public class SDayNightLighting extends EntitySystem {
    private Color dayColor = new Color(1f, 1f, 0.8f, 1f);
    private Color nightColor = new Color(0.07f, 0.07f, 0.09f, 1f);

    @Override
    public void update(float deltaTime) {
        float frac = GS.i.getTime() % 1;

        Color outColor = null;
        if (frac < STime.nightEnd) outColor = nightColor;
        if (frac >= STime.nightEnd && frac < STime.dayStart) outColor = nightColor.cpy().lerp(dayColor, (frac - STime.nightEnd) / (STime.dayStart - STime.nightEnd));
        if (frac >= STime.dayStart && frac < STime.dayEnd) outColor = dayColor;
        if (frac >= STime.dayEnd && frac < STime.nightStart) outColor = dayColor.cpy().lerp(nightColor, (frac - STime.dayEnd) / (STime.nightStart - STime.dayEnd));
        if (frac >= STime.nightStart) outColor = nightColor;

        GS.i.getRayHandler().setAmbientLight(outColor);
    }
}
