package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.orionswift.projectzombie.screen.GS;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SParticleEmitters extends EntitySystem {
    private List<ParticleEffect> effects;

    public SParticleEmitters() {
        this.effects = new ArrayList<>();
    }

    public List<ParticleEffect> getEffects() {
        return effects;
    }

    @Override
    public void update(float deltaTime) {
        GS.i.getBatch().begin();

        for (ParticleEffect particleEffect : effects) {
            particleEffect.draw(GS.i.getBatch(), deltaTime);

        }

        GS.i.getBatch().end();
    }
}
