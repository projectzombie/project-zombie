package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.component.CAnimationOnShoot;
import com.orionswift.projectzombie.event.EPlayAnimation;
import com.orionswift.projectzombie.event.EStartFiringWeapon;
import com.orionswift.projectzombie.event.EStopAnimation;
import com.orionswift.projectzombie.event.EStopFiringWeapon;
import com.orionswift.projectzombie.screen.GS;
import net.engio.mbassy.listener.Handler;

public class SAnimationOnShoot extends EntitySystem {
    @Handler
    public void startShooting(EStartFiringWeapon startFiringWeapon) {
        CAnimationOnShoot aos = startFiringWeapon.getEntity().getComponent(CAnimationOnShoot.class);
        if (aos != null) {
            GS.i.getBus().publish(new EPlayAnimation(startFiringWeapon.getEntity(), aos.getAnimation(), aos.getPriority()));
        }
    }

    @Handler
    public void stopShooting(EStopFiringWeapon stopFiringWeapon) {
        CAnimationOnShoot aos = stopFiringWeapon.getEntity().getComponent(CAnimationOnShoot.class);
        if (aos != null) {
            GS.i.getBus().publish(new EStopAnimation(stopFiringWeapon.getEntity(), aos.getAnimation()));
        }
    }
}
