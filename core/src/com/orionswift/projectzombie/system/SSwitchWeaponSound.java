package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.event.ESwitchWeapon;
import com.orionswift.projectzombie.util.Assets;
import net.engio.mbassy.listener.Handler;

public class SSwitchWeaponSound extends EntitySystem {
    @Handler
    public void switchWeapon(ESwitchWeapon e) {
        if (e.getOldWeapon() != null && e.getOldWeapon() != e.getNewWeapon()) {
            Assets.weaponSwitch.play(e.getEntity().getComponent(CBody.class).getPosition());
        }
    }
}
