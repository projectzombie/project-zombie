package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.component.CDamageOnTouch;
import com.orionswift.projectzombie.component.CRemoveOnTouch;
import com.orionswift.projectzombie.event.EEntitiesCollide;
import com.orionswift.projectzombie.screen.GS;
import net.engio.mbassy.listener.Handler;

public class SRemoveOnTouch extends EntitySystem {
    @Handler(priority = -1)
    public void entitiesCollide(EEntitiesCollide entitiesCollide) {
        if (entitiesCollide.getEntityA().getComponent(CRemoveOnTouch.class) != null) {
            GS.i.getEngine().removeEntity(entitiesCollide.getEntityA());
        } else if (entitiesCollide.getEntityB().getComponent(CRemoveOnTouch.class) != null) {
            GS.i.getEngine().removeEntity(entitiesCollide.getEntityB());
        }
    }
}
