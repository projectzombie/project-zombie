package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector3;
import com.orionswift.projectzombie.event.EPlaceTile;
import com.orionswift.projectzombie.event.ERemoveTile;
import com.orionswift.projectzombie.event.EStartTilePlacement;
import com.orionswift.projectzombie.event.EStopTilePlacement;
import com.orionswift.projectzombie.screen.GS;
import com.orionswift.projectzombie.tile.TilePosition;
import com.orionswift.projectzombie.tile.TileType;
import net.engio.mbassy.listener.Handler;

import java.util.HashSet;
import java.util.Set;

public class STilePlacement extends EntitySystem implements InputProcessor {
    private TileType selected;
    private Set<TilePosition> placedDuringCurrentSession = new HashSet<>();

    public boolean isTiling;

    public STilePlacement() {
        super(1001);
    }

    @Handler
    public void startTilePlacement(EStartTilePlacement stp) {
        GS.i.timePause();
        isTiling = true;
        selected = stp.getTileType();
    }

    @Handler
    public void stopTilePlacement(EStopTilePlacement stp) {
        GS.i.timeUnpause();
        isTiling = false;

        placedDuringCurrentSession.clear();
    }

    @Override
    public void update(float deltaTime) {
        if (isTiling) {
            Vector3 mousePos = GS.i.getCamera().unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
            TilePosition pos = new TilePosition((int) Math.floor(mousePos.x), (int) Math.floor(mousePos.y));

            if (!GS.i.getTiles().containsKey(pos)) {
                GS.i.getBatch().begin();
                Color oldColor = GS.i.getBatch().getColor();
                GS.i.getBatch().setColor(1, 1, 1, 0.5f);
                GS.i.getBatch().draw(selected.getTexture(), pos.getX(), pos.getY(), 1, 1);
                GS.i.getBatch().setColor(oldColor);
                GS.i.getBatch().end();
            }
            drawGrid();
        }
    }

    private void drawGrid() {
        GS.i.getShapeRenderer().begin(ShapeRenderer.ShapeType.Line);
        GS.i.getShapeRenderer().setColor(1, 1, 1, 1);
        int x1 = (int) (GS.i.getCamera().position.x - GS.i.getCamera().viewportWidth / 2) - 1;
        int x2 = (int) (GS.i.getCamera().position.x + GS.i.getCamera().viewportWidth / 2) + 1;
        int y1 = (int) (GS.i.getCamera().position.y - GS.i.getCamera().viewportHeight / 2) - 1;
        int y2 = (int) (GS.i.getCamera().position.y + GS.i.getCamera().viewportHeight / 2) + 1;

        for (int x = x1; x < x2; x++) {
            GS.i.getShapeRenderer().line(x, y1, x, y2);
        }

        for (int y = y1; y < y2; y++) {
            GS.i.getShapeRenderer().line(x1, y, x2, y);
        }
        GS.i.getShapeRenderer().end();
    }

    private void place() {
        if (isTiling) {
            Vector3 mousePos = GS.i.getCamera().unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
            TilePosition pos = new TilePosition((int) Math.floor(mousePos.x), (int) Math.floor(mousePos.y));

            if (!GS.i.getTiles().containsKey(pos)) {
                EPlaceTile event = new EPlaceTile(pos, selected);
                GS.i.getBus().publish(event);
                if (!event.isCancelled()) {
                    placedDuringCurrentSession.add(pos);
                }
            }
        }
    }

    private void break_() {
        if (isTiling) {
            Vector3 mousePos = GS.i.getCamera().unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
            TilePosition pos = new TilePosition((int) Math.floor(mousePos.x), (int) Math.floor(mousePos.y));

            if (GS.i.getTiles().containsKey(pos)) {
                ERemoveTile event = new ERemoveTile(pos, placedDuringCurrentSession.contains(pos));
                GS.i.getBus().publish(event);
                if (!event.isCancelled()) {
                    placedDuringCurrentSession.remove(pos);
                }
            }
        }
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (button == Input.Buttons.LEFT) {
            place();
        } else if (button == Input.Buttons.RIGHT) {
            break_();
        }
        return isTiling;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
            place();
        } else if (Gdx.input.isButtonPressed(Input.Buttons.RIGHT)) {
            break_();
        }
        return isTiling;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}