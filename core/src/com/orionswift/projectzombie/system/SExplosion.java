package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.QueryCallback;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CRepel;
import com.orionswift.projectzombie.event.EDamage;
import com.orionswift.projectzombie.event.EExplosion;
import com.orionswift.projectzombie.screen.GS;
import net.engio.mbassy.listener.Handler;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SExplosion extends EntitySystem {
    @Handler
    public void explosion(final EExplosion explosion) {
        final float radius = 5;
        List<Fixture> res = new ArrayList<>();

        final Vector2 position = explosion.getSource().getComponent(CBody.class).getPosition();

        final Body body = explosion.getSource().getComponent(CBody.class).getBody();
        final Set<Body> bodiesDone = new HashSet<>();
        GS.i.getWorld().QueryAABB(new QueryCallback() {
            @Override
            public boolean reportFixture(Fixture fixture) {
                Body entityBody = fixture.getBody();

                if (!bodiesDone.contains(entityBody)) {
                    bodiesDone.add(entityBody);

                    if (entityBody.getUserData() instanceof Entity) {
                        if (entityBody.getPosition().dst2(body.getPosition()) < radius * radius) {
                            Entity entity = (Entity) entityBody.getUserData();
                            if (entity != explosion.getSource()) {
                                Vector2 entityPos = entity.getComponent(CBody.class).getPosition();

                                float falloff = 1 - entityPos.dst2(position) / (radius * radius);
                                GS.i.getBus().publish(new EDamage(entity, explosion.getSource(), explosion.getSource(), explosion.getDamage() * falloff, 7 * falloff, EDamage.KnockbackMode.SourcePosition, EDamage.DamageType.Bullet));
                            }
                        }
                    }
                }
                return true;
            }
        }, position.x - radius, position.y - radius, position.x + radius, position.y + radius);
    }
}