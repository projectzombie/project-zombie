package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CDoor;
import com.orionswift.projectzombie.event.EPlaceTile;
import com.orionswift.projectzombie.event.ERemoveTile;
import com.orionswift.projectzombie.screen.GS;
import com.orionswift.projectzombie.tile.TileEntityHandle;
import com.orionswift.projectzombie.util.Assets;
import com.orionswift.projectzombie.util.EntityCreator;
import com.orionswift.projectzombie.util.SoundEffect;
import net.engio.mbassy.listener.Handler;

public class SPlaceRemoveTile extends EntitySystem {
    @Handler
    public void placeTile(EPlaceTile placeTile) {
        Entity tile = EntityCreator.createTile(placeTile.getType());
        GS.i.getEngine().addEntity(tile);

        tile.getComponent(CBody.class).setTransform(placeTile.getPos().getX() + 0.5f, placeTile.getPos().getY() + 0.5f, 0);
        if (tile.getComponent(CDoor.class) != null) {
            CDoor door = tile.getComponent(CDoor.class);
            door.getAnchorBody().setTransform(placeTile.getPos().getX() + (door.isVertical() ? 0.5f : 0f), placeTile.getPos().getY() + (!door.isVertical() ? 0.5f : 0f), 0);
        }

        GS.i.getTiles().put(placeTile.getPos(), new TileEntityHandle(tile, placeTile.getType()));

        Assets.itemBuy.play();
    }

    @Handler
    public void removeTile(ERemoveTile removeTile) {
        Entity tile = GS.i.getTiles().get(removeTile.getPos()).getEntity();
        if (tile != null) {
            GS.i.getTiles().remove(removeTile.getPos());
            GS.i.getEngine().removeEntity(tile);

            Assets.tileBreak.play();
        }
    }
}
