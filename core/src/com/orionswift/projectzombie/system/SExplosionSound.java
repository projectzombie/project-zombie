package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.event.EExplosion;
import com.orionswift.projectzombie.util.Assets;
import net.engio.mbassy.listener.Handler;

public class SExplosionSound extends EntitySystem {
    @Handler
    public void explosion(EExplosion e) {
        Assets.explode.play(e.getSource().getComponent(CBody.class).getPosition());
    }
}
