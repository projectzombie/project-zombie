package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CRaycastCollision;
import com.orionswift.projectzombie.component.CWeapon;
import com.orionswift.projectzombie.event.EShootBullet;
import com.orionswift.projectzombie.util.EntityCreator;

import net.engio.mbassy.listener.Handler;

public class SFireBulletEntity extends EntitySystem {
    @Handler
    public void shootBullet(EShootBullet sb) {
        CBody srcBody = sb.getShooter().getComponent(CBody.class);
        CWeapon weapon = sb.getShooter().getComponent(CWeapon.class);

        Entity bullet = EntityCreator.createBullet(weapon.getWeapon());
        CBody bulletBody = bullet.getComponent(CBody.class);

        bulletBody.setTransform(sb.getMuzzleOffset().cpy().rotate(sb.getMouseAngle()).add(srcBody.getPosition()), sb.getMouseAngle() * MathUtils.degreesToRadians);
        bulletBody.setLinearVelocity(new Vector2(50, 0).rotate(sb.getMouseAngle()));
        getEngine().addEntity(bullet);

        bullet.getComponent(CRaycastCollision.class).getOldPos().set(bulletBody.getPosition());
    }
}
