package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.screen.GS;

public class STime extends EntitySystem {
    public static float secondsPerDay = 300;

    // Transition must be 20.625 seconds
    public static float transition = 20.625f / secondsPerDay;

    public static float dayEnd = 0.75f;
    public static float nightEnd = 0.25f;

    public static float nightStart = dayEnd + transition;
    public static float dayStart = nightEnd + transition;

    public static boolean pause = false;

    @Override
    public void update(float deltaTime) {
        if (pause)
            return;
        GS.i.setTime(GS.i.getTime() + deltaTime / secondsPerDay);
    }

    public static float getCurrentTime() {
        return GS.i.getTime() % 1;
    }

    public static boolean isDay() {
        return GS.i.getTime() % 1 >= dayStart && GS.i.getTime() % 1 <= dayEnd;
    }

    public static void setToDay() {
        GS.i.setTime((GS.i.getTime() - GS.i.getTime() % 1) + (dayStart));
    }

    public static void setToNight() {
        GS.i.setTime((GS.i.getTime() - GS.i.getTime() % 1) + (nightStart));
    }

}
