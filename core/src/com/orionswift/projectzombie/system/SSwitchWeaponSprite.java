package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.component.CPlayerLegTorsoControls;
import com.orionswift.projectzombie.component.CRender;
import com.orionswift.projectzombie.event.ESwitchWeapon;
import com.orionswift.projectzombie.util.SpriteInfo;
import net.engio.mbassy.listener.Handler;

import java.util.ArrayList;

public class SSwitchWeaponSprite extends EntitySystem {
    @Handler(priority = -1)
    public void switchWeapon(ESwitchWeapon switchWeapon) {
        if (switchWeapon.getEntity().getComponent(CRender.class) != null) {
            if (switchWeapon.getOldWeapon() != null) {
                for (SpriteInfo si : new ArrayList<>(switchWeapon.getEntity().getComponent(CRender.class).getSprites())) {
                    if (si.getSprite() == switchWeapon.getOldWeapon().getWeaponSprite().getSprite()) {
                        switchWeapon.getEntity().getComponent(CRender.class).getSprites().remove(si);
                    }
                }
            }

            if (switchWeapon.getNewWeapon().getWeaponSprite() != null) {
                switchWeapon.getEntity().getComponent(CRender.class).getSprites().add(switchWeapon.getNewWeapon().getWeaponSprite());

                if (switchWeapon.getEntity().getComponent(CPlayerLegTorsoControls.class) != null) {
                    switchWeapon.getEntity().getComponent(CPlayerLegTorsoControls.class).setGun(switchWeapon.getNewWeapon().getWeaponSprite().getSprite());
                }
            } else {
                if (switchWeapon.getEntity().getComponent(CPlayerLegTorsoControls.class) != null) {
                    switchWeapon.getEntity().getComponent(CPlayerLegTorsoControls.class).setGun(null);
                }
            }
        }
    }
}
