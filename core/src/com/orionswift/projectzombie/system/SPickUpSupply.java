package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.component.CPickUpSupply;
import com.orionswift.projectzombie.component.CSupply;
import com.orionswift.projectzombie.event.EEntitiesCollide;
import com.orionswift.projectzombie.screen.GS;
import net.engio.mbassy.listener.Handler;

public class SPickUpSupply extends EntitySystem {
    @Handler
    public void collide(EEntitiesCollide entitiesCollide) {
        boolean pick = false;
        Entity picker = null;
        Entity supply = null;

        if (entitiesCollide.getEntityA().getComponent(CPickUpSupply.class) != null && entitiesCollide.getEntityB().getComponent(CSupply.class) != null) {
            pick = true;
            picker = entitiesCollide.getEntityA();
            supply = entitiesCollide.getEntityB();
        }

        if (entitiesCollide.getEntityA().getComponent(CSupply.class) != null && entitiesCollide.getEntityB().getComponent(CPickUpSupply.class) != null) {
            pick = true;
            supply = entitiesCollide.getEntityA();
            picker = entitiesCollide.getEntityB();
        }

        if (pick) {
            if (supply.getComponent(CSupply.class).getType().getOnPickUp().apply(picker)) {
                GS.i.getEngine().removeEntity(supply);
            }
        }
    }
}
