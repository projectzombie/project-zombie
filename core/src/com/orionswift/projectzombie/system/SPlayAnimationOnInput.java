package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.orionswift.projectzombie.component.CDirectionalInput;
import com.orionswift.projectzombie.component.CPlayAnimationOnInput;
import com.orionswift.projectzombie.component.CRender;
import com.orionswift.projectzombie.event.EPlayAnimation;
import com.orionswift.projectzombie.event.EStopAnimation;
import com.orionswift.projectzombie.screen.GS;

public class SPlayAnimationOnInput extends IteratingSystem {
    public SPlayAnimationOnInput() {
        super(Family.all(CRender.class, CDirectionalInput.class, CPlayAnimationOnInput.class).get());
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        CPlayAnimationOnInput paoi = entity.getComponent(CPlayAnimationOnInput.class);

        if (!entity.getComponent(CDirectionalInput.class).getMoveDirection().isZero() && !paoi.isMoving()) {
            GS.i.getBus().publish(new EPlayAnimation(entity, paoi.getAnimation(), paoi.getPriority()));
            paoi.setMoving(true);
        } else if (entity.getComponent(CDirectionalInput.class).getMoveDirection().isZero() && paoi.isMoving()) {
            GS.i.getBus().publish(new EStopAnimation(entity, paoi.getAnimation()));
            paoi.setMoving(false);
        }
    }
}
