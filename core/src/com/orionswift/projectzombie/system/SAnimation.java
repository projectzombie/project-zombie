package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.component.CRender;
import com.orionswift.projectzombie.event.EPlayAnimation;
import com.orionswift.projectzombie.event.EStopAnimation;
import com.orionswift.projectzombie.util.AnimationsSprite;
import com.orionswift.projectzombie.util.SpriteInfo;
import net.engio.mbassy.listener.Handler;

public class SAnimation extends EntitySystem {
    @Handler
    public void playAnimation(EPlayAnimation playAnimation) {
        CRender render = playAnimation.getEntity().getComponent(CRender.class);
        for (SpriteInfo sprite : render.getSprites()) {
            if (sprite.getSprite() instanceof AnimationsSprite) {
                ((AnimationsSprite) sprite.getSprite()).playAnimation(playAnimation.getAnimation(), playAnimation.getPriority());
            }
        }
    }

    @Handler
    public void stopAnimation(EStopAnimation stopAnimation) {
        CRender render = stopAnimation.getEntity().getComponent(CRender.class);
        for (SpriteInfo sprite : render.getSprites()) {
            if (sprite.getSprite() instanceof AnimationsSprite) {
                ((AnimationsSprite) sprite.getSprite()).removeAnimation(stopAnimation.getAnimation());
            }
        }
    }
}
