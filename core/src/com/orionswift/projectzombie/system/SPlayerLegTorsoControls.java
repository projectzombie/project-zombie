package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CDirectionalInput;
import com.orionswift.projectzombie.component.CPlayerLegTorsoControls;
import com.orionswift.projectzombie.screen.GS;

public class SPlayerLegTorsoControls extends IteratingSystem {

    @SuppressWarnings("unchecked")
    public SPlayerLegTorsoControls() {
        super(Family.all(CBody.class, CDirectionalInput.class, CPlayerLegTorsoControls.class).get());
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        CDirectionalInput input = entity.getComponent(CDirectionalInput.class);
        CBody body = entity.getComponent(CBody.class);
        CPlayerLegTorsoControls pltc = entity.getComponent(CPlayerLegTorsoControls.class);

        Vector3 mousePositionInWorldSpace = GS.i.getCamera()
                .unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
        float mouseAngle = new Vector2(mousePositionInWorldSpace.x, mousePositionInWorldSpace.y).sub(body.getPosition())
                .angle();

        pltc.getTorso().setRotation(mouseAngle);
        pltc.getHead().setRotation(mouseAngle);

        if (pltc.getGun() != null) {
            pltc.getGun().setRotation(mouseAngle);
        }

        if (!input.getMoveDirection().isZero()) {
            pltc.getLegs().setRotation(input.getMoveDirection().angle());
        }

        body.setTransform(body.getPosition(), mouseAngle * MathUtils.degreesToRadians);
    }
}