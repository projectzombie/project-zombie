package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.math.Vector2;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CSoundEffectOnDamage;
import com.orionswift.projectzombie.event.EDamage;

import net.engio.mbassy.listener.Handler;

public class SSoundEffectOnDamage extends EntitySystem {
    @Handler
    public void damage(EDamage damage) {
        if (damage.getDamagee().getComponent(CSoundEffectOnDamage.class) != null) {
            CSoundEffectOnDamage seod = damage.getDamagee().getComponent(CSoundEffectOnDamage.class);
            if (damage.getDamageType() == EDamage.DamageType.Bullet) {
            	Vector2 source = damage.getSource().getComponent(CBody.class).getPosition();
                seod.getBulletSound().play(source);
            }

            if (damage.getDamageType() == EDamage.DamageType.Melee) {
            	Vector2 source = damage.getSource().getComponent(CBody.class).getPosition();
                seod.getMeleeSound().play(source);
            }
        }
    }
}
