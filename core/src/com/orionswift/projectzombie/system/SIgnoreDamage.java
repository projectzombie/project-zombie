package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.component.CIgnoreDamage;
import com.orionswift.projectzombie.event.EDamage;
import net.engio.mbassy.listener.Handler;

public class SIgnoreDamage extends EntitySystem {
    @Handler
    public void damage(EDamage damage) {
        if (damage.getDamagee().getComponent(CIgnoreDamage.class) != null && damage.getSource().getComponent(CIgnoreDamage.class) != null) {
            if (damage.getDamagee().getComponent(CIgnoreDamage.class).getMask().get(damage.getSource().getComponent(CIgnoreDamage.class).getGroup())) {
                damage.setCancelled(true);
            }
        }
    }
}
