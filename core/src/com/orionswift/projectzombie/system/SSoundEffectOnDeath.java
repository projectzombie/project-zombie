package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.math.Vector2;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CSoundEffectOnDeath;
import com.orionswift.projectzombie.event.EEntityDeath;

import net.engio.mbassy.listener.Handler;

public class SSoundEffectOnDeath extends EntitySystem {
	@Handler
	public void soundEffectOnDeath(EEntityDeath ed) {
		if (ed.getEntity().getComponent(CSoundEffectOnDeath.class) != null) {
            ed.getEntity().getComponent(CSoundEffectOnDeath.class).getSound().play(ed.getEntity().getComponent(CBody.class).getPosition());
        }
	}
}