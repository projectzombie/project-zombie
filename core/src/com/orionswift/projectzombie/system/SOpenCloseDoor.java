package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.ashley.utils.ImmutableArray;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CDoor;
import com.orionswift.projectzombie.component.CDoorOpener;
import com.orionswift.projectzombie.screen.GS;

public class SOpenCloseDoor extends IteratingSystem {
    public SOpenCloseDoor() {
        super(Family.all(CBody.class, CDoor.class).get());
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        CBody body = entity.getComponent(CBody.class);
        CDoor door = entity.getComponent(CDoor.class);

        ImmutableArray<Entity> opener = GS.i.getEngine().getEntitiesFor(Family.all(CDoorOpener.class).get());
        if (opener.size() > 0) {
            float dst2 = body.getPosition().dst2(opener.first().getComponent(CBody.class).getPosition());
            if (dst2 < door.getOpenProximity() * door.getOpenProximity()) {
                door.getJoint().enableLimit(false);
            } else {
                door.getJoint().enableLimit(true);
            }
        }
    }
}
