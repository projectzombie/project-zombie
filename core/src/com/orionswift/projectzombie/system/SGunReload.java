package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.component.CWeapon;
import com.orionswift.projectzombie.event.EGunReload;
import com.orionswift.projectzombie.weapon.Gun;

import net.engio.mbassy.listener.Handler;

public class SGunReload extends EntitySystem {
	@Handler
	public void reload(EGunReload gr) {
		Gun gun = (Gun) gr.getEntity().getComponent(CWeapon.class).getWeapon();
		gun.getAmmo().reload();
	}
}
