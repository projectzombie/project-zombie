package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CPlayerLegTorsoControls;
import com.orionswift.projectzombie.component.CRender;
import com.orionswift.projectzombie.screen.GS;
import com.orionswift.projectzombie.util.SpriteInfo;

import java.util.Collections;
import java.util.Comparator;

public class SRender extends IteratingSystem {
    public SRender() {
        super(Family.all(CBody.class, CRender.class).get());
    }

    @Override
    public void update(float deltaTime) {
        GS.i.getBatch().begin();
        super.update(deltaTime);
        GS.i.getBatch().end();
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        CBody body = entity.getComponent(CBody.class);
        CRender render = entity.getComponent(CRender.class);

        Collections.sort(render.getSprites(), new Comparator<SpriteInfo>() {
            @Override
            public int compare(SpriteInfo c1, SpriteInfo c2) {
                return Integer.compare(c1.getZ(), c2.getZ());
            }
        });

        for (SpriteInfo info : render.getSprites()) {
            Sprite sprite = info.getSprite();

            sprite.setPosition(body.getPosition().x - sprite.getWidth() / 2 + info.getOffset().x, body.getPosition().y - sprite.getHeight() / 2 + info.getOffset().y);
            // TODO: This check is really ugly, refactor
            if (entity.getComponent(CPlayerLegTorsoControls.class) == null) {
                sprite.setRotation(body.getAngle() * MathUtils.radiansToDegrees);
            }

            sprite.draw(GS.i.getBatch());
        }
    }
}
