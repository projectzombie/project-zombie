package com.orionswift.projectzombie.system;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CTrailRender;
import com.orionswift.projectzombie.screen.GS;

public class STrailRender extends IteratingSystem {
    public STrailRender() {
        super(Family.all(CBody.class, CTrailRender.class).get(), 1001);
    }

    @Override
    public void update(float deltaTime) {
        GS.i.getShapeRenderer().begin(ShapeRenderer.ShapeType.Filled);
        super.update(deltaTime);
        GS.i.getShapeRenderer().end();
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        CBody body = entity.getComponent(CBody.class);
        CTrailRender tr = entity.getComponent(CTrailRender.class);

        Iterator<CTrailRender.TrailPos> iter = tr.getTrailPositions().iterator();
        while (iter.hasNext()) {
            CTrailRender.TrailPos pos = iter.next();

            if ((System.currentTimeMillis() - pos.getMilliTime()) / 1000f > tr.getTime()) {
                iter.remove();
            }
        }

        ArrayList<CTrailRender.TrailPos> trailPositions = tr.getTrailPositions();
        Vector2 lastPos = body.getPosition();
        for (CTrailRender.TrailPos tp : trailPositions) {
            float fraction = (System.currentTimeMillis() - tp.getMilliTime()) / 1000f / tr.getTime();
            GS.i.getShapeRenderer().setColor(tr.getTopColor().cpy().lerp(tr.getBottomColor(), fraction));
            GS.i.getShapeRenderer().rectLine(lastPos, tp.getPos(), MathUtils.lerp(tr.getTopWidth(), tr.getBottomWidth(), fraction));

            lastPos = tp.getPos();
        }

        trailPositions.add(0, new CTrailRender.TrailPos(System.currentTimeMillis(), body.getPosition().cpy()));
    }
}
