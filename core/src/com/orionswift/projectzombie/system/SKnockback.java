package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.math.Vector2;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.event.EDamage;
import net.engio.mbassy.listener.Handler;

public class SKnockback extends EntitySystem {
    @Handler
    public void damage(EDamage damage) {
        CBody damagee = damage.getDamagee().getComponent(CBody.class);
        CBody source = damage.getSource().getComponent(CBody.class);

        Vector2 dir = new Vector2();
        if (damage.getKnockbackMode() == EDamage.KnockbackMode.SourceVelocity) {
            dir = source.getLinearVelocity().cpy().nor();
        } else if (damage.getKnockbackMode() == EDamage.KnockbackMode.SourcePosition){
            dir = damagee.getPosition().cpy().sub(source.getPosition()).nor();
        }
        dir.scl(damage.getKnockback());

        damagee.applyLinearImpulse(dir, damagee.getPosition(), true);
    }
}
