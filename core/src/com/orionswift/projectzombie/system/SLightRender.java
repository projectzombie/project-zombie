package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.screen.GS;

public class SLightRender extends EntitySystem {
    public SLightRender() {
        super(1000);
    }

    @Override
    public void update(float deltaTime) {
        GS.i.getRayHandler().setCombinedMatrix(GS.i.getCamera());
        GS.i.getRayHandler().updateAndRender();
    }
}
