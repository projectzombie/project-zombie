package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.component.CShowGameOverOnDeath;
import com.orionswift.projectzombie.event.EEntityDeath;
import com.orionswift.projectzombie.screen.GS;
import com.orionswift.projectzombie.ui.DeadUI;

import net.engio.mbassy.listener.Handler;

public class SGameOverOnDeath extends EntitySystem {
	@Handler
	public void death(EEntityDeath ed) {
		boolean show = !(ed.getEntity().getComponent(CShowGameOverOnDeath.class) == null);
		if(show) {
			if(GS.i.getUIStage() != new DeadUI()) {
				GS.i.switchStage(new DeadUI());
				// GS.i.pause = true; this looks ugly
			}
		}
	}
}
