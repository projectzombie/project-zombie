package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.component.CAbsorbHealthOnDamage;
import com.orionswift.projectzombie.component.CHealth;
import com.orionswift.projectzombie.component.CIgnoreAbsorbHealthOnDamage;
import com.orionswift.projectzombie.event.EDamage;

import net.dermetfan.utils.math.MathUtils;
import net.engio.mbassy.listener.Handler;

public class SAbsorbHealthOnDamage extends EntitySystem {
	@Handler
	public void absorb(EDamage damage) {
		if(damage.getDamagee().getComponent(CIgnoreAbsorbHealthOnDamage.class) != null)
			return;
		
		CAbsorbHealthOnDamage absorb = damage.getSourceSource().getComponent(CAbsorbHealthOnDamage.class);
		CHealth health = damage.getSourceSource().getComponent(CHealth.class);
		if(absorb != null && health != null) {
			health.setHealth(
					MathUtils.clamp(health.getHealth() + (damage.getDamage() * absorb.getPercentage()), 0, health.getMaxHealth()
			));
		}
	}
}
