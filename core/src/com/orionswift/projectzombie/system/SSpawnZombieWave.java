package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.EntitySystem;
import com.orionswift.projectzombie.event.ESpawnZombieRandomly;
import com.orionswift.projectzombie.event.EWaveEnd;
import com.orionswift.projectzombie.event.EWaveStart;
import com.orionswift.projectzombie.screen.GS;
import com.orionswift.projectzombie.util.ZombieType;
import net.engio.mbassy.listener.Handler;

public class SSpawnZombieWave extends EntitySystem {
    private float secondsPerZombie;
    private float counter;

    public boolean isDoingWave = false;

    @Handler
    public void waveStart(EWaveStart start) {
        float zombiesToSpawn = GS.i.getWaves().getZombieCount(GS.i.getWave());

        float nightTime = ((1 + STime.nightEnd) - STime.nightStart) * STime.secondsPerDay;
        secondsPerZombie = nightTime / zombiesToSpawn;

        isDoingWave = true;
    }

    @Handler
    public void waveEnd(EWaveEnd end) {
        isDoingWave = false;
    }

    @Override
    public void update(float deltaTime) {
        if (isDoingWave) {
            counter += deltaTime;
            while (counter > secondsPerZombie) {
                counter -= secondsPerZombie;

                float multiplier = (float) ((Math.random() / 5) * 2 + 0.8);
                multiplier *= GS.i.getWaves().getWaveConfiguration(GS.i.getWave()).getMultiplierMultiplier();

                ZombieType type = GS.i.getWaves().getWaveConfiguration(GS.i.getWave()).getRandomType().get();

                GS.i.getBus().publish(new ESpawnZombieRandomly(type, multiplier));
            }
        }
    }
}
