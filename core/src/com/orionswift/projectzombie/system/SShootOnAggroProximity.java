package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.Vector2;
import com.orionswift.projectzombie.component.CAggro;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CShootOnAggroProximity;
import com.orionswift.projectzombie.component.CWeapon;
import com.orionswift.projectzombie.event.EStartFiringWeapon;
import com.orionswift.projectzombie.event.EStopFiringWeapon;
import com.orionswift.projectzombie.screen.GS;

public class SShootOnAggroProximity extends IteratingSystem {
    public SShootOnAggroProximity() {
        super(Family.all(CWeapon.class, CAggro.class, CShootOnAggroProximity.class, CBody.class).get());
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        CWeapon weapon = entity.getComponent(CWeapon.class);
        if (entity.getComponent(CAggro.class).getAggro().isPresent()) {
            float dist2 = entity.getComponent(CAggro.class).getAggro().get().getComponent(CBody.class).getPosition().dst2(entity.getComponent(CBody.class).getPosition());
            float prox = entity.getComponent(CShootOnAggroProximity.class).getDistance();

            if (dist2 <= prox * prox) {
                if (!weapon.getWeapon().isFiring()) {
                    GS.i.getBus().publish(new EStartFiringWeapon(entity));
                }
            } else {
                if (weapon.getWeapon().isFiring()) {
                    GS.i.getBus().publish(new EStopFiringWeapon(entity));
                }
            }
        } else {
            if (weapon.getWeapon().isFiring()) {
                GS.i.getBus().publish(new EStopFiringWeapon(entity));
            }
        }
    }
}
