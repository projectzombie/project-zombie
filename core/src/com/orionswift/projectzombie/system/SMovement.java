package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.Vector2;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CDirectionalInput;
import com.orionswift.projectzombie.component.CMovement;
import com.orionswift.projectzombie.util.NumericalSpringing;

public class SMovement extends IteratingSystem {
    public SMovement() {
        super(Family.all(CBody.class, CMovement.class, CDirectionalInput.class).get(), 10);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        CBody body = entity.getComponent(CBody.class);
        CMovement movement = entity.getComponent(CMovement.class);
        CDirectionalInput input = entity.getComponent(CDirectionalInput.class);

        if (!input.getMoveDirection().isZero()) {
            Vector2 target = input.getMoveDirection().scl(movement.getSpeed());
            Vector2 from = body.getLinearVelocity();

            NumericalSpringing.springVector(from, movement.getSpringVelocity(), target, 1, 8, deltaTime);
            body.setLinearVelocity(from);

            body.setLinearDamping(0);
        } else {
            body.setLinearDamping(movement.getGroundFriction());
        }
    }
}
