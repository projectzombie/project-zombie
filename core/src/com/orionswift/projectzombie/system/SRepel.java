package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.QueryCallback;
import com.orionswift.projectzombie.component.CBody;
import com.orionswift.projectzombie.component.CDirectionalInput;
import com.orionswift.projectzombie.component.CRepel;
import com.orionswift.projectzombie.screen.GS;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SRepel extends IteratingSystem {
    public SRepel() {
        super(Family.all(CBody.class, CRepel.class, CDirectionalInput.class).get(), 5);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        final CBody body = entity.getComponent(CBody.class);
        final CRepel repel = entity.getComponent(CRepel.class);
        CDirectionalInput di = entity.getComponent(CDirectionalInput.class);

        final Vector2 out = new Vector2();

        final Set<Body> bodiesDone = new HashSet<>();
        GS.i.getWorld().QueryAABB(new QueryCallback() {
            @Override
            public boolean reportFixture(Fixture fixture) {
                Body entityBody = fixture.getBody();

                if (!bodiesDone.contains(entityBody)) {
                    bodiesDone.add(entityBody);

                    if (entityBody.getUserData() instanceof Entity && ((Entity) entityBody.getUserData()).getComponent(CRepel.class) != null) {
                        if (entityBody.getPosition().dst2(body.getPosition()) < repel.getRadius() * repel.getRadius()) {
                            Vector2 res = entityBody.getPosition().sub(body.getPosition());
                            float len2 = res.len2();
                            out.sub(res.nor().scl((repel.getRadius() * repel.getRadius()) - len2));
                        }
                    }
                }
                return true;
            }
        }, body.getPosition().x - repel.getRadius(), body.getPosition().y - repel.getRadius(), body.getPosition().x + repel.getRadius(), body.getPosition().y + repel.getRadius());

        di.getMoveDirection().add(out.scl(2)).nor();
    }
}
