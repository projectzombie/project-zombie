package com.orionswift.projectzombie.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.orionswift.projectzombie.component.CRemoveOnAnimatedSpriteFinish;
import com.orionswift.projectzombie.screen.GS;

public class SRemoveOnAnimatedSpriteFinish extends IteratingSystem {
    public SRemoveOnAnimatedSpriteFinish() {
        super(Family.all(CRemoveOnAnimatedSpriteFinish.class).get());
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        CRemoveOnAnimatedSpriteFinish roasf = entity.getComponent(CRemoveOnAnimatedSpriteFinish.class);

        if (roasf.getSprite().isAnimationFinished()) {
            GS.i.getEngine().removeEntity(entity);
        }
    }
}
