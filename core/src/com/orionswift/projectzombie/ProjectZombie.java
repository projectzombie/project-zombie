package com.orionswift.projectzombie;

import com.badlogic.gdx.Game;
import com.orionswift.projectzombie.screen.MenuScreen;

public class ProjectZombie extends Game {

    @Override
    public void create() {
        setScreen(new MenuScreen());
    }

    public static ProjectZombie i;

    {
        i = this;
    }
}
