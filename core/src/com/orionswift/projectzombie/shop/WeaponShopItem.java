package com.orionswift.projectzombie.shop;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.orionswift.projectzombie.component.CWeapon;
import com.orionswift.projectzombie.component.CWeaponStash;
import com.orionswift.projectzombie.event.ESwitchWeapon;
import com.orionswift.projectzombie.screen.GS;
import com.orionswift.projectzombie.weapon.Weapon;

import java.lang.reflect.InvocationTargetException;

public class WeaponShopItem extends ShopItem {
    private final Class<? extends Weapon> weaponClass;
    private boolean owned;

    public WeaponShopItem(String name, TextureRegion weaponIcon, Class<? extends Weapon> weaponClass, float price) {
        super(name, price, weaponIcon);
        this.weaponClass = weaponClass;
    }

    @Override
    public void purchase() {
        owned = true;

        for (Entity entity : GS.i.getEngine().getEntitiesFor(Family.all(CWeaponStash.class).get())) {
            CWeaponStash stash = entity.getComponent(CWeaponStash.class);
            CWeapon cweapon = entity.getComponent(CWeapon.class);

            try {
                Weapon weapon = weaponClass.getConstructor(Entity.class).newInstance(entity);

                stash.getOwnedWeapons().add(weapon);

                if (stash.getOwnedWeapons().size() == 1) {
                    GS.i.getBus().publish(new ESwitchWeapon(entity, cweapon == null ? null : cweapon.getWeapon(), weapon));
                }
            } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean shouldDisableButton() {
        return super.shouldDisableButton() || owned;
    }
}
