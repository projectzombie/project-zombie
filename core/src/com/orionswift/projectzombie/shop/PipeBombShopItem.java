package com.orionswift.projectzombie.shop;

import com.badlogic.ashley.core.Family;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.orionswift.projectzombie.component.CPipeBombs;
import com.orionswift.projectzombie.screen.GS;

public class PipeBombShopItem extends ShopItem {
    public PipeBombShopItem(String name, float price, TextureRegion icon) {
        super(name, price, icon);
    }

    @Override
    public void purchase() {
        CPipeBombs pb = GS.i.getEngine().getEntitiesFor(Family.all(CPipeBombs.class).get()).first().getComponent(CPipeBombs.class);
        pb.setCount(pb.getCount() + 1);
    }
}
