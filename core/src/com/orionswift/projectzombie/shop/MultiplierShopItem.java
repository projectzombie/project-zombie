package com.orionswift.projectzombie.shop;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.google.common.base.Function;
import com.orionswift.projectzombie.component.CMultiplier;
import com.orionswift.projectzombie.screen.GS;

public class MultiplierShopItem extends ShopItem {
    private Function<CMultiplier, Void> apply;

    public MultiplierShopItem(String name, float price, TextureRegion icon, Function<CMultiplier, Void> apply) {
        super(name, price, icon);
        this.apply = apply;
    }

    @Override
    public void purchase() {
        ImmutableArray<Entity> muls = GS.i.getEngine().getEntitiesFor(Family.all(CMultiplier.class).get());
        if (muls.size() > 0) {
            apply.apply(muls.get(0).getComponent(CMultiplier.class));
        }
    }
}
