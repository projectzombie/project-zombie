package com.orionswift.projectzombie.shop;

import com.orionswift.projectzombie.event.EStartTilePlacement;
import com.orionswift.projectzombie.screen.GS;
import com.orionswift.projectzombie.tile.TileType;
import com.orionswift.projectzombie.ui.TilePlacementUI;

public class TileTypeShopItem extends ShopItem {
    private TileType type;

    public TileTypeShopItem(TileType type) {
        super(type.name(), type.getPrice(), type.getTexture());
        this.type = type;
        buyButtonMessage = "Place ($" + prettyPrintFloat(type.getPrice()) + ")";
    }

    @Override
    public void _purchase() {
        // Charging happens in tileplacement, not here, so skip check
        purchase();
    }

    @Override
    public void purchase() {
        GS.i.switchStage(new TilePlacementUI());
        GS.i.getBus().publish(new EStartTilePlacement(type));
        GS.i.pause = false;
    }

    protected String prettyPrintFloat(float f) {
        if (f == (long) f)
            return String.format("%d", (long) f);
        else
            return String.format("%s", f);
    }
}
