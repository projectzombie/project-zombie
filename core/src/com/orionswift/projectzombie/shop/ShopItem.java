package com.orionswift.projectzombie.shop;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.orionswift.projectzombie.screen.GS;
import com.orionswift.projectzombie.util.Assets;
import com.orionswift.projectzombie.util.SoundEffect;

public abstract class ShopItem {
    private String name;
    private float price;
    private TextureRegion icon;
    protected String buyButtonMessage;

    public ShopItem(String name, float price, TextureRegion icon) {
        this.name = name;
        this.price = price;
        this.icon = icon;
        buyButtonMessage = "Buy ($" + prettyPrintFloat(price) + ")";
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public TextureRegion getIcon() {
        return icon;
    }

    public boolean shouldDisableButton() {
        return GS.i.money.get() < price;
    }

    public void _purchase() {
        if (GS.i.money.get() >= price) {
            GS.i.money.sub(price);

            Assets.itemBuy.play();

            purchase();
        }
    }

    public abstract void purchase();

    public String getBuyButtonMessage() {
        return buyButtonMessage;
    }

    protected String prettyPrintFloat(float f) {
        if (f == (long) f)
            return String.format("%d", (long) f);
        else
            return String.format("%s", f);
    }
}
