package com.orionswift.projectzombie.shop;

import java.util.List;

public class Category {
    private String name;
    private List<ShopItem> items;

    public Category(String name, List<ShopItem> items) {
        this.name = name;
        this.items = items;
    }

    public String getName() {
        return name;
    }

    public List<ShopItem> getItems() {
        return items;
    }
}
